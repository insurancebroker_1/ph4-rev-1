import requests as req
import urllib
import json
import hmac
import hashlib
import time
import base64
import itertools as _itertools
import LineNoty
#from poloniex2.poloniex import Poloniex

EXCHANGE_NAME = "Poloniex"
API_Key = "8K2YA0K6-HKCN3GZ5-ZR7F4V1N-UNZU3PN2"
Secret = "a6f0953250129cdda121206473563831675dab509115f85d71517dacb29c474ace7a1f4c872de415d273cc626426bd5fbb8fd26d57aa5ce09ff1b3aadf5640c8"

# polo = Poloniex(API_Key,Secret)
# polo.withdraw('XRP',30,'rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1015164727')

class _PoloniexAuth(req.auth.AuthBase):

    """Poloniex Request Authentication."""

    def __init__(self, apikey, secret):
        try:
            self._apikey, self._secret = apikey, secret
        except Exception as ex : 
            print(str(ex) + ' at __init__()')

    def __call__(self, request):
        try:
            signature = hmac.new(
                str.encode(self._secret, 'utf-8'),
                str.encode(request.body, 'utf-8'),
                hashlib.sha512
            )
            request.headers.update({"Key": self._apikey,
                                    "Sign": signature.hexdigest()})

            print(request.body)
            print(request.headers)
            return request
        except Exception as ex : 
            print(str(ex) + ' at __call__()')

#currency=XRP&amount=30&address=rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs&paymentId=1015164727&command=withdraw&nonce=1555262768160
#currency=XRP&amount=30&address=rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs&paymentId=1015164727&command=withdraw&nonce=1555262638156




def BuyAllCurrency(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        rateSymbol = getRateForBuy(_symbol)
        amountSymbol = getAmountForBuy(_symbol)
        if(getMinimalBuy(_symbol) >= amountSymbol):
            return False

        time.sleep(1)
        nonce = int(time.time()*1000)
        result = ''
        data = {
            'command': 'buy',
            'currencyPair': symbol,
            'rate': rateSymbol,
            'amount': amountSymbol,
            'fillOrKill': 1,
            'immediateOrCancel': 1,
            'nonce': int(nonce)+3
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)
        if 'orderNumber' not in jsonConvert:
            result = BuyAllCurrency(_symbol)
        else:
            result = True

        return result
    except Exception as ex : 
            print(str(ex) + ' at BuyAllCurrency('+_symbol+')')

def SellAllCurrency(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        rateSymbol = getRateForSell(_symbol)
        amountSymbol = GetBalance(_symbol)
        if(getMinimalSell(symbol) >= amountSymbol):
            return False
        
        time.sleep(1)
        nonce = int(time.time()*1000)
        result = ''
        data = {
            'command': 'sell',
            'currencyPair': symbol,
            'rate': rateSymbol,
            'amount': amountSymbol,
            'fillOrKill': 1,
            'immediateOrCancel': 1,
            'nonce': int(nonce)+1
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)
        if 'orderNumber' not in jsonConvert:
            result = SellAllCurrency(_symbol)
        else:
            result = True

        return result
    except Exception as ex : 
            print(str(ex) + ' at SellAllCurrency('+_symbol+')')



def withdrawAllBalanceByTag(address, addressTag, _symbol='XRP'):
    try:
        amountSymbol = GetBalance(_symbol)
        time.sleep(1)
        data = {      
            'currency': _symbol,
            'amount': amountSymbol,
            'address': address,
            'paymentId': addressTag,
            'command': 'withdraw',
            'nonce': int(time.time() * 1000)+1,
        }
        #data = urllib.parse.urlencode(data)
        # bodyReq = 'nonce='+str(nonce)+'&currency='+_symbol+'&amount='+str(amountSymbol)+'&command=withdraw&address='+str(address)+'&paymentId='+str(addressTag)
        # print(data)
        # h = hmac.new(Secret.encode('utf-8'), data.encode('utf-8'), hashlib.sha512).hexdigest()
        # header = {'Content-Length': '125', 'Accept-Encoding': 'gzip, deflate', 'Accept': '*/*', 'User-Agent': 'python-requests/2.21.0', 'Connection': 'keep-alive','Key':  API_Key, 'Sign': h,'Content-Type': 'application/x-www-form-urlencoded'}

        s = req.session()

        r =  s.post('https://poloniex.com/tradingApi',
                    data=data,   auth=_PoloniexAuth(API_Key,Secret))
        jsonConvert = json.loads(r.text)
        return jsonConvert
    except Exception as ex : 
            print(str(ex) + ' at withdrawAllBalanceByTag('+address+','+addressTag+','+_symbol+')')
    



def GetFeePrice():
  try:
    fee = {'tradeFee':0.25,
    'transfer':{
        'BTC':0.00050000,
        'ETH':0.01,
        'XRP':0.25,
        'OMG':0.54,
        'LTC':0.001
    }
    
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')



#print(withdrawAllBalanceByTag('rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1015164727'))
#print(getAmountForBuy('ETH'))


#print(testFunction('rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs',1015164727))
#print(getAmountForBuy('ETH'))

################################################################################################################################################################################################################################


def SellOrder(sourceSymbol, tragetSymbol):
    try:
        symbol = sourceSymbol+'_'+tragetSymbol
        rateSymbol = getRateForSell(tragetSymbol)
        amountSymbol = GetBalance(tragetSymbol)
        if(getMinimalSell(symbol) >= amountSymbol):
            return False
        
        time.sleep(1)
        nonce = int(time.time()*1000)
        result = ''
        data = {
            'command': 'sell',
            'currencyPair': symbol,
            'rate': rateSymbol,
            'amount': amountSymbol,
            'fillOrKill': 1,
            'immediateOrCancel': 1,
            'nonce': int(nonce)+1
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)
        if 'orderNumber' not in jsonConvert:
            result = SellOrder(sourceSymbol, tragetSymbol)
        else:
            LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(amountSymbol))
            result = True

        return result
    except Exception as ex : 
        print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')

def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance):
    try:
        symbol = sourceSymbol+'_'+tragetSymbol
        rateSymbol = getRateForBuy(tragetSymbol)
        amountSymbol = getAmountForBuy(tragetSymbol , volumn) 
        if not isRebalance:
            amountSymbol = getAmountForBuy(tragetSymbol)

        if(getMinimalBuy(tragetSymbol) >= amountSymbol):
            return False

        time.sleep(1)
        nonce = int(time.time()*1000)
        result = ''
        data = {
            'command': 'buy',
            'currencyPair': symbol,
            'rate': rateSymbol,
            'amount': amountSymbol,
            'fillOrKill': 1,
            'immediateOrCancel': 1,
            'nonce': int(nonce)+3
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)
        if 'orderNumber' not in jsonConvert:
            result = BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance)
        else:
            LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountSymbol))
            result = True

        return result
    except Exception as ex : 
        print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')



def transferOrder(symbol,tragetAccount, addressTag="" , exchange=""):
    try:     
        amountSymbol = GetBalance(symbol)
        time.sleep(1)
        data={}
        # data = {      
        #     'currency': symbol,
        #     'amount': amountSymbol,
        #     'address': tragetAccount,
        #     'paymentId': addressTag,
        #     'command': 'withdraw',
        #     'nonce': int(time.time() * 1000)+1,
        # }
        if addressTag != "":
            data = {      
                'currency': symbol,
                'amount': amountSymbol,
                'address': tragetAccount,
                'paymentId': addressTag,
                'command': 'withdraw',
                'nonce': int(time.time() * 1000)+1,
            }
        else:
            data = {      
                'currency': symbol,
                'amount': amountSymbol,
                'address': tragetAccount,
                'command': 'withdraw',
                'nonce': int(time.time() * 1000)+1,
            }
        

        #data = urllib.parse.urlencode(data)
        # bodyReq = 'nonce='+str(nonce)+'&currency='+_symbol+'&amount='+str(amountSymbol)+'&command=withdraw&address='+str(address)+'&paymentId='+str(addressTag)
        # print(data)
        # h = hmac.new(Secret.encode('utf-8'), data.encode('utf-8'), hashlib.sha512).hexdigest()
        # header = {'Content-Length': '125', 'Accept-Encoding': 'gzip, deflate', 'Accept': '*/*', 'User-Agent': 'python-requests/2.21.0', 'Connection': 'keep-alive','Key':  API_Key, 'Sign': h,'Content-Type': 'application/x-www-form-urlencoded'}

        s = req.session()

        r =  s.post('https://poloniex.com/tradingApi',
                    data=data,   auth=_PoloniexAuth(API_Key,Secret))
        jsonConvert = json.loads(r.text)
        LineNoty.notiForTransfer(symbol,tragetAccount, str(amountSymbol) , EXCHANGE_NAME , addressTag, exchange)

        return jsonConvert
    except Exception as ex : 
        print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')

def CheckPendingOrder(symbol):
    try:
        nonce = int(time.time() * 1000)
        data = {
            'command': 'returnDepositsWithdrawals',
            'start' : 1 ,
            'end' : int(nonce)   ,
            'nonce': int(nonce)+1
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)

        for x in range(1, 10):
            if(jsonConvert['withdrawals'][x * (-1)]['currency'] == symbol ):
                strResult = str(jsonConvert['withdrawals'][x * (-1)]['status'])
                return strResult[0:strResult.rindex(':')]
        return 'null'
    except Exception as ex : 
        print(str(ex) + ' at CheckPendingOrder('+symbol+')')


def GetTradeStatus(sourceSymbol, tragetSymbol):
  try:
    r = req.get('https://poloniex.com/public?command=returnCurrencies')
    jsonload = json.loads(r.text)
        
    return jsonload[tragetSymbol]
  except Exception as ex : 
        print(str(ex) + ' at GetTradeStatus('+sourceSymbol+','+tragetSymbol+')')

# def GetFeePrice(tragetSymbol):
#   r = req.get('https://poloniex.com/public?command=returnCurrencies')
#   jsonload = json.loads(r.text)
       
#   return float(jsonload[tragetSymbol]['txFee'])

def GetBalance(symbol):
    try:
        time.sleep(1)
        nonce = int(time.time() * 1000)
        data1 = {
            'command': 'returnBalances',
            'nonce': nonce
        }
        data1 = urllib.parse.urlencode(data1).encode()

        h = hmac.new(Secret.encode(), data1, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data1,   headers=header)
        jsonConvert = json.loads(r.text)
        return float(jsonConvert[symbol])
    except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')

def getAmountForBuy(_symbol , volumn = 0.0):
    try:
        symbol = 'USDT_'+_symbol
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=5')
        jsonConvert = json.loads(r.text)
        balance = volumn
        if balance == 0.0:
           balance = GetBalance('USDT')

        amountBuy =  balance/float(jsonConvert['asks'][3][0])

        return amountBuy
    except Exception as ex : 
        print(str(ex) + ' at getAmountForBuy('+_symbol+')')


def getMinimumSellSource(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        if _symbol == "USDT":
            return 1
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=1')
        jsonConvert = json.loads(r.text)
        rateMini = float(jsonConvert['bids'][0][0])/1

        return rateMini
    except Exception as ex : 
        print(str(ex) + ' at getMinimumSellSource('+_symbol+')')

def getMinimumBuySource(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        if _symbol == "USDT":
            return 1
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=1')
        jsonConvert = json.loads(r.text)
        rateMini = 1/float(jsonConvert['asks'][0][0]) 

        return rateMini
    except Exception as ex : 
        print(str(ex) + ' at getMinimumBuySource('+_symbol+')')



def testFunction(address, addressTag, _symbol='XRP'):
    try:
        time.sleep(1)
        nonce = int(time.time() * 1000)

        data = {
            'command': 'returnDepositsWithdrawals',
            'start' : 1 ,
            'end' : int(nonce) ,
            'nonce': int(nonce)+1
        }
        data = urllib.parse.urlencode(data).encode()

        h = hmac.new(Secret.encode(), data, hashlib.sha512).hexdigest()

        header = {'Key': API_Key, 'Sign': h,
                'Content-Type': 'application/x-www-form-urlencoded'}

        r = req.post(url='https://poloniex.com/tradingApi',
                    data=data,   headers=header)
        jsonConvert = json.loads(r.text)
        return jsonConvert
    except Exception as ex : 
        print(str(ex) + ' at testFunction('+address+','+addressTag+','+_symbol+')')

def getMinimalSell(_symbol):
    try: 
        rateMini = 0
        if _symbol == 'XRP':
            rateMini = 33
        elif _symbol == 'ETH':
            rateMini = 0.003

        return rateMini
    except Exception as ex : 
        print(str(ex) + ' at getMinimalSell('+_symbol+')')
#print(withdrawAllBalanceByTag('rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1015164727'))
#print(getAmountForBuy('ETH'))

def getMinimalBuy(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=1')
        jsonConvert = json.loads(r.text)
        rateMini = 10/float(jsonConvert['asks'][0][0]) 

        return rateMini
    except Exception as ex : 
        print(str(ex) + ' at getMinimalBuy('+_symbol+')')
#print(testFunction('rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs',1015164727))
#print(getAmountForBuy('ETH'))

def getRateForSell(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=1')
        jsonConvert = json.loads(r.text)
        rateSell = float(jsonConvert['bids'][0][0]) * (90/100)
        return rateSell
    except Exception as ex : 
        print(str(ex) + ' at getRateForSell('+_symbol+')')
################################################################################################################################################################################################################################

def getRateForBuy(_symbol):
    try:
        symbol = 'USDT_'+_symbol
        r = req.post(url='https://poloniex.com/public?command=returnOrderBook&currencyPair='+symbol+'&depth=1')
        jsonConvert = json.loads(r.text)
        rateBuy = float(jsonConvert['asks'][0][0]) * (110/100)
        return rateBuy
    except Exception as ex : 
        print(str(ex) + ' at getRateForBuy('+_symbol+')')

def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')