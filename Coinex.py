import os
import sys
import json
import Lib.ccxt as ccxt
import requests as req
import datetime

root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root + '/python')

coinex = ccxt.coinex({
    'apiKey': "51CFE7AD5A4147788B08D453B389E957",
    'secret': "3E9859CCE90A4EDBAB0932FE8835DDF2B02F140ACAB92E90",
    'verbose': False,  # switch it to False if you don't want the HTTP log
})


def GetBalance(symbol):
    try:
        respons = coinex.fetch_balance()

        return float(respons['info']['data'][symbol]['available'])
    except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')


def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance):
    try:
        symbol = tragetSymbol+sourceSymbol
        amount = volumn
        if not isRebalance:
         amount = GetBalance(sourceSymbol)

        respons = coinex.create_order(symbol ,'market','buy',amount)
         
        return True
    except Exception as ex :
        print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')


def SellOrder(sourceSymbol , tragetSymbol):
    try:
        symbol = tragetSymbol +"/"+sourceSymbol
        amount = GetBalance(tragetSymbol)


        respons = coinex.create_order(symbol ,'market','sell',amount)
         
        return True
    except Exception as ex :
        print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')

def transferOrder(symbol,tragetAccount, addressTag=""):
    try:
        balance = GetBalance(symbol)
        
        respons = coinex.withdraw(symbol,balance,tragetAccount,addressTag)

        return True
    except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')

def CheckPendingOrder(symbol):#CheckPendingOrder()
  try:
    x = datetime.datetime.now()
    y = datetime.datetime(x.year, x.month, x.day-2)
    respons = coinex.fetch_withdrawals(symbol,y,10)
    for item in respons['data']:
        if item['coin_type'] == symbol:
            if item['status'] == 'finish':
                return True
            else:
                return False

    return False
  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')


def GetTradeStatus(sourceSymbol, tragetSymbol):
  try:
    if sourceSymbol == 'XRP':
      sourceSymbol = 'XXRP'

    if tragetSymbol == 'XRP':
      tragetSymbol = 'XXRP'

    if sourceSymbol == 'BTC':
      sourceSymbol = 'XXBT'

    if tragetSymbol == 'BTC':
      tragetSymbol = 'XXBT'

    r = req.get('https://api.kraken.com/0/public/AssetPairs?pair='+tragetSymbol+sourceSymbol)
    jsonload = json.loads(r.text)

    if tragetSymbol+sourceSymbol in jsonload['result']:
      return True

        
    return False
  except Exception as ex : 
    print(str(ex) + ' at GetTradeStatus('+sourceSymbol+','+tragetSymbol+')')

def GetFeePrice():
  try:
    fee = {'tradeFee':0.001,
    'transfer':{
      'BTC':0.00050000,
      'ETH':0.01,
      'XRP':0.15,
      'OMG':1.1,
      'LTC':0.001
      }
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')


