import Okex
import Bitfinex
import HitBTC
import time
from datetime import datetime

FtoE = "Fund To Exchange"
EtoF = "Exchange To Fund"

SWITCH_Transfer = EtoF

def main():
    try:
        global SWITCH_Transfer
        global FtoE
        global EtoF
        
        print(Okex.GetBalance('USDT'))
        print(Bitfinex.GetBalance('USDT'))
        print(HitBTC.GetBalance('USDT'))

        while(True):
            writeLog(" ------------------------------    Start  Loop   ------------------------------ ")
            if SWITCH_Transfer == EtoF:
                writeLog("Start Mode " + EtoF)
                if not Okex.transferSpotToFundAccount('USDT'):
                    writeLog("OKEX transferSpotToFundAccount(USDT) Error!!")
                else:
                    writeLog("OKEX transferSpotToFundAccount(USDT) Success")

                okexBalance , okexSuccess = Okex.GetBalanceFund('USDT')
                if not okexSuccess:    
                    writeLog("OKEX GetBalanceFund(USDT) Error!!")
                else:
                    writeLog("OKEX GetBalanceFund(USDT) Success Amount = " + str(okexBalance) )




                if not  Bitfinex.transferExchangeToFund('USDT'):
                    writeLog("Bitfinex transferExchangeToFund(USDT) Error!!")
                else:
                    writeLog("Bitfinex transferExchangeToFund(USDT) Success")
               
                bitfinexBalance , bitfinexSuccess = Bitfinex.GetBalanceFund('USDT')
                if not bitfinexSuccess:    
                    writeLog("Bitfinex GetBalanceFund(USDT) Error!!")
                else:
                    writeLog("Bitfinex GetBalanceFund(USDT) Success Amount = " + str(bitfinexBalance) )




                if not  HitBTC.transferExchangeToBank('USDT'):
                    writeLog("HitBTC transferExchangeToBank(USDT) Error!!")
                else:
                    writeLog("HitBTC transferExchangeToBank(USDT) Success")
                
                HitBTCBalance , HitBTCSuccess = HitBTC.GetBalanceFund('USDT')
                if not HitBTCSuccess:    
                    writeLog("HitBTC GetBalanceFund(USDT) Error!!")
                else:
                    writeLog("HitBTC GetBalanceFund(USDT) Success Amount = " + str(HitBTCBalance) )

                SWITCH_Transfer = FtoE
                writeLog("Switch Mode To" + FtoE)


            elif SWITCH_Transfer == FtoE:
                writeLog("Start Mode " + FtoE)

                okexBalance , okexSuccess , OkexTransfer =  Okex.GetBalanceTest('USDT')
                if not OkexTransfer:
                    writeLog("OKEX transferFundAccountToSpot(USDT) Error!!")
                else:
                    writeLog("OKEX transferFundAccountToSpot(USDT) Success")

                if not okexSuccess:    
                    writeLog("OKEX GetBalance(USDT) Error!!")
                else:
                    writeLog("OKEX GetBalance(USDT) Success Amount = " + str(okexBalance) )


                BitfinexBalance , BitfinexSuccess , BitfinexTransfer = Bitfinex.GetBalanceTest('USDT')
                if not BitfinexTransfer:
                    writeLog("Bitfinex transferFundToExchange(USDT) Error!!")
                else:
                    writeLog("Bitfinex transferFundToExchange(USDT) Success")

                if not BitfinexSuccess:    
                    writeLog("Bitfinex GetBalance(USDT) Error!!")
                else:
                    writeLog("Bitfinex GetBalance(USDT) Success Amount = " + str(BitfinexBalance) )

                
                HitBTCBalance , HitBTCSuccess , HitBTCTransfer =  HitBTC.GetBalanceTest('USDT')
                if not HitBTCTransfer:
                    writeLog("HitBTC transferBankToExchange(USDT) Error!!")
                else:
                    writeLog("HitBTC transferBankToExchange(USDT) Success")

                if not HitBTCSuccess:    
                    writeLog("HitBTC GetBalance(USDT) Error!!")
                else:
                    writeLog("HitBTC GetBalance(USDT) Success Amount = " + str(HitBTCBalance) )

                SWITCH_Transfer = EtoF
                writeLog("Switch Mode To" + EtoF)
            else:
                writeLog("Error not case [" + FtoE + "] and [" + EtoF + "]")

            writeLog(" ------------------------------ Sleep For 5 Second ------------------------------ ")
            time.sleep(5)
            
        return True
    except Exception as ex :
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        writeLog('main() Error :' + str(ex))
        print('['+current_time+'] main() Error :' + str(ex))



def writeLog(content):
    try:
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        f = open("log_test_transfer.txt", "a")
        f.write("["+current_time+"] "+ content + "\r\n")
        f.close()
        return True
    except Exception as ex :
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        print('['+current_time+'] writeLog() Error :'+ str(ex))


print(main())