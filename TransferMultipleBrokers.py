import BX 
import Binance
import Poloniex
import BrokerList

def CheckingWhereMoney():
    broklist = BrokerList.GetBrokerList()
    balancelist = []
    for brok in broklist:
        balance = broklist[brok]['BALANCE']
        balancelist.append({'brokeName':brok,'balance':balance})

    TopBroker ={}
    maxValue = 0
    for bal in balancelist:
        if bal['balance']> maxValue:
            TopBroker = bal

    return TopBroker['brokeName']

