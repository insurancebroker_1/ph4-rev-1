import asyncio
import requests as req
import websockets
import json
import time
import _thread 
import time
import os, ssl
import certifi

ssl_context = ssl.create_default_context(cafile=certifi.where())


COIN_ARRAY=[]
COIN_PRICE={}

# def create_file_txt(sourceSymbol,tragetSymbol):
#         name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
#         f = open(name_file, "w")
#         f.write('wait')
#         f.close()
#         time.sleep(1)
async def wss_get_price(sourceSymbol,tragetSymbol=""):
   
    symbol =  tragetSymbol+sourceSymbol
    PING_COUNT = 0
    executePing =  500
    
    async with websockets.connect('wss://stream.binance.com:9443/ws/'+symbol.lower()+'@depth20@100ms' , ssl=ssl_context) as websocket:
        # name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
        global COIN_PRICE
        try:
            while True :
                PING_COUNT += 1
                if PING_COUNT > executePing:
                    PING_COUNT = 0
                    await sendPing(websocket)

                response = await websocket.recv()
                response_json = json.loads(response)
                if "asks" in response_json and "bids" in response_json:
                    COIN_PRICE[symbol]['asks'] = {}
                    COIN_PRICE[symbol]['bids'] = {}
                    for asks in response_json['asks']:
                        COIN_PRICE[symbol]["asks"][asks[0]] = float(asks[1])

                    for bids in response_json['bids']:
                        COIN_PRICE[symbol]["bids"][bids[0]] = float(bids[1])
        except Exception as ex :
            wss_get_price(sourceSymbol,tragetSymbol)
            # f = open(name_file, "w")
            # f.write(response_bids_price)
            # f.close()



async def sendPing(websocket):
    await websocket.send('{"method": "LIST_SUBSCRIPTIONS","id": 3}')
                       
def run_wss_get_price(sourceSymbol,tragetSymbol = "") :
    try:
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price(sourceSymbol,tragetSymbol))
    except Exception as ex :
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price(sourceSymbol,tragetSymbol))
        

def run_thread_wss_get_price(sourceSymbol,tragetSymbol = "") :
    sourceSymbol  =  sourceSymbol.upper()
    tragetSymbol  =  tragetSymbol.upper()
    global COIN_PRICE
    symbol =  tragetSymbol+sourceSymbol
    COIN_PRICE[symbol] = {}
    # create_file_txt(sourceSymbol,tragetSymbol)
    _thread.start_new_thread( run_wss_get_price, (sourceSymbol,tragetSymbol) )



async def setWsExchangeSymbol(): #<--- แก้หัวข้อตามนี้
  try:

    # print(chr(27) + "[2J")
    # print('Balance : '+ str(GetBalance(tragetSymbol)))
    r = req.get('https://api.binance.com/api/v1/exchangeInfo')
    jsonConvert = json.loads(r.text)
    
    
    for  index , symbol in enumerate(jsonConvert['symbols'] , start=1):
        await run_thread_wss_get_price(symbol['quoteAsset'] , symbol['baseAsset'])
        time.sleep(1)
        # asyncio.get_event_loop_policy().new_event_loop().run_until_complete (wss_get_price(symbol['quoteAsset'] , symbol['baseAsset']))
        # if index < 45 :
            # run_thread_wss_get_price(symbol['quoteAsset'] , symbol['baseAsset'])

  except Exception as ex : 
    print(str(ex) + ' at setWsExchangeSymbol()')



def setWsExchangeSelectSymbol():
    try:

        for symbol in COIN_ARRAY:
          run_thread_wss_get_price(symbol)
           

    except Exception as ex : 
        print(str(ex) + ' at setWsExchangeSelectSymbol()')


def getCOIN_PRICE():
    global COIN_PRICE
    return COIN_PRICE
