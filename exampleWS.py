

# import binance_ws
# import bitfinex_ws
# import bitkub_ws
import bittrex_ws
# import poloniex_ws
# import kraken_ws
# import okex_ws
# import huobi_ws
import time
import json


# def BinanceWebsocket():
#     binance_ws.COIN_ARRAY = ['ETHUSDT', 'LTCUSDT', 'BNBUSDT', 'NEOUSDT', 'MCOUSDT', 'QTUMUSDT', 'OMGUSDT', 'ZRXUSDT', 'FUNUSDT', 'IOTAUSDT', 'LINKUSDT', 'MTLUSDT', 'EOSUSDT', 'ETCUSDT',
#                              'ZECUSDT', 'DASHUSDT', 'TRXUSDT', 'XRPUSDT', 'ENJUSDT', 'NULSUSDT', 'XMRUSDT', 'BATUSDT', 'ADAUSDT', 'XLMUSDT', 'WAVESUSDT', 'GTOUSDT', 'ICXUSDT', 'RLCUSDT',
#                              'IOSTUSDT', 'NANOUSDT', 'ZILUSDT', 'ONTUSDT', 'STORMUSDT', 'WANUSDT', 'CVCUSDT', 'THETAUSDT', 'IOTXUSDT', 'KEYUSDT', 'MFTUSDT', 'HOTUSDT', 'VETUSDT', 'DOCKUSDT', 'HCUSDT', 'RVNUSDT',
#                              'MITHUSDT', 'RENUSDT', 'ONGUSDT', 'FETUSDT', 'CELRUSDT', 'MATICUSDT', 'ATOMUSDT', 'TFUELUSDT', 'ONEUSDT', 'FTMUSDT', 'ALGOUSDT', 'ERDUSDT', 'DOGEUSDT', 'DUSKUSDT', 'ANKRUSDT', 'COSUSDT',
#                              'COCOSUSDT', 'TOMOUSDT', 'PERLUSDT', 'CHZUSDT', 'BANDUSDT', 'BEAMUSDT', 'XTZUSDT', 'HBARUSDT', 'NKNUSDT', 'STXUSDT', 'KAVAUSDT', 'ARPAUSDT', 'CTXCUSDT', 'BCHUSDT', 'TROYUSDT', 'VITEUSDT',
#                              'FTTUSDT', 'OGNUSDT', 'DREPUSDT', 'TCTUSDT', 'NPXSUSDT', 'DENTUSDT', 'BTTUSDT', 'WINUSDT', 'EURUSDT', 'BULLUSDT', 'BEARUSDT', 'USDCUSDT', 'PAXUSDT', 'BUSDUSDT', 'NGNUSDT', 'USDSUSDT',
#                              'TUSDUSDT', 'BTCUSDT', 'WAVEUSDT', 'THETUSDT', 'MATIUSDT', 'TFUEUSDT', 'COCOUSDT', 'STORUSDT',  'ETHBTC', 'LTCBTC', 'BNBBTC', 'NEOBTC', 'MCOBTC', 'QTUMBTC', 'OMGBTC', 'ZRXBTC', 'FUNBTC', 'IOTABTC', 'LINKBTC', 'MTLBTC', 'EOSBTC', 'ETCBTC', 'ZECBTC', 'DASHBTC',
#                              'TRXBTC', 'XRPBTC', 'ENJBTC', 'NULSBTC', 'XMRBTC', 'BATBTC', 'ADABTC', 'XLMBTC', 'WAVESBTC', 'GTOBTC', 'ICXBTC', 'RLCBTC', 'IOSTBTC', 'NANOBTC', 'ZILBTC', 'ONTBTC', 'STORMBTC', 'WANBTC',
#                              'CVCBTC', 'THETABTC', 'IOTXBTC', 'KEYBTC', 'MFTBTC', 'HOTBTC', 'VETBTC', 'DOCKBTC', 'HCBTC', 'RVNBTC', 'MITHBTC', 'RENBTC', 'ONGBTC', 'FETBTC', 'CELRBTC', 'MATICBTC', 'ATOMBTC', 'TFUELBTC',
#                              'ONEBTC', 'FTMBTC', 'ALGOBTC', 'ERDBTC', 'DOGEBTC', 'DUSKBTC', 'ANKRBTC', 'COSBTC', 'COCOSBTC', 'TOMOBTC', 'PERLBTC', 'CHZBTC', 'BANDBTC', 'BEAMBTC', 'XTZBTC', 'HBARBTC', 'NKNBTC', 'STXBTC',
#                              'KAVABTC', 'ARPABTC', 'CTXCBTC', 'BCHBTC', 'TROYBTC', 'VITEBTC', 'FTTBTC', 'OGNBTC', 'DREPBTC', 'TCTBTC', 'QTUMETH', 'EOSETH', 'BNBETH', 'MCOETH', 'OMGETH', 'ZRXETH', 'FUNETH', 'NEOETH',
#                              'IOTAETH', 'LINKETH', 'MTLETH', 'ETCETH', 'ZECETH', 'DASHETH', 'TRXETH', 'XRPETH', 'ENJETH', 'NULSETH', 'XMRETH', 'BATETH', 'ADAETH', 'XLMETH', 'LTCETH', 'WAVESETH', 'GTOETH', 'ICXETH',
#                              'RLCETH', 'IOSTETH', 'NANOETH', 'ZILETH', 'ONTETH', 'STORMETH', 'WANETH', 'CVCETH', 'THETAETH', 'IOTXETH', 'NPXSETH', 'KEYETH', 'MFTETH', 'DENTETH', 'HOTETH', 'VETETH', 'DOCKETH', 'HCETH',
#                              'NULSBNB', 'BATBNB', 'NEOBNB', 'IOTABNB', 'XLMBNB', 'LTCBNB', 'WAVESBNB', 'GTOBNB', 'ICXBNB', 'MCOBNB', 'RLCBNB', 'NANOBNB', 'ZILBNB', 'ONTBNB', 'STORMBNB', 'QTUMBNB', 'WANBNB', 'ADABNB',
#                              'EOSBNB', 'THETABNB', 'XRPBNB', 'ENJBNB', 'TRXBNB', 'ETCBNB', 'MFTBNB', 'VETBNB', 'RVNBNB', 'MITHBNB', 'RENBNB', 'BTTBNB', 'ONGBNB', 'HOTBNB', 'ZRXBNB', 'FETBNB', 'XMRBNB', 'ZECBNB', 'IOSTBNB',
#                              'CELRBNB', 'DASHBNB', 'OMGBNB', 'MATICBNB', 'ATOMBNB', 'TFUELBNB', 'ONEBNB', 'FTMBNB', 'ALGOBNB', 'ERDBNB', 'DOGEBNB', 'DUSKBNB', 'ANKRBNB', 'WINBNB', 'COSBNB', 'COCOSBNB', 'TOMOBNB', 'PERLBNB',
#                              'CHZBNB', 'BANDBNB', 'BEAMBNB', 'XTZBNB', 'HBARBNB', 'NKNBNB', 'STXBNB', 'KAVABNB', 'ARPABNB', 'CTXCBNB', 'BCHBNB', 'TROYBNB', 'VITEBNB', 'FTTBNB', 'OGNBNB', 'DREPBNB', 'TCTBNB', 'TRXXRP', 'BTTTRX',
#                              'WINTRX', 'BNBBUSD', 'BTCBUSD', 'XRPBUSD', 'ETHBUSD', 'LTCBUSD', 'LINKBUSD', 'ETCBUSD', 'TRXBUSD', 'EOSBUSD', 'XLMBUSD', 'ADABUSD', 'BCHBUSD', 'QTUMBUSD', 'VETBUSD', 'EURBUSD', 'BULLBUSD', 'BEARBUSD',
#                              'BNBPAX', 'BTCPAX', 'ETHPAX', 'XRPPAX', 'EOSPAX', 'XLMPAX', 'USDCPAX', 'LINKPAX', 'LTCPAX', 'TRXPAX', 'BTTPAX', 'ZECPAX', 'ADAPAX', 'NEOPAX', 'BATPAX', 'ALGOPAX', 'DUSKPAX', 'ONTPAX', 'BCHPAX',
#                              'BTCTUSD', 'ETHTUSD', 'BNBTUSD', 'XRPTUSD', 'EOSTUSD', 'XLMTUSD', 'ADATUSD', 'TRXTUSD', 'NEOTUSD', 'PAXTUSD', 'USDCTUSD', 'LINKTUSD', 'WAVESTUSD', 'LTCTUSD', 'BTTTUSD', 'ZECTUSD', 'ATOMTUSD', 'BATTUSD',
#                              'ALGOTUSD', 'BCHTUSD', 'BNBUSDC', 'BTCUSDC', 'ETHUSDC', 'XRPUSDC', 'EOSUSDC', 'LINKUSDC', 'WAVESUSDC', 'LTCUSDC', 'TRXUSDC', 'BTTUSDC', 'ZECUSDC', 'ADAUSDC', 'NEOUSDC', 'ATOMUSDC', 'BATUSDC', 'ONEUSDC',
#                              'DUSKUSDC', 'WINUSDC', 'TOMOUSDC', 'BCHUSDC', 'BTCEUR', 'ETHEUR', 'BNBEUR', 'XRPEUR', 'BNBUSDS', 'BTCUSDS', 'BUSDNGN', 'BNBNGN', 'BTCNGN',
#                              # type2
#                              'BTCBUSD', 'PAXBTC', 'TUSDBTC', 'USDCBTC', 'EURBTC', 'USDSBTC', 'NGNBTC', 'ETHBTC', 'ETHBUSD', 'ETHPAX', 'TUSDETH', 'ETHUSDC', 'ETHEUR', 'BTCBNB', 'ETHBNB', 'BNBBUSD', 'BNBPAX', 'BNBTUSD', 'BNBUSDC', 'BNBEUR', 'BNBUSDS', 'BNBNGN', 'NEOBTC', 'NEOETH', 'NEOBNB', 'NEOPAX', 'NEOTUSD', 'NEOUSDC', 'LTCBTC', 'LTCETH', 'LTCBNB', 'LTCBUSD', 'LTCPAX', 'LTCTUSD', 'LTCUSDC', 'QTUMBTC', 'QTUMETH', 'QTUMBNB', 'QTUMBUSD', 'ADABTC', 'ADAETH', 'ADABNB', 'ADABUSD', 'ADAPAX', 'ADATUSD', 'ADAUSDC', 'XRPBTC', 'XRPETH', 'XRPBNB', 'XRPBUSD', 'XRPPAX', 'XRPTUSD', 'XRPUSDC', 'XRPEUR', 'EOSBTC', 'EOSETH', 'EOSBNB', 'EOSBUSD', 'EOSPAX', 'EOSTUSD', 'EOSUSDC', 'IOTABTC', 'IOTAETH', 'IOTABNB', 'XLMBTC', 'XLMETH'
#                              , 'XLMBNB', 'XLMBUSD', 'XLMPAX', 'XLMTUSD', 'ONTBTC', 'ONTETH', 'ONTBNB', 'ONTPAX', 'TRXBTC', 'TRXETH', 'TRXBNB', 'TRXXRP', 'TRXBUSD', 'TRXPAX', 'TRXTUSD', 'TRXUSDC', 'ETCBTC', 'ETCETH', 'ETCBNB', 'ETCBUSD', 'ICXBTC', 'ICXETH', 'ICXBNB', 'NULSBTC', 'NULSETH', 'NULSBNB', 'VETBTC', 'VETETH', 'VETBNB', 'VETBUSD', 'PAXTUSD', 'USDCPAX', 'USDCTUSD', 'LINKBTC', 'LINKETH', 'LINKBUSD', 'LINKPAX', 'LINKTUSD', 'LINKUSDC', 'WAVESBTC', 'WAVESETH', 'WAVESBNB', 'WAVESTUSD', 'WAVESUSDC', 'BTTBNB', 'BTTTRX', 'BTTPAX', 'BTTTUSD', 'BTTUSDC', 'ONGBTC', 'ONGBNB', 'HOTBTC', 'HOTETH', 'HOTBNB', 'ZILBTC', 'ZILETH', 'ZILBNB', 'ZRXBTC', 'ZRXETH', 'ZRXBNB', 'FETBTC', 'FETBNB', 'BATBTC', 'BATETH', 'BATBNB', 'BATPAX', 'BATTUSD', 'BATUSDC', 'XMRBTC', 'XMRETH', 'XMRBNB', 'ZECBTC', 'ZECETH', 'ZECBNB', 'ZECPAX', 'ZECTUSD', 'ZECUSDC', 'IOSTBTC', 'IOSTETH', 'IOSTBNB', 'CELRBTC', 'CELRBNB', 'DASHBTC', 'DASHETH', 'DASHBNB', 'NANOBTC', 'NANOETH', 'NANOBNB', 'OMGBTC', 'OMGETH', 'OMGBNB', 'THETABTC', 'THETAETH', 'THETABNB', 'ENJBTC', 'ENJETH', 'ENJBNB', 'MITHBTC', 'MITHBNB', 'MATICBTC', 'MATICBNB', 'ATOMBTC', 'ATOMBNB', 'ATOMTUSD', 'ATOMUSDC'
#                              , 'TFUELBTC', 'TFUELBNB', 'ONEBTC', 'ONEBNB', 'ONEUSDC', 'FTMBTC', 'FTMBNB', 'ALGOBTC', 'ALGOBNB', 'ALGOPAX', 'ALGOTUSD', 'GTOBTC', 'GTOETH', 'GTOBNB', 'ERDBTC', 'ERDBNB', 'DOGEBTC', 'DOGEBNB', 'DUSKBTC', 'DUSKBNB', 'DUSKPAX', 'DUSKUSDC', 'ANKRBTC', 'ANKRBNB', 'WINBNB', 'WINTRX', 'WINUSDC', 'COSBTC', 'COSBNB', 'NPXSETH', 'COCOSBTC', 'COCOSBNB', 'MTLBTC', 'MTLETH', 'TOMOBTC', 'TOMOBNB', 'TOMOUSDC', 'PERLBTC', 'PERLBNB', 'DENTETH', 'MFTBTC', 'MFTETH', 'MFTBNB', 'KEYBTC', 'KEYETH', 'STORMBTC', 'STORMETH', 'STORMBNB', 'DOCKBTC', 'DOCKETH', 'WANBTC', 'WANETH', 'WANBNB', 'FUNBTC', 'FUNETH', 'CVCBTC', 'CVCETH', 'CHZBTC', 'CHZBNB', 'BANDBTC', 'BANDBNB', 'BUSDNGN', 'BEAMBTC', 'BEAMBNB', 'XTZBTC', 'XTZBNB', 'RENBTC', 'RENBNB', 'RVNBTC', 'RVNBNB', 'HCBTC', 'HCETH', 'HBARBTC', 'HBARBNB', 'NKNBTC', 'NKNBNB', 'STXBTC', 'STXBNB', 'KAVABTC', 'KAVABNB', 'ARPABTC', 'ARPABNB', 'IOTXBTC', 'IOTXETH', 'RLCBTC', 'RLCETH', 'RLCBNB', 'MCOBTC', 'MCOETH', 'MCOBNB', 'CTXCBTC', 'CTXCBNB', 'BCHBTC', 'BCHBNB', 'BCHBUSD', 'BCHPAX', 'BCHTUSD', 'BCHUSDC', 'TROYBTC', 'TROYBNB', 'VITEBTC', 'VITEBNB', 'FTTBTC', 'FTTBNB', 'EURBUSD', 'OGNBTC', 'OGNBNB', 'DREPBTC', 'DREPBNB', 'BULLBUSD', 'BEARBUSD', 'TCTBCT', 'TCTBNB']

#     binance_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(binance_ws.COIN_PRICE)
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)


# BinanceWebsocket()

# BinanceWebsocket()
# def BitfinexWebsocket():
#     bitfinex_ws.COIN_ARRAY = ["tBTCUSD" , "tXRPUSD"] #, "tXRPUSD"
#     bitfinex_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(bitfinex_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# BitfinexWebsocket()


# def BitkubWebsocket():
#     bitkub_ws.COIN_ARRAY = ["thb_btc" , "thb_xrp"]
#     bitkub_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(bitkub_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# BitkubWebsocket()


# def PoloniexWebsocket():
#     poloniex_ws.COIN_ARRAY = ["USDT_BTC" , "USDT_XRP"]
#     poloniex_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(poloniex_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# PoloniexWebsocket()

# def KrakenWebsocket():
#     kraken_ws.COIN_ARRAY = [ "XBT/USD", "XBT/EUR"]
#     kraken_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(kraken_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# KrakenWebsocket()

# def OkexWebsocket():
#     okex_ws.COIN_ARRAY = [ "ETH-USDT", "BTC-USDT"]
#     okex_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(okex_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# OkexWebsocket()

# def Huobisocket():
#     huobi_ws.COIN_ARRAY = [ "btcusdt","ethusdt"] #, "ethusdt"
#     huobi_ws.setWsExchangeSelectSymbol()
#     time.sleep(5)

#     while True:
#         # f = open('mock.json', 'r+')
#         # f.truncate(0)
#         strJson = json.dumps(huobi_ws.getCOIN_PRICE())
#         f = open("mock.json", "w")
#         f.write(strJson)
#         f.close()
#         time.sleep(1)

# Huobisocket()


def BittrexSocket():
    bittrex_ws.COIN_ARRAY = [ "USD-BTC","USD-ETH"] #, "ethusdt"
    bittrex_ws.setWsExchangeSelectSymbol()
    time.sleep(5)

    while True:
        # f = open('mock.json', 'r+')
        # f.truncate(0)
        strJson = json.dumps(bittrex_ws.getCOIN_PRICE())
        f = open("mock.json", "w")
        f.write(strJson)
        f.close()
        time.sleep(1)


BittrexSocket()
