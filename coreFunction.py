

import Binance
import Huobi

def calculateKSwap(exchange):
    try:
        exchange = exchange.lower()
        tradeFee = 0.15
        if exchange == 'binance':
            tradeFee = Binance.GetFeePrice()['tradeFee']
        elif exchange == 'huobi':
            tradeFee = Huobi.GetFeePrice()['tradeFee']

        
        K = (100/(1 - (tradeFee / 100) ))/100

        return K
    except Exception as ex : 
      print(str(ex) + ' at calculateKSwap('+exchange+')')

print(calculateKSwap('huobi'))