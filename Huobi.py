from Lib.huobi_Python.huobi.requstclient import RequestClient
from Lib.huobi_Python.huobi.model import *
import hmac
import hashlib
import requests as req
import json
import math
import ApiKeyExchange
from datetime import datetime
import urllib.parse
import base64
import LineNoty


api_key = ApiKeyExchange.Houbi_KEY
secret_ket = ApiKeyExchange.Houbi_SECRET
account_id = ApiKeyExchange.Houbi_id
client = RequestClient(api_key = api_key,secret_key=secret_ket)
EXCHANGE_NAME = "Huobi"


def GetBalance(symbol):
        try :
            symbol = symbol.lower()
            all_balance = client.get_account_balance()
            for each_coin in range(0,len(all_balance[0].balances)-1) :
                    coin = all_balance[0].balances[each_coin].currency
                    balance = all_balance[0].balances[each_coin].balance
                    if  symbol == coin:
                            return float(balance)
            return False
        
        except Exception as ex:
                print(str(ex)+"at GetBalance("+symbol+")")


def GetAccount(symbol, exchange=''):
    try:
        symbol = symbol.lower()
        # timestampJson = req.get('https://api.huobi.pro/v1/common/timestamp')
        # timestampJson = json.loads(timestampJson.text)

        # payload = {
        #         "currency" : symbol
        # }
        strQuery = "&currency="+symbol
        # strQuery = ""
        # Timestamp = timestampJson['data']
        datetimeTime = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
        Timestamp = urllib.parse.quote(datetimeTime)
        
        SignatureVersion= '2'
        AccessKeyId = api_key
        msgCode =  "GET\napi.huobi.pro\n/v2/account/deposit/address\nAccessKeyId="+AccessKeyId+"&SignatureMethod=HmacSHA256&SignatureVersion="+SignatureVersion+"&Timestamp="+Timestamp +strQuery

        h = hmac.new(secret_ket.encode('utf-8') , msg=msgCode.encode('utf-8') ,digestmod = hashlib.sha256).digest()
        # h1 = hmac.new(  bytes('',"ascii") , msg=bytes('GET\napi.huobi.pro\n/v1/order/orders\nAccessKeyId=e2xxxxxx-99xxxxxx-84xxxxxx-7xxxx&SignatureMethod=HmacSHA256&SignatureVersion=2&Timestamp=2017-05-11T15%3A19%3A30&order-id=1234567890' ,"ascii") ,digestmod = hashlib.sha256).hexdigest()
        # h2 = hashlib.sha256(bytes('GET\napi.huobi.pro\n/v1/order/orders\nAccessKeyId=e2xxxxxx-99xxxxxx-84xxxxxx-7xxxx&SignatureMethod=HmacSHA256&SignatureVersion=2&Timestamp=2017-05-11T15%3A19%3A30&order-id=1234567890', 'utf-8') ).hexdigest()
        h64 = base64.b64encode(h).decode()
        Signature = h64
        SignatureURL = urllib.parse.quote(Signature)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        rawBody = "AccessKeyId="+AccessKeyId+strQuery+"&SignatureMethod=HmacSHA256"+"&SignatureVersion="+SignatureVersion+"&Timestamp="+str(Timestamp)+"&Signature="+SignatureURL
        URL = "https://api.huobi.pro/v2/account/deposit/address?"+rawBody

        request = req.get(URL, headers=headers )
        jsonReq = json.loads(request.text)

        address = jsonReq['data'][0]['address']
        tag = jsonReq['data'][0]['addressTag']
        exchange = exchange.upper()
        if exchange == "KRAKEN" and symbol == 'usdt' :
                address = jsonReq['data'][2]['address']



        return address , tag


 
    except Exception as ex :
      print(str(ex) + 'Huabi at GetAccount()')    



def BuyOrder(sourceSymbol, tragetSymbol,volume, isRebalance):
        #[Example]
        #               checkboolean = BuyOrder('usdt', 'xrp',True,volume=999)
        #               checkboolean = BuyOrder('usdt', 'xrp',False)
        #               return boolean
        try :
                sourceSymbol = sourceSymbol.lower()
                tragetSymbol = tragetSymbol.lower()
                info = GetPrecision_market(tragetSymbol,sourceSymbol)
                amountToBuy = 0
       
                mintrade_size = info['min-order-value']
                market = str(tragetSymbol+sourceSymbol)
                price_depth =client.get_price_depth(market)
                i=0
                ACCOUNT_TYPE = AccountType.SPOT
                ORDER_TYPE   = OrderType.BUY_MARKET
                if isRebalance == True :
                        all_amount = volume
                        fee_trade = all_amount* GetFeePrice()['tradeFee'] /100
                        volume_after_paid_fee = all_amount - fee_trade

                        if volume_after_paid_fee < mintrade_size :
                                print( "Huobi Buy minsize Error  ( amount : " + str(volume_after_paid_fee) + " , min : " + str(mintrade_size)  + " )" )
                                return False

                        while True:
                                qty_from_order  = price_depth.asks[i].amount
                                rate            = round(price_depth.asks[i].price,info['price-precision'])
                                value_order     = qty_from_order*rate
            
                                if value_order >= volume_after_paid_fee :
                                        qty = decress_tick(round(volume_after_paid_fee , info['amount-precision'] ))
                                        amountToBuy += qty
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                        LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                                        break
             
                                else :
                                        qty_from_order  =round(price_depth.asks[i].amount , info['amount-precision'] )
                                        amountToBuy += qty_from_order
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                        
                                if volume_after_paid_fee < mintrade_size : 
                                    LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                                    break

                                
                else :
                        all_amount  = GetBalance(sourceSymbol)
                        fee_trade = all_amount*GetFeePrice()['tradeFee']/100
                        volume_after_paid_fee = all_amount - fee_trade
                        if volume_after_paid_fee < mintrade_size :
                                print( "Huobi Buy minsize Error  ( amount : " + str(volume_after_paid_fee) + " , min : " + str(mintrade_size) + " )" )
                                return False

                        while True:
                                qty_from_order  = round(price_depth.asks[i].amount,info['amount-precision'])
                                rate            = round(price_depth.asks[i].price,info['price-precision'])
                                value_order     = qty_from_order*rate
                                if qty_from_order >= volume_after_paid_fee :
                                        qty = round(volume_after_paid_fee/rate , info['amount-precision'] )
                                        amountToBuy += qty
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                        LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                                        break
             
                                else :
                                        qty_from_order  =round(price_depth.asks[i].amount , info['amount-precision'] )
                                        amountToBuy += qty_from_order
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                        
                                if volume_after_paid_fee < mintrade_size : 
                                   LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                                   break


                if str(status).isnumeric():
                        checkboolean = True
                return checkboolean
        
        except Exception as ex :
                print(str(ex)+"at BuyOrder("+sourceSymbol+","+tragetSymbol+","+str(volume)+","+ isRebalance+")")
                return False
def BuyOrderSwap(sourceSymbol, tragetSymbol,volume, isRebalance =True):
        #[Example]
        #               checkboolean = BuyOrder('usdt', 'xrp',True,volume=999)
        #               checkboolean = BuyOrder('usdt', 'xrp',False)
        #               return boolean
        try :
                sourceSymbol = sourceSymbol.lower()
                tragetSymbol = tragetSymbol.lower()
                info = GetPrecision_market(tragetSymbol,sourceSymbol)
            
                mintrade_size = info['min-order-value']
                market = str(tragetSymbol+sourceSymbol)
                price_depth =client.get_price_depth(market)
                i=0
                ACCOUNT_TYPE = AccountType.SPOT
                ORDER_TYPE   = OrderType.BUY_LIMIT
                if isRebalance == True :
                        all_amount = volume
                        fee_trade = all_amount* GetFeePrice()['tradeFee'] /100
                        volume_after_paid_fee = all_amount 

                        if volume_after_paid_fee < mintrade_size :
                                print( "Huobi BuyOrderSwap minsize Error  ( amount : " + str(volume_after_paid_fee) + " , min : " + str(mintrade_size)  + " )")
                                return False

                        while True:
                                qty_from_order  = price_depth.asks[i].amount
                                rate            = round(price_depth.asks[i].price,info['price-precision'])
                                value_order     = qty_from_order*rate
                               
                                if qty_from_order >= volume_after_paid_fee :
                     
                                        qty = decress_tick(round(volume_after_paid_fee , info['amount-precision'] ))
                
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                
                                        break
             
                                else :
                                        
                                        qty_from_order  =round(price_depth.asks[i].amount , info['amount-precision'] )
                                        
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - qty_from_order
                                        i=i+1
                                        
                                if volume_after_paid_fee*rate < mintrade_size : break

                                
                else :
                        all_amount  = GetBalance(sourceSymbol)
                        fee_trade = all_amount*GetFeePrice()['tradeFee']/100
                        volume_after_paid_fee = all_amount - fee_trade
                        if volume_after_paid_fee < mintrade_size :
                                print( "Huobi BuyOrderSwap minsize Error  ( amount : " + str(volume_after_paid_fee) + " , min : " + str(mintrade_size)  + " )")
                                return False
                        while True:
                                qty_from_order  = round(price_depth.asks[i].amount,info['amount-precision'])
                                rate            = round(price_depth.asks[i].price,info['price-precision'])
                                value_order     = qty_from_order*rate
                                if qty_from_order >= volume_after_paid_fee :
                                        qty = round(volume_after_paid_fee/rate , info['amount-precision'] )
                                
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount =qty,
                                                                        price = rate
                                                                      )
                                        break
             
                                else :
                                        qty_from_order  =round(price_depth.asks[i].amount , info['amount-precision'] )
                                        status = client.create_order(
                                                                        symbol = market ,
                                                                        account_type = ACCOUNT_TYPE ,
                                                                        order_type = ORDER_TYPE ,
                                                                        amount = qty_from_order,
                                                                        price = rate
                                                                      )
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                        
                                if volume_after_paid_fee < mintrade_size : break


                if str(status).isnumeric():
                        checkboolean = True
                return checkboolean
        
        except Exception as ex :
                print(str(ex)+"at BuyOrder("+sourceSymbol+","+tragetSymbol+","+str(volume)+","+ isRebalance+")")
                return False



        
##def GetTradeStatus(tragetSymbol):
##        try :
##               amount_COIN = len(public_API.get_currencies()[1])-1;
##               for a in range(0,amount_COIN):
##                        if public_API.get_currencies()[1][a]["Currency"] == tragetSymbol:
##                                status = public_API.get_currencies()[1][a]["IsActive"]
##                                return status
##        except Exception as ex:
##                print(str(ex)+'at GetTradeStatus('+tragetSymbol+')')
##


def SellOrder(sourceSymbol , tragetSymbol):
        #[Example]
        #               checkboolean = SellOrder('xrp','usdt')
        #               return boolean
        try :
                sourceSymbol = sourceSymbol.lower()
                tragetSymbol = tragetSymbol.lower()
                all_amount  = GetBalance(tragetSymbol)
                info = GetPrecision_market(tragetSymbol,sourceSymbol)
            
                mintrade_size = info['minorderamt']
##                minorderamt = len(str(info['minorderamt']))-2
                market = str(tragetSymbol+sourceSymbol)
                best_quote = client.get_best_quote(market)
                ACCOUNT_TYPE = AccountType.SPOT
                ORDER_TYPE   = OrderType.SELL_MARKET
                fee_trade       = all_amount*GetFeePrice()['tradeFee']/100
                AMOUNT       =   round(all_amount-fee_trade,info['amount-precision'])
                PRICE        = round(best_quote.bid_price*(91/100),info['price-precision'])
                if AMOUNT < mintrade_size :
                                print( "Huobi SellOrder minsize Error  ( amount : " + str(AMOUNT) + " , min : " + str(mintrade_size) + " )" )
                                return False

                status = client.create_order(
                                        symbol = market ,
                                        account_type = ACCOUNT_TYPE ,
                                        order_type = ORDER_TYPE ,
                                        amount = AMOUNT,
                                        price = PRICE
                                      )
                if str(status).isnumeric():
                        LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(AMOUNT))
                        checkboolean = True
                return checkboolean        
        except Exception as ex :
                print(str(ex) , "at SellOrder(" +sourceSymbol+","+tragetSymbol+")")
                return False



def transferOrder(symbol,tragetAccount, addressTag="", exchange = ""):
        
         #[Example]
         #            status = transferOrder( 'xrp','rPVMhWBsfF9iMXYj3aAzJVkPDTFNSyWdKy','1553529033')
         #            return boolean
        try :
                chain_str = 'usdterc20'
                exchange = exchange.upper()
                if exchange == 'KRAKEN' :
                        chain_str  = 'usdt'
                symbol = symbol.lower()
                info_coin = GetInfo_withdraw(symbol,chain_str)
                qty = GetBalance(symbol)
                qty = qty-info_coin['feewithdraw']
                qty = round(qty,int(info_coin['precision']))
               
                
                qty = decress_tick(qty)

                             
                if symbol == 'usdt':
                        try :

                                withdraw_ = client.withdraw(
                                                                address = tragetAccount,
                                                                amount  = qty,
                                                                currency =symbol,
                                                                chain  = chain_str
                                        )
                                LineNoty.notiForTransfer(symbol,tragetAccount, str(qty) , EXCHANGE_NAME , addressTag, exchange)
                                return str(withdraw_).isnumeric()
                        except Exception as ex :
                                print(str(ex))
                                return False
                if addressTag != "":
                        try:
                                withdraw_ = client.withdraw(
                                                                 address     = tragetAccount,
                                                                 amount      = qty,
                                                                 currency    = symbol,
                                                                 address_tag = addressTag
                                                                 )
                                LineNoty.notiForTransfer(symbol,tragetAccount, str(qty) , EXCHANGE_NAME , addressTag, exchange)
                                return str(withdraw_).isnumeric()

                        except Exception as ex :
                                print(str(ex))
                                return False

                else :
                        try:
                                withdraw_ = client.withdraw(
                                                                address  = tragetAccount,
                                                                amount   = qty,
                                                                currency = symbol
                                                                )
                                LineNoty.notiForTransfer(symbol,tragetAccount, str(qty) , EXCHANGE_NAME , addressTag, exchange)
                                return str(withdraw_).isnumeric()
                        
                        except Exception as ex :
                                print(str(ex))
                                return False
                                
        except Exception as ex:
                print(str(ex)+ 'at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')



def CheckPendingOrder(symbol):
        # [Example] CheckPendingOrder('xrp')
        #           return boolean
        #           False คือ ไม่มี pending
        #           True  คือ pendingอยู่
        #           มีการ print แสดง status 
        try :   
               symbol = symbol.lower() 
               hisw =  client.get_withdraw_history(
                                                   currency= symbol,
                                                   from_id= 1,
                                                   size=100
                                                   )
               Val_Pending = hisw[len(hisw)-1].withdraw_state
        #        print(Val_Pending)
               if str(Val_Pending) == "confirmed" :
                       return False
               else :
                       return True
                
        except Exception as ex :
                      print(str(ex) + ' at CheckPendingOrder('+symbol+')')
##    
##def GetAccount(symbol=""):
##    try :
##            
##    except Exception as ex :
##            print(str(ex) + ' at GetAccount('+symbol+')')



def GetPrecision_market(coin,market):
        # [Example...]
        #            [Price_Precision , Amount_Precision] = GetPrecisionCoin('XRP','USDT')
        
        Check = False
        coin = coin.lower()
        market = market.lower()
        All_coin_info = client.get_exchange_info()
       
        
        for x in range(0,len(All_coin_info.symbol_list)):
                if All_coin_info.symbol_list[x].base_currency == coin and All_coin_info.symbol_list[x].quote_currency == market :
                        Price_Precision = int(All_coin_info.symbol_list[x].price_precision)
                        Amount_Precision = int(All_coin_info.symbol_list[x].amount_precision)
                        value_precision = float(All_coin_info.symbol_list[x].value_precision)
                        minorderamt = float(All_coin_info.symbol_list[x].min_order_amt)
                        min_value_order = float(All_coin_info.symbol_list[x].min_order_value)
                        jsoninfo = {"price-precision" : Price_Precision,
                                    "amount-precision": Amount_Precision,
                                    "value-precision" : value_precision,
                                    "minorderamt" : minorderamt,
                                    "min-order-value" :min_value_order}
                        
                        Check = True
                        return jsoninfo
        if Check == False:
                print('Not have name coin or market')
                return False 

        
def GetInfo_withdraw(coin,chain_str=''):
        try:
                coin = coin.lower()
                getText = req.get("https://api.huobi.pro/v2/reference/currencies?currency="+coin)
                jsontext = json.loads(getText.text)
                if coin =='usdt':
                        if chain_str == 'trc20usdt' or chain_str =='' :
                                
                                if jsontext['data'][0]['chains'][0]['chain'] == 'trc20usdt':
                                        
                                        if jsontext['data'][0]['chains'][0]['withdrawFeeType'] == 'fixed' :
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][0]['transactFeeWithdraw'])
                                        elif jsontext['data'][0]['chains'][0]['withdrawFeeType'] == 'circulated':
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][0]['minTransactFeeWithdraw'])
                                        
                                        minwithdraw_ = float(jsontext['data'][0]['chains'][0]['minWithdrawAmt'])
                                        precision_ = float(jsontext['data'][0]['chains'][0]['withdrawPrecision'])
                        
                        if chain_str == 'usdterc20' or chain_str =='' :
                                
                                if jsontext['data'][0]['chains'][2]['chain'] == 'usdterc20':
                                        
                                        if jsontext['data'][0]['chains'][2]['withdrawFeeType'] == 'fixed' :
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][2]['transactFeeWithdraw'])
                                        elif jsontext['data'][0]['chains'][2]['withdrawFeeType'] == 'circulated':
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][2]['minTransactFeeWithdraw'])
                                        
                                        minwithdraw_ = float(jsontext['data'][0]['chains'][2]['minWithdrawAmt'])
                                        precision_ = float(jsontext['data'][0]['chains'][2]['withdrawPrecision'])

                        if chain_str == 'usdt' :
                              
                                if jsontext['data'][0]['chains'][1]['chain'] == 'usdt':
                                        
                                        if jsontext['data'][0]['chains'][1]['withdrawFeeType'] == 'fixed' :
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][1]['transactFeeWithdraw'])
                                        elif jsontext['data'][0]['chains'][1]['withdrawFeeType'] == 'circulated':
                                                feewithdraw_ = float(jsontext['data'][0]['chains'][1]['minTransactFeeWithdraw'])           
                                        minwithdraw_ = float(jsontext['data'][0]['chains'][1]['minWithdrawAmt'])
                                        precision_ = float(jsontext['data'][0]['chains'][1]['withdrawPrecision'])
                        
                                

                else :
                        minwithdraw_ = float(jsontext['data'][0]['chains'][0]['minWithdrawAmt'])
                        precision_ = float(jsontext['data'][0]['chains'][0]['withdrawPrecision'])
                        if jsontext['data'][0]['chains'][0]['withdrawFeeType'] == 'fixed' :
                                feewithdraw_ = float(jsontext['data'][0]['chains'][0]['transactFeeWithdraw'])
                        elif jsontext['data'][0]['chains'][0]['withdrawFeeType'] == 'circulated':
                                feewithdraw_ = float(jsontext['data'][0]['chains'][0]['minTransactFeeWithdraw'])

                jsoninfo = {'feewithdraw' : feewithdraw_,
                            'minwithdraw':minwithdraw_,
                            'precision':precision_}
                
                return jsoninfo
        except Exception as ex :
                return False


def GetInfo_withdrawAll():
        try:
                
                getText = req.get("https://api.huobi.pro/v2/reference/currencies")
                jsontext = json.loads(getText.text)
                jsoninfoArray = []

                for jsonData in jsontext['data']:
                     if len(jsonData['chains']) > 0:
                         if jsonData['currency'] == 'usdt' :
                                if jsonData['chains'][2]['chain'] == 'usdterc20':
                                        
                                        if jsonData['chains'][2]['withdrawFeeType'] == 'fixed' :
                                                feewithdraw_ = float(jsonData['chains'][2]['transactFeeWithdraw'])
                                        elif jsonData['chains'][2]['withdrawFeeType'] == 'circulated':
                                                feewithdraw_ = float(jsonData['chains'][2]['minTransactFeeWithdraw'])
                                                
                                        minwithdraw_ = float(jsonData['chains'][2]['minWithdrawAmt'])
                                        precision_ = float(jsonData['chains'][2]['withdrawPrecision'])
                                        

                         else :
                                minwithdraw_ = float(jsonData['chains'][0]['minWithdrawAmt'])
                                precision_ = float(jsonData['chains'][0]['withdrawPrecision'])
                                if jsonData['chains'][0]['withdrawFeeType'] == 'fixed' :
                                        feewithdraw_ = float(jsonData['chains'][0]['transactFeeWithdraw'])
                                elif jsonData['chains'][0]['withdrawFeeType'] == 'circulated':
                                        feewithdraw_ = float(jsonData['chains'][0]['minTransactFeeWithdraw'])


                         jsoninfo = { 'currency' : jsonData['currency'],
                                'feewithdraw' : feewithdraw_,
                                'minwithdraw':minwithdraw_,
                                'precision':precision_}
                        
                         jsoninfoArray.append(jsoninfo)
                
                return jsoninfoArray
        except Exception as ex :
                return False     



def GetFeePrice():
    try:
        jsonz = req.get('https://api.huobi.pro/v2/reference/currencies').text
        coinlist = json.loads(jsonz)

        jsonResult = { 'tradeFee':0.2,
                       'transfer':{'18C': 172.9366, 'AAC': 179.9217, 'ABL': 150.0, 'ABT': 3.3351, 'ACT': 0.01, 'ADA': 1.0, 'ADD': 100.0, 'ADT': 200.0, 'ADX': 7.2876, 'AE': 1.0, 'AIDOC': 137.3462, 'AKRO': 71.1876, 'ALGO': 0.1, 'ANKR': 100.0, 'APPC': 15.3949, 'ARDR': 2.0, 'ARPA': 42.3522, 'AST': 18.7652, 'ATOM': 0.005, 'ATP': 2.0, 'BAT': 2.4389, 'BCH': 0.0001, 'BCV': 56.6423, 'BFT': 35.7015, 'BHD': 0.05, 'BHT': 16.5214, 'BIX': 4.4055, 'BKBT': 1114.1611, 'BLZ': 21.0476, 'BOX': 135.024, 'BSV': 0.0005, 'BTC': 0.0005, 'BTG': 0.001, 'BTM': 5.0, 'BTS': 1.0, 'BTT': 100.0, 'BUT': 286.5924, 'CDC': 2270.7034, 'CHAT': 253.0995, 'CKB': 1.0, 'CMT': 20.0, 'CNN': 9862.7787, 'CNNS': 93.6224, 'COVA': 1884.7309, 'CRE': 169.3323, 'CRO': 10.0, 'CTXC': 5.4761, 'CVC': 16.987, 'CVCOIN': 10.0145, 'CVNT': 68.8306, 'DAC': 208.2515, 'DASH': 0.002, 'DAT': 550.9861, 'DATX': 1762.4521, 'DBC': 5.0, 'DCR': 0.01, 'DGD': 0.0257, 'DOCK': 47.9833, 'DOGE': 20.0, 'DTA': 1614.7905, 'EDU': 5033.2633, 'EGCC': 1987.0753, 'EGT': 245.8202, 'EKO': 328.963, 'EKT': 4.0248, 'ELA': 0.005, 'ELF': 7.36, 'EM': 101.2911, 'ENG': 0.9455, 'EON': 10.0, 'EOP': 5.0, 'EOS': 0.1, 'ETC': 0.03, 'ETH': 0.005, 'ETN': 150.0, 'EVX': 1.6184, 'FAIR': 271.6749, 'FOR': 22.7332, 'FSN': 0.3035, 'FTI': 1894.0433, 'FTT': 0.2424, 'GAS': 0.005, 'GET': 115.9683, 'GNT': 12.5707, 'GNX': 62.1095, 'GRS': 0.05, 'GSC': 118.988,
'GT': 1.165, 'GTC': 137.664, 'GUSD': 2.0, 'GVE': 2593.0101, 'GXC': 0.5, 'HC': 0.005, 'HIT': 9899.5695, 'HOT': 154.352, 'HPT': 87.0555, 'HT': 0.1, 'HUSD': 1.0, 'HVT': 50.0, 'ICX': 5.0, 'IDT': 96.575, 'IIC': 3662.6147, 'INC': 5.6473, 'IOST': 78.0057, 'IOTA': 0.5, 'IQ': 50.0, 'IRIS': 1.0, 'ITC': 4.3386, 'KAN': 224.5874, 'KCASH': 94.8662, 'KMD': 0.002, 'KNC': 2.0959, 'LAMB': 13.9199, 'LBA': 24.1427, 'LET': 153.0883, 'LINK': 0.2056, 'LOL': 500.0, 'LOOM':
22.8628, 'LSK': 0.1, 'LTC': 0.001, 'LUN': 0.4785, 'LXT': 128.6065, 'LYM': 154.1796, 'MAN': 9.2147, 'MANA': 16.7956, 'MCO': 0.1062, 'MDS': 141.1504, 'MEET': 55.6509, 'MEETONE': 50.0, 'MEX': 1387.1856, 'MGO': 2.0, 'MT': 412.4081, 'MTL': 1.5086, 'MTN': 139.9821, 'MTX': 21.5363, 'MUSK': 538.6837, 'MVL': 2000.0, 'MX': 7.4059, 'MXC': 316.2421, 'MZK': 50.0, 'NANO': 0.01, 'NAS': 0.2, 'NCASH': 478.6348, 'NCC': 183.121, 'NEO': 0.0, 'NEW': 5.0, 'NEXO': 4.5828,
'NKN': 21.3922, 'NODE': 71.9415, 'NPXS': 2989.6013, 'NULS': 0.01, 'OCN': 770.6913, 'OGO': 34.846, 'OMG': 0.6009, 'ONE': 200.0, 'ONG': 1.0, 'ONT': 1.0, 'OST': 43.1185, 'PAI': 5.0, 'PAX': 2.0, 'PAY': 7.9934, 'PC': 4124.3275, 'PHX': 0.5, 'PIZZA': 25.0, 'PNT': 5390.625, 'POLY': 17.638, 'PORTAL': 498.5909, 'POWR': 11.1667, 'PROPY': 5.4368, 'PVT': 737.8167, 'QASH': 8.46, 'QSP': 43.1665, 'QTUM': 0.01, 'QUN': 95.8306, 'RBTC': 0.00015, 'RCCC': 35.7324, 'RCN': 9.4854, 'RDN': 3.4461, 'REN': 12.4257, 'REQ': 34.3868, 'RSR': 277.1734, 'RTE': 571.2393, 'RUFF': 80.6781, 'SALT': 7.324, 'SC': 0.1, 'SEELE': 2.89, 'SEXC': 50.0, 'SHE': 785.9199, 'SKM': 300.0, 'SMT': 7.0, 'SNC': 28.655, 'SNT':
42.0408, 'SOC': 123.616, 'SRN': 69.4521, 'SSP': 2030.0088, 'STEEM': 0.01, 'STK': 226.4596, 'STORJ': 3.8386, 'SWFTC': 498.2668, 'TFUEL': 1.0, 'THETA': 10.0, 'TNB': 353.0206, 'TNT': 8.0518, 'TOP': 279.0649, 'TOPC': 73.951, 'TOS': 344.8965, 'TRIO': 317.6312, 'TRX': 30.9022, 'TT': 1.0, 'TUSD': 2.0, 'UC': 3275.1091, 'UGAS': 22.4463, 'UIP': 55.4519, 'USDC': 2.0, 'USDT': 0.0, 'UTK': 30.7927, 'UUU': 903.4996, 'VET': 150.0, 'VIDY': 355.4043, 'VSYS': 1.0, 'WAN': 0.1, 'WAVES': 0.002, 'WAXP': 22.5933, 'WGP': 0.5, 'WICC': 5.0, 'WPR': 61.4628, 'WTC': 0.9086, 'WXT': 30.0, 'XEM': 4.0, 'XLM': 0.01, 'XMR': 0.01, 'XMX': 373.2554, 'XRP': 0.1, 'XTZ': 0.1, 'XVG': 0.1, 'XZC': 0.1, 'YCC': 45.6621, 'YEE': 498.0151, 'ZEC': 0.001, 'ZEN': 0.002, 'ZIL': 73.8584, 'ZJLT': 1392.2518, 'ZLA': 61.0646, 'ZRX': 1.9416}
                }

        # for coin in coinlist['data']:
        #         if len(coin['chains']) > 0 and coin['chains'][0]['withdrawFeeType']=='fixed':
        #                         jsonResult['transfer'][coin['currency'].upper()] = float(coin['chains'][0]['transactFeeWithdraw'])
        #         elif len(coin['chains']) > 0: 
        #                 jsonResult['transfer'][coin['currency'].upper()] = float(coin['chains'][0]['minTransactFeeWithdraw'])

        return jsonResult
    except Exception as ex : 
        print(str(ex) + '   at GetFeePrice()')





def decress_tick(digit):
        digit=float(digit)
        count =0
        newdigit=""
        digit = str(digit)
        lendigit = len(digit)
        lastdigit = digit[lendigit-1]
        decress  = int(lastdigit)
  
        if decress != 0 :
                decress = decress-1
                decress = str(decress)
                for i in digit:
                        count = count+1
      
                        if count == lendigit:
                                newdigit = newdigit + decress
                                
                        else :
                                newdigit = newdigit + i

                digit = float(newdigit)
        return digit

def APICoinlistConvertToStandart(data):
        preData = json.loads(data)
        return json.dumps( preData['tick'])

def getMinimumBuySource( fakeParams='',tragetSymbol=''):
        try:
                jsonz = req.get('https://api.huobi.pro/v2/reference/currencies').text
                coinlist = json.loads(jsonz)
                valueReturn = 0

                for coin in coinlist['data']:
                        if coin['currency'].upper()==tragetSymbol:
                                valueReturn = float(coin['chains'][0]['minWithdrawAmt'])
                                break
                      

                return valueReturn
        except Exception as ex : 
                print(str(ex) + '   at GetFeePrice()')


def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')


def getCoinInfo(sourceSymbol , tragetSymbol = ""):
   try:
     sourceSymbol = sourceSymbol.lower()
     tragetSymbol = tragetSymbol.lower()

     if tragetSymbol != "":
        symbol = tragetSymbol + sourceSymbol
        symbol = symbol.lower()
        r = req.get('https://api.huobi.pro/v1/common/symbols')
        checkCount = 0
        jsonReq = json.loads(r.text)
        if 'data' in jsonReq:
                for item in jsonReq['data']:
                        if item['symbol'] == symbol :
                                checkCount = checkCount + 1
                        if item['state'] != "online" :
                                return False
        else:
                return False

        if checkCount < 1:
                return False
     

     r1 = req.get('https://api.huobi.pro/v2/reference/currencies')
     jsonReq1 = json.loads(r1.text)
     checkCount = 0
     numCheck = 2
     if tragetSymbol == "":
        numCheck = 1 

     if 'data' in jsonReq1 :
         for item2 in jsonReq1['data']:
            if item2['currency'] == sourceSymbol or  item2['currency'] == tragetSymbol:
                checkCount = checkCount + 1
                if item2['chains'][0]['depositStatus'] != 'allowed' or  item2['chains'][0]['withdrawStatus'] != 'allowed':
                        return False
     else:
        return False 

     if checkCount < numCheck:
        return False

     return True
   except Exception as ex:
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+', '+tragetSymbol+')')
    return False 


# def GetHuobiAccount(symbol=""):
#   try:
#     addressAll = GetHuobiAccount(symbol)
#     address = addressAll['data'][0]['address']
#     tag = addressAll['data'][0]['addressTag']


#     return address , tag
#   except Exception as ex : 
#       print(str(ex) + ' at GetOkexAccount('+symbol+')')



#def GetAddress(coin):
 #       try:
  #              coin = coin.lower()
   #             getText = req.get("https://api.huobi.pro/v2/reference/currencies?currency="+coin)
    #            jsontext = json.loads(getText.text)
      #          return getText
     #   except Exception as ex :
       #         print(str(ex))



 
