import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
import json
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio
import LineNoty


BX_ID = 1
Binance_ID = 2

def getInitialPhase1( exchangeMoreId, exchangeLessId):
  try:
    data = {
        "exchangeMoreId" : exchangeMoreId,
        "exchangeLessId" : exchangeLessId
    }
    header = { 'Content-type': 'application/json; charset=utf-8'}
    getlink = req.post('http://149.28.154.112:4568/archelon/getInitialPhase1' , data=json.dumps(data) , headers =  header)
    jsonssss = json.loads(getlink.text)
    return jsonssss['result']
  except Exception as ex:
    print(str(ex) + ' at getInitialPhase1()')


def getPercentPhase1( exchangeMoreId, exchangeLessId):
  try:
    data = {
        "exchangeMoreId" : exchangeMoreId,
        "exchangeLessId" : exchangeLessId
    }
    header = { 'Content-type': 'application/json; charset=utf-8'}
    getlink = req.post('http://149.28.154.112:4568/archelon/getSettingInitialPhase1' , data=json.dumps(data) , headers =  header)
    jsonssss = json.loads(getlink.text)
    return jsonssss
  except Exception as ex:
    print(str(ex) + ' at getInitialPhase1()')



def getListCoinPhase1( exchangeMoreId, exchangeLessId):
  try:
    data = {
        "exchangeMoreId" : exchangeMoreId,
        "exchangeLessId" : exchangeLessId
    }
    header = { 'Content-type': 'application/json; charset=utf-8'}
    getlink = req.post('http://149.28.154.112:4568/archelon/getCoinInitialPhase1' , data=json.dumps(data) , headers =  header)
    jsonssss = json.loads(getlink.text)
    return jsonssss
  except Exception as ex:
    print(str(ex) + ' at getInitialPhase1()')
