import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
import json
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio
import ApiKeyExchange
import LineNoty
import ABTMain

BalanceTragetStartProcess = 0.0
BalanceSourceStartProcess = 0.0
isFirstTime = True


percentRatePhase = 3.00

def GetBalance(symbol): #GetBalance(symbol)
  try:

    if symbol =='POWR':
      symbol = 'POW'
    if symbol == 'BCHABC':
      symbol = 'BCH'
    if symbol == 'DASH':
      symbol = 'DAS'
    if symbol == 'DOGE':
      symbol = 'DOG'

    # -------------- get Balance
    #parameter use :  currency  ,  amount 
    time.sleep(3)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/balance/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h })
    jsonCoverted = json.loads(r.text)
    return float(jsonCoverted['balance'][symbol]['available'])
  except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')



def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance , isSaveRate = True):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
    if tragetSymbol == 'DOGE':
      tragetSymbol = 'DOG'

    global percentRatePhase
    resultBool = True
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    rateBuy = getRateForBuySaveRate(sourceSymbol, tragetSymbol ,percentRatePhase)

    if not isSaveRate:
      rateBuy = getRateForBuy(sourceSymbol, tragetSymbol)
    # if tragetSymbol in ABTMain.listTHBNeedConvertCoins:
    #   pairKey = getPairingId('BTC' , tragetSymbol )
    #   CalRate = getRateForSale('BTC', tragetSymbol )
    # else:
    #   pairKey = getPairingId(sourceSymbol , tragetSymbol )
    #   CalRate = getRateForSale(sourceSymbol, tragetSymbol )
    global BalanceTragetStartProcess
    global BalanceSourceStartProcess
    global isFirstTime
    amount = volumn
    if not isRebalance:
      time.sleep(3) #< ----------------- timing for waiting withdraw
      amount  = GetBalance(sourceSymbol)
      if BalanceSourceStartProcess  == 0 and isFirstTime:
         BalanceSourceStartProcess = amount
      
    time.sleep(2)

    if BalanceTragetStartProcess  == 0 and isFirstTime:
      time.sleep(3) #< ----------------- timing for waiting withdraw
      BalanceTragetStartProcess = GetBalance(tragetSymbol)

    if BalanceSourceStartProcess  == 0 and isFirstTime:
      time.sleep(3)
      BalanceSourceStartProcess = GetBalance(sourceSymbol)

    
    isFirstTime = False
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/order/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : pairing , 'type' : 'buy' , 'amount' : amount , 'rate' : rateBuy })
    orderJson = json.loads(r.text)
    print(r.text)
    time.sleep(3) #< ----------------- timing for waiting withdraw
    currentBalance = GetBalance(sourceSymbol)

    buyMinimal = getMinimumBuySource(sourceSymbol)
   # ToDo เช็คว่า มีการซื้อเซ็ตราคาไว้ไหม 
    if  currentBalance  > buyMinimal and not isRebalance:
      
      BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance , isSaveRate)
      
      # return True
    elif ( currentBalance - (BalanceSourceStartProcess-volumn) ) > buyMinimal and  isRebalance:
      
      fix_Balance = BalanceSourceStartProcess - volumn + buyMinimal 
      volumn_new = currentBalance - (BalanceSourceStartProcess-volumn)

      BuyOrderWithVolumn(sourceSymbol, tragetSymbol,volumn_new , fix_Balance , isSaveRate)
      
      time.sleep(3) #< ----------------- timing for waiting withdraw
      currentBalance = GetBalance(sourceSymbol)
      time.sleep(3)
      currentTragetBal = GetBalance(tragetSymbol)  
      balanceTragetEndProcess = currentTragetBal
      balanceSourceEndProcess = currentBalance
      isFirstTime = True
     
      Source = BalanceSourceStartProcess - balanceSourceEndProcess
      Target = balanceTragetEndProcess - BalanceTragetStartProcess
      Rate = 0.0
      if Source > 0:
        Rate = Source / Target
     
      
      LineNoty.notiBuyOrSell('Buy' , 'BX' , Source , sourceSymbol , Target , tragetSymbol , balanceSourceEndProcess ,balanceTragetEndProcess, Rate , getPriceBidBx(1) , getRateBinanceBid("BTCUSDT"))
      BalanceTragetStartProcess = 0.0
      BalanceSourceStartProcess = 0.0
      

    else :
      time.sleep(3) #< ----------------- timing for waiting withdraw
      currentTragetBal = GetBalance(tragetSymbol)  
      balanceTragetEndProcess = currentTragetBal
      balanceSourceEndProcess = currentBalance
      isFirstTime = True

     
      Source = BalanceSourceStartProcess - balanceSourceEndProcess
      Target = balanceTragetEndProcess - BalanceTragetStartProcess
      Rate = 0.0
      if Source > 0:
        Rate = Source / Target

     
      
      LineNoty.notiBuyOrSell('Buy' , 'BX' , Source , sourceSymbol , Target , tragetSymbol , balanceSourceEndProcess ,balanceTragetEndProcess, Rate , getPriceBidBx(1) , getRateBinanceBid("BTCUSDT"))
      BalanceTragetStartProcess = 0.0
      BalanceSourceStartProcess = 0.0


    return resultBool
  except Exception as ex : 
      isFirstTime = True
      BalanceTragetStartProcess = 0.0
      BalanceSourceStartProcess = 0.0  
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')



def getPriceBidBx(parId):
  try:
    r = req.get('https://bx.in.th/api/orderbook/?pairing='+str(parId))
    orderJson = json.loads(r.text)
    return float(orderJson['bids'][0][0] )
  except Exception as ex : 
    print(str(ex) + ' at getPriceBid()')
  
def getRateBinanceBid(symbol):
  try: 
   
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return float(jsonConvertedRate['bids'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getRate('+symbol+')')



def transferOrder(symbol,tragetAccount, addressTag=""):
  try:
    
    if symbol =='POWR':
      symbol = 'POW'
    if symbol == 'BCHABC':
      symbol = 'BCH'
    if symbol == 'DASH':
      symbol = 'DAS'
    if symbol == 'DOGE':
      symbol = 'DOG'
    
    resultBool = False
    if(symbol == "XRP"):
      _tragetAccount = tragetAccount + "?dt=" + addressTag
    elif symbol == "EOS":
      _tragetAccount = tragetAccount + "?memo=" + addressTag
    else:
      _tragetAccount = tragetAccount
    time.sleep(3) #< ----------------- timing for waiting withdraw
    BalanceSymbol = GetBalance(symbol)
    time.sleep(2)  
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest() 
    
    if BalanceSymbol > 0:
      r = req.post('https://bx.in.th/api/withdrawal/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'currency' : symbol  , 'amount' : BalanceSymbol , 'address' : _tragetAccount })
      jsonConvert = json.loads(r.text)
      print(r.text)
      resultBool = (jsonConvert['success'])
      time.sleep(3) #< ----------------- timing for waiting withdraw
      endingBalance = GetBalance(symbol)
      LineNoty.notiTranfer("Withdraw","BX","Binance" , BalanceSymbol ,endingBalance,symbol )

    return resultBool
  except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


def getPairingId(sourceSymbol , tragetSymbol):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
    if tragetSymbol == 'DOGE':
      tragetSymbol = 'DOG'
    r = req.post('https://bx.in.th/api/pairing/')
    jsonload = json.loads(r.text)
    for pair in range(1,35):
      if str(pair) in jsonload:
        if jsonload[str(pair)]['primary_currency'] == sourceSymbol and jsonload[str(pair)]['secondary_currency'] == tragetSymbol:
          return str(jsonload[str(pair)]['pairing_id'])
        if jsonload[str(pair)]['primary_currency'] == tragetSymbol and jsonload[str(pair)]['secondary_currency'] == sourceSymbol:
          return str(jsonload[str(pair)]['pairing_id'])

    return "Null"
  except Exception as ex : 
      print(str(ex) + ' at getPairingId('+sourceSymbol+','+tragetSymbol+')')

 


def getRateForBuy(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateBuy = float(jsonConvert['asks'][0][0]) * (110/100) 
    return rateBuy
  except Exception as ex : 
    print(str(ex) + ' at getRateForBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForBuySaveRate(sourceSymbol, tragetSymbol , saveRate ):
  try:
    time.sleep(1)
    saveRate = 100 + saveRate
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateBuy = float(jsonConvert['asks'][0][0]) * (saveRate/100) 
    return rateBuy
  except Exception as ex : 
    print(str(ex) + ' at getRateForBuy('+sourceSymbol+','+tragetSymbol+')')


def getRateForSale(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateSale = float(jsonConvert['bids'][0][0]) * (90/100) 
    return rateSale
  except Exception as ex : 
    print(str(ex) + ' at getRateForSale('+sourceSymbol+','+tragetSymbol+')')

def getRateForSaleSaveRate(sourceSymbol, tragetSymbol , saveRate ):
  try:
    time.sleep(1)
    saveRate = 100 - saveRate

    pairing = getPairingId(sourceSymbol , tragetSymbol)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateSale = float(jsonConvert['bids'][0][0]) * (saveRate/100) 
    return rateSale
  except Exception as ex : 
    print(str(ex) + ' at getRateForSale('+sourceSymbol+','+tragetSymbol+')')

def GetFeeTranferCost(symbol):
  try:
    if(symbol == 'ETH'):
      return 0.00500000

    if(symbol == 'XRP'):
      return 0.010000

    if(symbol == 'OMG'):
      return 0.20000000

    if(symbol == 'LTC'):
      return 0.00500000
  except Exception as ex : 
    print(str(ex) + ' at GetFeeTranferCost('+symbol+')')


def GetFeePrice():
  try:
    fee = {'tradeFee':0.25,
    'transfer':{
      'BTC':0.0005,
      'ETH':0.005,
      'XRP':0.01,
      'OMG':0.2,
      'LTC':0.005,
      'EOS':0.0001,
      'EVX':0.01,
      'POWR':0.01,
      'REP':0.01,
      'BCH':0.0001,
      'XZC':0.005,
      'DASH':0.005,
      'ZEC':0.005,
      'DOGE':5,
      'BSV':0.0001
      }
    
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')

def GetTradeStatus(sourceSymbol, tragetSymbol):
  try:
    r = req.post('https://bx.in.th/api/pairing/')
    jsonload = json.loads(r.text)
    for pair in jsonload:
      if pair['primary_currency'] == sourceSymbol and pair['secondary_currency'] == tragetSymbol:
        return pair['active']

    return False
  except Exception as ex : 
    print(str(ex) + ' at GetTradeStatus('+sourceSymbol+','+tragetSymbol+')')

def SellOrder(sourceSymbol , tragetSymbol , isSaveRate = True): # น่าจะถูกละ
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
    if tragetSymbol == 'DOGE':
      tragetSymbol = 'DOG'

    global BalanceTragetStartProcess
    global BalanceSourceStartProcess
    global isFirstTime
    global percentRatePhase

    key = ApiKeyExchange.BX_KEY
    api_secret = ApiKeyExchange.BX_SECRET
    time.sleep(3) #< ----------------- timing for waiting withdraw
    Balance = GetBalance(tragetSymbol)  
    time.sleep(2)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    resultBool= True

    
    
    signature = key + nonce + api_secret
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    
    CalRate =  0
    
    if tragetSymbol in ABTMain.listTHBNeedConvertCoins:
      pairKey = getPairingId('BTC' , tragetSymbol )
      CalRate = getRateForSaleSaveRate('BTC', tragetSymbol , percentRatePhase)
      if not isSaveRate:
        CalRate = getRateForSale('BTC', tragetSymbol )
    else:
      pairKey = getPairingId(sourceSymbol , tragetSymbol )
      CalRate = getRateForSaleSaveRate(sourceSymbol, tragetSymbol , percentRatePhase )
      if not isSaveRate:
        CalRate = getRateForSale(sourceSymbol, tragetSymbol )
    
    if BalanceTragetStartProcess  == 0 and isFirstTime:
      BalanceTragetStartProcess = Balance

    


    r = req.post('https://bx.in.th/api/order/', {'key': key ,'nonce': nonce,'signature': h , 'pairing' : int(pairKey) , 'type' : 'sell' , 'amount' : Balance , 'rate' : CalRate })
    orderJson = json.loads(r.text)
    orderID = orderJson['order_id']
    print(r.text)
    
    time.sleep(2)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/cancel/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : int(pairKey) , 'order_id' : orderID })
    resultBool =  False
    
    if orderJson['success'] == True:
      resultBool = True
    

    
    time.sleep(3) #< ----------------- timing for waiting withdraw
    currentBalance = GetBalance(tragetSymbol)


    if BalanceSourceStartProcess  == 0 and isFirstTime:
      BalanceSourceStartProcess = currentBalance


    isFirstTime = False
    
      

    miniSale = getMinimumBuySource(tragetSymbol)
    if currentBalance  > miniSale :
      time.sleep(1)
      SellOrder(sourceSymbol, tragetSymbol , isSaveRate)
      #LineNoty.notiBuyOrSell('Sell' , 'BX'  , Target , tragetSymbol , Source , sourceSymbol  ,balanceTragetEndProcess , balanceSourceEndProcess , Rate , getPriceBidBx(1) , getRateBinanceBid("BTCUSDT"))

      return True

    time.sleep(1)
    isFirstTime = True
    

   
    if resultBool :
      time.sleep(3) #< ----------------- timing for waiting withdraw
    currentTragetBal = GetBalance(tragetSymbol)  
    balanceTragetEndProcess = currentTragetBal
    balanceSourceEndProcess = currentBalance

    Source = balanceTragetEndProcess - BalanceTragetStartProcess
    Target = BalanceSourceStartProcess - balanceSourceEndProcess
    Rate = 0.0
    if Target > 0:
      Rate = Source / Target

      LineNoty.notiBuyOrSell('Sell' , 'BX'  , Target , tragetSymbol , Source , sourceSymbol  ,balanceTragetEndProcess , balanceSourceEndProcess , Rate , getPriceBidBx(1) , getRateBinanceBid("BTCUSDT"))
    BalanceTragetStartProcess = 0.0
    BalanceSourceStartProcess = 0.0


    return resultBool 
  except Exception as ex : 
    isFirstTime = True
    BalanceTragetStartProcess = 0.0
    BalanceSourceStartProcess = 0.0 
    print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')


 
def CheckPendingOrder(symbol):#CheckPendingOrder()
  try:
    time.sleep(1)
    
    
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    #print(nonce)

    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()


    r = req.post('https://bx.in.th/api/withdrawal-history/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h })

    jsonConvert = json.loads(r.text)
    
    for withdraw in jsonConvert['withdrawals'][:10]:
      if withdraw['currency'] == symbol.upper() :
        return withdraw['withdrawal_status'] 
      
    return 'null'
  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')  



def getMinimumSellSource(sourceSymbol):
  try:
    getText = req.get("https://bx.in.th/api/pairing/")
    jsonConvertedRate = json.loads(getText.text)
    for minQt in jsonConvertedRate:
      if minQt['primary_currency'] == sourceSymbol :
        return minQt['primary_min']
      
      if minQt['secondary_currency'] == sourceSymbol:
        return minQt['secondary_min']

    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')  



def getMinimumBuySource(tragetSymbol):
  try:
    getText = req.get("https://bx.in.th/api/pairing/")
    jsonConvertedRate = json.loads(getText.text)
    for pair in range(1,35):
      if str(pair) in jsonConvertedRate:
        if jsonConvertedRate[str(pair)]['primary_currency'] == tragetSymbol:
          return float(jsonConvertedRate[str(pair)]['primary_min'])
      
        if jsonConvertedRate[str(pair)]['secondary_currency'] == tragetSymbol:
          return float(jsonConvertedRate[str(pair)]['secondary_min'])


    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+')')  








def BuyOrderWithVolumn(sourceSymbol, tragetSymbol,volumn,fix_Balance , isSaveRate = True):
  try:
    pairing = getPairingId(sourceSymbol , tragetSymbol)
    rateBuy = getRateForBuy(sourceSymbol, tragetSymbol )
    amount = volumn
    time.sleep(1)
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/order/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : pairing , 'type' : 'buy' , 'amount' : amount , 'rate' : rateBuy })
    orderJson = json.loads(r.text)
    time.sleep(3) #< ----------------- timing for waiting withdraw
    BalanceSymbol = GetBalance(sourceSymbol)

    #orderID = orderJson['order_id']
    if BalanceSymbol > fix_Balance :
       time.sleep(1)
       BuyOrderWithVolumn(sourceSymbol, tragetSymbol, (BalanceSymbol - fix_Balance) ,fix_Balance)
       time.sleep(1) 


  except Exception as ex : 
      print(str(ex) + ' at BuyOrderWithVolumn('+sourceSymbol+','+tragetSymbol+','+volumn+')')




def getRateForBuySwap(sourceSymbol, tragetSymbol , pairing):
  try:
    time.sleep(1)
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateBuy = float(jsonConvert['asks'][0][0]) * (105/100) 
    return rateBuy
  except Exception as ex :
    print(str(ex) + ' at getRateForBuySwap('+sourceSymbol+','+tragetSymbol+')')


def getRateForBuySwapSaveRate(sourceSymbol, tragetSymbol , pairing , saveRate):
  try:
    time.sleep(1)
    saveRate = 100 + saveRate
    r = req.post(url='https://bx.in.th/api/orderbook/?pairing='+pairing)
    jsonConvert = json.loads(r.text)
    rateBuy = float(jsonConvert['asks'][0][0]) * (saveRate/100) 
    return rateBuy
  except Exception as ex :
    print(str(ex) + ' at getRateForBuySwap('+sourceSymbol+','+tragetSymbol+')')

def BuyOrderSwap(sourceSymbol, tragetSymbol , amountCoin , isSaveRate = True):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
    if tragetSymbol == 'DOGE':
      tragetSymbol = 'DOG'

    global BalanceTragetStartProcess
    global BalanceSourceStartProcess
    global percentRatePhase

    balance = GetBalance(sourceSymbol)
    BalanceSourceStartProcess = balance
    time.sleep(2)
    BalanceTragetStartProcess = GetBalance(tragetSymbol)

    pairing = getPairingId(sourceSymbol , tragetSymbol)

    rateBuy = getRateForBuySwapSaveRate(sourceSymbol, tragetSymbol , pairing , percentRatePhase)
    if not isSaveRate:
      rateBuy = getRateForBuySwap(sourceSymbol, tragetSymbol , pairing)
    
    amountForBuy = amountCoin * rateBuy

    if balance < amountForBuy:
      amountForBuy = balance
      amountCoin = balance / rateBuy
    
    nonce = datetime.now().strftime('%Y%m%d%H%M%S')
    signature = ApiKeyExchange.BX_KEY + nonce + ApiKeyExchange.BX_SECRET
    h = hashlib.sha256(signature.encode('utf-8')).hexdigest()
    r = req.post('https://bx.in.th/api/order/', {'key': ApiKeyExchange.BX_KEY ,'nonce': nonce,'signature': h , 'pairing' : pairing , 'type' : 'buy' , 'amount' : amountForBuy , 'rate' : rateBuy })
    orderJson = json.loads(r.text)


    balanceTragetEndProcess = GetBalance(tragetSymbol)
    time.sleep(2)
    balanceSourceEndProcess = GetBalance(sourceSymbol)

    Source = BalanceSourceStartProcess - balanceSourceEndProcess
    Target = balanceTragetEndProcess - BalanceTragetStartProcess
    Rate = 0.0
    if Source > 0:
        Rate = Source / Target

    LineNoty.notiBuyOrSell('Buy' , 'BX' , Source , sourceSymbol , Target , tragetSymbol , balanceSourceEndProcess ,balanceTragetEndProcess, Rate , getPriceBidBx(1) , getRateBinanceBid("BTCUSDT"))

    BalanceTragetStartProcess = 0.0
    BalanceSourceStartProcess = 0.0

    if not orderJson['success']:
      return False , 0

    return True , amountCoin
  except Exception as ex :
    print(str(ex) + ' at BuyOrderSwap('+sourceSymbol+','+tragetSymbol+','+amountCoin+')')