import hashlib
import hmac
import json
import requests as req
import LineNoty
import time
import Bitfinex

# API info
API_HOST = 'https://api.bitkub.com'
API_KEY = '3e4df3b1e283f014f859519fef707076'
API_SECRET = b'5e8f2e84d6fcbdf77e242e6a4d463a09'
lmtQuery = '1'
percentSaveRate = 3
usdtRate = Bitfinex.getRateUsdUsdt()

def json_encode(data):
	return json.dumps(data, separators=(',', ':'), sort_keys=True)

def sign(data):
	j = json_encode(data)
	# print('Signing payload: ' + j)
	h = hmac.new(API_SECRET, msg=j.encode(), digestmod=hashlib.sha256)
	return h.hexdigest()

def GetBalance(symbol): #GetBalance(symbol)
  try:

    if symbol =='POWR':
      symbol = 'POW'
    if symbol == 'BCHABC':
      symbol = 'BCH'
    if symbol == 'DASH':
      symbol = 'DAS'
     

    response = req.get(API_HOST + '/api/servertime')
    ts = int(response.text)
    header = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'X-BTK-APIKEY': API_KEY,
    }
    data = {
        'ts': ts,
    }
    signature = sign(data)
    data['sig'] = signature
    
    response = req.post(API_HOST + '/api/market/balances', headers=header, data=json_encode(data))

    resJson = json.loads(response.text)
   
    return float(resJson['result'][symbol]['available'])
  except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')

def getRateBinanceBid(symbol):
  try: 
   
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return float(jsonConvertedRate['bids'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getRate('+symbol+')')



def getRateForBuy(_symbol , percentRate = 3):
    try:
        symbol = 'THB_'+_symbol
        r = req.get( API_HOST + '/api/market/asks?sym='+ symbol + '&lmt='+lmtQuery)
        jsonConvert = json.loads(r.text)
        _percentRate = 100 + percentRate
        rateBuy = float(jsonConvert['result'][0][3]) * (_percentRate/100)
        return rateBuy
    except Exception as ex : 
        print(str(ex) + ' at getRateForBuy('+_symbol+')')
 
def getRateForSell(_symbol , percentRate = 3):
    try:
        symbol = 'THB_'+_symbol
        r = req.get( API_HOST + '/api/market/bids?sym='+ symbol + '&lmt='+lmtQuery)
        jsonConvert = json.loads(r.text)
        _percentRate = 100 - percentRate
        rateBuy = float(jsonConvert['result'][0][3]) * (_percentRate/100)
        return rateBuy
    except Exception as ex : 
        print(str(ex) + ' at getRateForSell('+_symbol+')')

def getPriceBidBitkub(_symbol):
    try:
        symbol = 'THB_'+_symbol
        r = req.get( API_HOST + ' /api/market/bids?sym='+ symbol + '&lmt='+lmtQuery)
        jsonConvert = json.loads(r.text)
        rateBuy = float(jsonConvert['result'][0][0])
        return rateBuy
    except Exception as ex : 
        print(str(ex) + ' at getRateForBuy('+_symbol+')')



def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance , isSaveRate = True):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
    

    resultBool = False
    sym = sourceSymbol+'_'+tragetSymbol
    typ = 'market'
    if isSaveRate:
        typ = 'limit'

    amt = volumn
    rat = int(getRateForBuy(tragetSymbol))
    if not isRebalance:
        amt = int(GetBalance(sourceSymbol))

    response = req.get(API_HOST + '/api/servertime')
    ts = int(response.text)
    header = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'X-BTK-APIKEY': API_KEY,
    }
    data = {
        'ts': ts,
        'sym': sym,
        'amt': amt,
        'rat': rat,
        'typ' : typ
    }

    signature = sign(data)
    data['sig'] = signature
    
    response = req.post(API_HOST + '/api/market/place-bid', headers=header, data=json_encode(data))
    resJson = json.loads(response.text)

    LineNoty.notiBuyOrSell('Buy' , 'Bitkub' , resJson["result"]["amt"] , sourceSymbol , resJson["result"]["rec"] , tragetSymbol , getRateForBuy(sourceSymbol) ,getRateForBuy(tragetSymbol), resJson["result"]["rat"] , getPriceBidBitkub('BTC') , getRateBinanceBid("BTCUSDT"))
    resultBool = True


    return resultBool
  except Exception as ex :
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')

def SellOrder(sourceSymbol, tragetSymbol , isSaveRate = True):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
   

    resultBool = False
    sym = sourceSymbol+'_'+tragetSymbol
    typ = 'market'
    if isSaveRate:
        typ = 'limit'

    amt = GetBalance(tragetSymbol)
    rat = int(getRateForSell(tragetSymbol))

    response = req.get(API_HOST + '/api/servertime')
    ts = int(response.text)
    header = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'X-BTK-APIKEY': API_KEY,
    }
    data = {
        'ts': ts,
        'sym': sym,
        'amt': amt,
        'rat': rat,
        'typ' : typ
    }

    signature = sign(data)
    data['sig'] = signature
    
    response = req.post(API_HOST + '/api/market/place-ask', headers=header, data=json_encode(data))
    resJson = json.loads(response.text)

    LineNoty.notiBuyOrSell('Sell' , 'Bitkub' , resJson["result"]["rec"] , tragetSymbol , resJson["result"]["amt"] , sourceSymbol , getRateForBuy(tragetSymbol), getRateForBuy(sourceSymbol) , resJson["result"]["rat"] , getPriceBidBitkub('BTC') , getRateBinanceBid("BTCUSDT"))
    resultBool = True


    return resultBool
  except Exception as ex :
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')


def GetAccount(symbol ,exchange=""):
  try:
    address = ""
    tag = ""
    if symbol == "BTC":
      address = "3591Gh53BAxuziBJznuBeiGMtoN8m5QVVi"
    elif symbol == "ETH":
      address = "0x439ecde4d680e847b6d42c0657b1e77cbd9dcc47"
    elif symbol == "LTC":
      address = "LNF2mgTeXVRh6VyAAiy1nnhypSxi9Y6MY3"
    elif symbol == "XRP":
      address = "rpXTzCuXtjiPDFysxq8uNmtZBe9Xo97JbW"
      tag = "1015713553"
    elif symbol == "USDT":
      address = "0x439ecde4d680e847b6d42c0657b1e77cbd9dcc47"


    return  address , tag
  except Exception as ex : 
        print(str(ex) + ' at GetBitfinexAccount('+symbol+')')


def BuyOrderSwap(sourceSymbol, tragetSymbol , amountCoin , isSaveRate = True):
  try:
    if tragetSymbol =='POWR':
      tragetSymbol = 'POW'
    if tragetSymbol == 'BCHABC':
      tragetSymbol = 'BCH'
    if tragetSymbol == 'DASH':
      tragetSymbol = 'DAS'
     

    resultBool = False
    sym = tragetSymbol
    typ = 'market'
    if isSaveRate:
        typ = 'limit'


    balance = int(GetBalance('THB'))
    rat = int(getRateForBuy(tragetSymbol))
    amt = int(amountCoin * rat)

    if balance < amt:
      amt = balance
      amountCoin = balance / rat

    response = req.get(API_HOST + '/api/servertime')
    ts = int(response.text)
    header = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'X-BTK-APIKEY': API_KEY,
    }
    data = {
        'ts': ts,
        'sym': sym,
        'amt': amt,
        'rat': rat,
        'typ' : typ
    }

    signature = sign(data)
    data['sig'] = signature
    
    response = req.post(API_HOST + '/api/market/place-bid', headers=header, data=json_encode(data))
    resJson = json.loads(response.text)

    LineNoty.notiBuyOrSell('Buy' , 'Bitkub' , resJson["result"]["amt"] , sourceSymbol , resJson["result"]["rec"] , tragetSymbol , getRateForBuy(sourceSymbol) ,getRateForBuy(tragetSymbol), resJson["result"]["rat"] , getPriceBidBitkub('BTC') , getRateBinanceBid("BTCUSDT"))
    resultBool = True


    return resultBool
  except Exception as ex :
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')


def transferOrder(symbol,tragetAccount, addressTag=""):
  try:
    if symbol =='POWR':
      symbol = 'POW'
    if symbol == 'BCHABC':
      symbol = 'BCH'
    if symbol == 'DASH':
      symbol = 'DAS'
    

    amt = GetBalance(symbol)
    time.sleep(1)
    response = req.get(API_HOST + '/api/servertime')
    ts = int(response.text)
    header = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-BTK-APIKEY': API_KEY,
    }

    data = {
        "ts": ts ,
        "cur" : symbol ,
        "amt" : amt ,
        "adr" : tragetAccount ,
        "mem" : addressTag ,
        
    }
    signature = sign(data)
    data['sig'] = signature

    response = req.post(API_HOST + '/api/crypto/withdraw', headers=header, data=json_encode(data))
 
    resJson = json.loads(response.text)
    
    return float(resJson['result'][symbol]['available'])
  except Exception as ex :
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag + ' )')

def GetFeePrice():
  try:
    fee = {'tradeFee':0.25,
    'transfer':{
      'BTC':0.0005,
      'ETH':0.005,
      'XRP':0.01,
      'OMG':0.2,
      'LTC':0.005,
      'EOS':0.0001,
      'EVX':0.01,
      'POWR':0.01,
      'REP':0.01,
      'BCH':0.0001,
      'XZC':0.005,
      'DASH':0.005,
      'ZEC':0.005,
      'DOGE':5,
      'BSV':0.0001
      }
    
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')






 
def getExchangeRate(forBX=""):
  try:
    getlink = req.get('https://api.exchangeratesapi.io/latest?symbols=USD,THB') #USDTHB
    jsonssss = json.loads(getlink.text)
    if forBX =="rebalace":
       return  (jsonssss['rates']['THB'] / jsonssss['rates']['USD'] )*(usdtRate)

    return ( (jsonssss['rates']['THB'] / jsonssss['rates']['USD']) -0.3) *(usdtRate)
  except Exception as ex:
    print(str(ex) + ' at getExchangeRate()')


def APICoinlistConvertToStandart(coin):
  usdtConvert = getExchangeRate()
  bid = []
  ask = []
  result = {}
  urlBid = req.get('https://api.bitkub.com/api/market/bids?sym='+coin+'&lmt=100').text
  urlAsk = req.get('https://api.bitkub.com/api/market/asks?sym='+coin+'&lmt=100').text
  preDataBid = json.loads(urlBid)
  preDataAsk = json.loads(urlAsk)

  for askData in preDataAsk['result']: #[2726255,1572103792,27147.05,274299,0.09896886]
    ask.append([askData[3] / usdtConvert ,askData[4]])

  for bidData in preDataBid['result']:  
    bid.append([bidData[3] / usdtConvert ,bidData[4]])

  result = {'bids':bid,'asks':ask}
  return json.dumps( result)

def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')

