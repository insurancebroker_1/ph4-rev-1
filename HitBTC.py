from Lib.hitbtcapi.client import Client
import requests
import requests as req
import json
import time
import ApiKeyExchange
import LineNoty

key =   ApiKeyExchange.HitBTC_Key
secret = ApiKeyExchange.HitBTC_Secret
EXCHANGE_NAME = "HitBTC"

# key ='KW07Gw14OecFLdkEt3RWZbK+AeTCrsPU'
# secret = '6f6ouZD8faOKlKLyJLCr6ER4qZ4rKq0Y'
client = Client(key,secret)

def GetBalance(symbol):
    #Example
    #           balance = GetBalance('XRP')
    #           return float
    transFerSuccess = True
    symbol = symbol.upper()
    
    try :
            if symbol == 'USDT':
                symbol = 'USD'

            if not transferBankToExchange(symbol):
                transFerSuccess = False
            
            time.sleep(5)
            all_balance =  client.get_trading_balance()
            for item in all_balance:
                    if symbol == item['currency']:
                        return float(item['available'])


            return 0
    except Exception as ex :
            print(str(ex)+'error at Getbalance'+'('+symbol+')')
            return 0

def checkAndBuyUSDT():
    try:
        amount = GetBalance('USDT20')
        minimum_size  = float(client.get_symbol('USDUSDT20')['quantityIncrement'])
        if amount > minimum_size:
            BuyOrder('USDT20', 'USD' , 0 , False)


        return True
    except Exception as ex :
        print(str(ex)+'error at checkAndBuyUSDTs()')
        return False

def GetBalanceTest(symbol):
    #Example
    #           balance = GetBalance('XRP')
    #           return float
    
    transFerSuccess = True
    symbol = symbol.upper()
    if symbol == 'USDT':
            symbol = 'USD'
    try :
            if not transferBankToExchange(symbol):
                transFerSuccess = False
            
            time.sleep(5)
            all_balance =  client.get_trading_balance()
            for item in all_balance:
                    if symbol == item['currency']:
                        return float(item['available']) , True , transFerSuccess


            return 0 , False , transFerSuccess 
    except Exception as ex :
            print(str(ex)+'error at Getbalance'+'('+symbol+')')
            return 0 , False , transFerSuccess


def GetBalanceFund(symbol):
    #Example
    #           balance = GetBalance('XRP')
    #           return float
    time.sleep(5)
    symbol = symbol.upper()
    if symbol == 'USDT':
            symbol = 'USD'
    try :
            all_balance =  client.get_account_balance( )
            for item in all_balance:
                    if symbol == item['currency']:
                        return float(item['available']) , True
            
            return 0 , False 
    except Exception as ex :
            print(str(ex)+'error at Getbalance'+'('+symbol+')')
            return 0 , False 

def GetBalanceExchange(symbol):
    #Example
    #           balance = GetBalance('XRP')
    #           return float
    time.sleep(5)
    symbol = symbol.upper()
    if symbol == 'USDT':
            symbol = 'USD'
    try :
            all_balance =  client.get_trading_balance( )
            for item in all_balance:
                    if symbol == item['currency']:
                        return float(item['available'])
    except Exception as ex :
            print(str(ex)+'error at Getbalance'+'('+symbol+')')

def GetAccount(symbol , exchange=""):
    try :
        symbol = symbol.upper()
        exchange = exchange.upper()
        if symbol == 'USDT':
            symbol = 'USDT20'
            if exchange == 'KRAKEN':
                symbol = 'USD'

        all_balance =  client.get_deposit_address(symbol)
        address =''
        tag =''
        if 'address' in all_balance:
            address = all_balance['address']

        if 'paymentId' in all_balance:
            tag = all_balance['paymentId']


        return address , tag
    except Exception as ex : 
        print(str(ex) + ' at GetAccount('+symbol+')')
            
        
def GetFeePrice():
    
    try:
        assetInfo = client.get_currencies()
        feeArray = {}

        for coin in assetInfo:
            feeArray[coin["id"]] = 0
            if "payoutFee" in coin:
                feeArray[coin["id"]] = float(coin["payoutFee"])
            

        fee = {'tradeFee':0.2,
                'transfer':feeArray
                }

        return fee
    except Exception as ex :
        print(str(ex) + ' at GetFeePrice()')


# def GetTradeStatus(tragetSymbol):
#         #[Example]
#         #               status =  GetTradeStatus('XRP')
#         #               return boolean
#         if symbol == 'USDT':
#             symbol = 'USD'    
#         try :
#                return client.get_currency(tragetSymbol)['transferEnabled']
#         except Exception as ex:
#                 print(str(ex)+'at GetTradeStatus('+tragetSymbol+')')



                

##transferOrder
# def CheckPendingOrder(symbol):
#     # [Example] CheckPendingOrder('XRP')
#         #           return boolean
#         #           False คือ ไม่มี pending  success
#         #           True  คือ created, pending, failed หรือ responseกลับมา arraysล่าสุด มี currency คนละชื่ออกับ symbol
#         #           มีการ print แสดง status
#         if symbol == 'USDT':
#             symbol = 'USD'
#         try :   
#                hisw =  client.get_account_transactions()
#                if hisw[0]['currency'] != symbol:
#                    return True
#                Val_Pending = hisw[0]['status']
#                if str(Val_Pending) == "success" :
#                    return False
#                else :
#                    return True
               
                
#         except Exception as ex :
#                       print(str(ex) + ' at CheckPendingOrder('+symbol+')')

def transferExchangeToBank(symbol):
    try:
        getAmount = GetBalanceExchange(symbol)
        if getAmount > 1:
            symbol = symbol.upper()
            if symbol == 'USDT':
                symbol = 'USD'
            data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                                                                            currency = symbol,
                                                                            amount = getAmount  ,
                                                                            type = 'exchangeToBank'
                                                                            )
            if 'id' not in data_transfer_tragetSymbol_to_main:
                return False
            
            return True  


        return False
    except Exception as ex:
        print(str(ex) , "at transferExchangeToBank(" +symbol+")")
        return False

def transferBankToExchange(symbol):
    try:
        symbol = symbol.upper()
        minimum_size = 0
        if symbol == 'USDT':
            symbol = 'USD'
            checkAndBuyUSDT()
            minimum_size  = float(client.get_symbol('USDUSDT20')['quantityIncrement'])
            

        getAmount , checkSuccess = GetBalanceFund(symbol)

        

        
        if getAmount > minimum_size:
           
            data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                                                                            currency = symbol,
                                                                            amount = getAmount  ,
                                                                            type = 'bankToExchange'
                                                                            )
            if 'id' not in data_transfer_tragetSymbol_to_main:
                return False

            
            return True

        return False
    except Exception as ex:
        print(str(ex) , "at transferExchangeToBank(" +symbol+")")
        return False

def SellOrder(sourceSymbol,tragetSymbol) :
    # Note *** ใน HitBTC  ทั้ง BTC LTC ETH มีซื้อขายแบบ USDT แต่ใน api ใช้อักษร BTC/USD แทน ซึ่งมีความหมายเดียวกับ = BTC/USDT
    #           XRP ใช้ XRP/USDT ตรวจสอบได้ใน def check  และใน HitBTC ไม่ใช้ USD 
    try :
        sourceSymbol = sourceSymbol.upper()
        tragetSymbol = tragetSymbol.upper()
        qty = GetBalance(tragetSymbol)
        if qty <= 0:
            print('not have money in Main Account')
            return False
        
        # main_to_exchange = client.transfer_to_trading(currency = tragetSymbol , amount = qty , type = 'bankToExchange')
        time.sleep(2.2)
        if sourceSymbol == 'USDT':
            symbol_must_convert = ['ETH' , 'BTC' , 'LTC']
            if (tragetSymbol in symbol_must_convert):
                sourceSymbol = 'USD'


        market = tragetSymbol + sourceSymbol
        bid_price = float(client.get_ticker(market)['bid'])
        minimum_trade_size  = float(client.get_symbol(market)['quantityIncrement'])
        if qty < minimum_trade_size :
            print('Quantity < minimum trade size')
            return False
        status = client.create_order(symbol=market,side ='sell',quantity=qty,price=bid_price , type="market")
        data_report = status['tradesReport'][0]
        if tragetSymbol == 'USDT':
            tragetSymbol = 'USD'
        AMOUNT_TRAGET_SYMBOL = float(data_report['quantity'])*float(data_report['price'])-float(data_report['fee'])
        # data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
        #                                                                 currency = tragetSymbol,
        #                                                                 amount = AMOUNT_TRAGET_SYMBOL  ,
        #                                                                 type = 'exchangeToBank'
        #                                                                 )
        if status['status'] == 'filled':
            LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(qty))
            return True

        return False
    except Exception as ex:
        print(str(ex) , "at SellOrder(" +sourceSymbol+","+tragetSymbol+")")
        return False

def BuyOrder(sourceSymbol, tragetSymbol,volume,isRebalance):
        #[Example]
        #               checkboolean = BuyOrder('USDT', 'XRP',5555,True)
        #               checkboolean = BuyOrder('USDT', 'XRP',False)
        #               return boolean
        try :
            symbol_convert = sourceSymbol
            symbolChange = sourceSymbol
            amountToBuy = 0
            if symbolChange == 'USDT':
                symbolChange = 'USD'

            symbol_must_convert = ['ETH' , 'BTC' , 'LTC']
            if (tragetSymbol in symbol_must_convert) and sourceSymbol =='USDT':
                sourceSymbol = 'USD'
            
            my_balance = GetBalance(symbol_convert)
            if my_balance <= 0:
                print('not have money in Main Account')
                return False
            data_stock_volum = 0
            i=0
            market = tragetSymbol+sourceSymbol
            minimum_trade_size  = float(client.get_symbol(market)['quantityIncrement'])




            if isRebalance == True:
                if my_balance < volume:
                    print('money not enough in Main Account')
                    return False
                # main_to_exchange = client.transfer_to_trading(currency = symbolChange,amount = volume,type = 'bankToExchange')
                time.sleep(2.8)
                if minimum_trade_size >volume  :
                    print('volume < minimum trade size')
                    return False
                while True :
                    order_book_ask = client.get_orderbook(market)['ask']
                    rate = float(order_book_ask[i]['price'])
                    qty_from_order  = float(order_book_ask[i]['size'])
                    value_order     = qty_from_order*rate
                   
                    if value_order >= volume:
                        qty = volume/rate
                        amountToBuy += qty
                        status = client.create_order(symbol=market,side ='buy',quantity=qty,price=rate , type="market")
                        time.sleep(2.8)
                        if status['status'] == 'filled':
                            data_report = status['tradesReport'][0]
                            AMOUNT_TRAGET_SYMBOL = float(data_report['quantity'])

                            # if i != 0 :
                            #     data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                            #                                                                         currency = tragetSymbol,
                            #                                                                         amount = data_stock_volum  ,
                            #                                                                         type = 'exchangeToBank'
                            #                                                                     )
                            # else :
                            
                            #     data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                            #                                                                         currency = tragetSymbol,
                            #                                                                         amount = AMOUNT_TRAGET_SYMBOL  ,
                            #                                                                         type = 'exchangeToBank'
                            #                                                                     )
                            LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                            return True
                        
                    else :
                        amountToBuy += qty_from_order
                        status = client.create_order(symbol=market,side ='buy',quantity=qty_from_order,price=rate , type="market")
                        time.sleep(2.8)
                        if status['status'] == 'filled':
                            data_report = status['tradesReport'][0]
                            AMOUNT_TRAGET_SYMBOL = float(data_report['quantity'])
                            data_stock_volum = data_stock_volum + AMOUNT_TRAGET_SYMBOL
                        volume = volume - value_order
                        i=i+1
            else :
                    # main_to_exchange = client.transfer_to_trading(currency = symbolChange,amount = my_balance,type = 'bankToExchange')
                    # time.sleep(2.8)
                    if minimum_trade_size >my_balance  :
                        print('volume < minimum trade size')
                        return False
                    while True :
                        order_book_ask = client.get_orderbook(market)['ask']
                        rate = float(order_book_ask[i]['price'])
                        qty_from_order  = float(order_book_ask[i]['size'])
                        value_order     = qty_from_order*rate
                       
                        if value_order >= my_balance:
                            qty = (my_balance/rate)*0.998
                            amountToBuy += qty
                            status = client.create_order(symbol=market,side ='buy',quantity=qty,price=rate , type="market")
                
                            time.sleep(2.8)
                            if status['status'] == 'filled':
                                data_report = status['tradesReport'][0]
                                AMOUNT_TRAGET_SYMBOL = float(data_report['quantity'])
                                # if i != 0 :
                                #     data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                                #                                                                         currency = tragetSymbol,
                                #                                                                         amount = data_stock_volum  ,
                                #                                                                         type = 'exchangeToBank'
                                #                                                                     )
                                # else :
                                
                                #     data_transfer_tragetSymbol_to_main = client.transfer_to_trading(
                                #                                                                         currency = tragetSymbol,
                                #                                                                         amount = AMOUNT_TRAGET_SYMBOL  ,
                                #                                                                         type = 'exchangeToBank'
                                #                                                                     )
                                LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                                return True
                        else :
                            amountToBuy += qty_from_order
                            status = client.create_order(symbol=market,side ='buy',quantity=qty_from_order,price=rate , type="market")
                            time.sleep(2.8)
                            if status['status'] == 'filled':
                                data_report = status['tradesReport'][0]
                                AMOUNT_TRAGET_SYMBOL = float(data_report['quantity'])
                                data_stock_volum = data_stock_volum + AMOUNT_TRAGET_SYMBOL
                            my_balance = my_balance - value_order
                            i=i+1
                
                        
                        
    
                
        except Exception as ex :
                print(str(ex)+"at BuyOrder(",sourceSymbol+","+tragetSymbol+","+str(isRebalance)+","+str(volume)+")")
                return False


    
# def check():#อันนี้ไม่เกี่ยว
#     b =client.get_tickers()
#     i=0
#     for s in b:
#         if b[i]['symbol'][0:6]=='BTCUSD':
#             print(b[i]['symbol'])
#         if b[i]['symbol'][0:6]=='LTCUSD':
#             print(b[i]['symbol'])
#         if b[i]['symbol'][0:6]=='ETHUSD':
#             print(b[i]['symbol'])
#         if b[i]['symbol'][0:6]=='XRPUSD':
#             print(b[i]['symbol'])
#         i=i+1


def transferOrder(symbol,tragetAccount, addressTag="" , exchange=""):
    #status = client.withdraw_('XRP',3,'rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1015165964')
    exchange = exchange.upper()
    if symbol == 'USDT':
        symbol = 'USDT20'
        if exchange == "KRAKEN":
            symbol = 'USD'
        else:
            SellOrder('USDT20','USD')

    
    
    try :
        transferExchangeToBank(symbol)
        my_amount , checkSuccess = GetBalanceFund(symbol)
        data = client.get_currency(symbol)
        fee  = float(data['payoutFee'])
        amountAfterFee = my_amount-fee
        if amountAfterFee <= 0 :
            return False
        if addressTag != "" :
            status = client.withdraw_(symbol,my_amount,tragetAccount,addressTag)
        else:
            status = client.withdraw(currency = symbol,amount=my_amount,address=tragetAccount)
    
        if  isinstance(status['id'], str) :
            LineNoty.notiForTransfer(symbol,tragetAccount, str(amountAfterFee) , EXCHANGE_NAME , addressTag, exchange)
            return True
    
        return False
    except Exception as ex:
        print(str(ex)+ 'at transferOrder('+symbol+','+tragetAccount+',',addressTag,')')
        return False
                



def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')



def getCoinInfo(sourceSymbol , tragetSymbol = ""):
   try:
     sourceSymbol = sourceSymbol.upper()
     tragetSymbol = tragetSymbol.upper()
     r = req.get('https://api.hitbtc.com/api/2/public/currency')
     if sourceSymbol == "USDT":
         sourceSymbol = "USDT20"
    
     if tragetSymbol == "USDT":
         tragetSymbol = "USDT20"

     checkCount = 0
     numCheck = 2
     if tragetSymbol == "":
            numCheck = 1 

     jsonReq = json.loads(r.text)
     for item in jsonReq:
        if item['id'] == sourceSymbol or item['id'] == tragetSymbol:
                checkCount = checkCount + 1
                if not item['crypto'] or not item['payoutEnabled'] or not item['payinEnabled'] or item['delisted']   :
                        return False
    

     if checkCount < numCheck:
        return False
     
     if tragetSymbol != "":
        symbol = tragetSymbol+sourceSymbol
        r1 = req.get('https://api.hitbtc.com/api/2/public/orderbook/' + symbol)
        jsonReq1 = json.loads(r1.text)

        if 'ask' in jsonReq1 and 'bid' in jsonReq1:
            if len(jsonReq1['ask']) == 0 and len(jsonReq1['bid']) == 0 :
                return False  
        else:
            return False 



     return True
   except Exception as ex:
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+', '+tragetSymbol+')')
    return False 