import requests as req
import json
import hmac
import hashlib
import datetime
import os
import urllib
import time
import ApiKeyExchange
import LineNoty
import math
import Kraken


API_KEY = ApiKeyExchange.Binance_KEY
API_SECRET = ApiKeyExchange.Binance_SECRET
EXCHANGE_NAME = "Binance"
floatCoin = {}

def SellAllBinace(): #text XRP
  try:   
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)
    

    symbol = 'XRPUSDT'
    side = 'SELL'
    typeBinance = 'MARKET'
    quantity = int(GetBalance('XRP'))
    timestamp = currentTimeBuy['serverTime']

    bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)

    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    print(h)
    
    parameter = {
        'symbol' :        symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : quantity,
        'timestamp' :  timestamp,
        'signature' : h 
    }

    header = { 'X-MBX-APIKEY': API_KEY}
    print(chr(27) + "[2J")
    print('Balance : '+ str(GetBalance('XRP')))
    if GetBalance('XRP') > 10:
      r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
      jsonConvert = json.loads(r.text)
      if jsonConvert['isWorking'] :
        return True
      else :
        return False
  except Exception as ex : 
     print(str(ex) + ' at SellAllBinace()')




def SellAllCryptoBinace(symbolEx):
  try:
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)
    

    
    

    symbol = symbolEx+'USDT'
    side = 'SELL'
    typeBinance = 'MARKET'
    quantity = int(GetBalance(symbolEx))
    timestamp = currentTimeBuy['serverTime']

    bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)

    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    print(h)

    parameter = {
        'symbol' :        symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : quantity,
        'timestamp' :  timestamp,
        'signature' : h 
    }

    header = { 'X-MBX-APIKEY': API_KEY}
    print(chr(27) + "[2J")
    print('Balance : '+ str(GetBalance(symbolEx)))
    if GetBalance(symbolEx) > 0.114757:
      r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
      jsonConvert = json.loads(r.text)
      if jsonConvert['isWorking'] :
        return True
      else :
        return False
  except Exception as ex : 
    print(str(ex) + ' at SellAllCryptoBinace('+str(symbolEx)+')')  



   
  
def getMiniQty():
  try:
    getText = req.get('https://www.binance.com/api/v1/depth?symbol=XRPUSDT')
    jsonConvertedRate = json.loads(getText.text)
    return float(jsonConvertedRate['bids'][0][0])*GetBalance('XRP')
  except Exception as ex : 
    print(str(ex) + ' at getMiniQty()')

def getRateAsk(symbol):
  try: 
   
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return float(jsonConvertedRate['asks'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getRate('+symbol+')')

def getRateBid(symbol):
  try: 
   
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return float(jsonConvertedRate['bids'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getRate('+symbol+')')


def getPriceBidBx(parId):
  try:
    # r = req.get('https://bx.in.th/api/orderbook/?pairing='+str(parId))
    # orderJson = json.loads(r.text)
    return float(250000.00 )
  except Exception as ex : 
    print(str(ex) + ' at getPriceBid()')


def getQuantityCanBuySP(symbol,volumn):
  try:
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    fee = 1
    
    totleRes = 0
    for ask in jsonConvertedRate['asks']:
      tragetRate = float(ask[0])
      tragetVolumn = float(ask[1])

      tragetVolumnTotle = tragetVolumn*tragetRate
      # tragetVolumnAndFee = (float(tragetVolumnTotle * (1+(fee/100))))
      if(volumn > tragetVolumnTotle):
        # volumn = volumn - tragetVolumnAndFee

        totleResV = tragetVolumn
        totleRes += totleResV

        # print('Order: Rate=' + str(tragetRate) + ' Volumn=' + str(tragetVolumn) + ' USDT=' + str(totleResV))
      else:
        # volumn = (float(volumn * (1-(fee/100))))
        totleResV = volumn / tragetRate
        totleRes += totleResV

        # print('Order: Rate=' + str(tragetRate) + ' Volumn=' + str(tragetVolumn) + ' USDT=' + str(totleResV))
        break
    totleRes = totleRes - ((totleRes*fee)/100)
    return totleRes
  except Exception as ex : 
    print(str(ex) + ' at getSwapAmountCanSell('+symbol+')')  
def getQuantityCanSellSP(symbol, volumn):
    
  try:

    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    

    fee = 0.1
    totleRes = 0
    for bid in jsonConvertedRate['bids']:
      tragetRate = float(bid[0])
      tragetVolumn = float(bid[1])
      tragetVolumnAndFee = (float(tragetVolumn * (1+(fee/100))))
      if(volumn > tragetVolumnAndFee):
        volumn = volumn - tragetVolumnAndFee

        totleResV = tragetRate * tragetVolumn
        totleRes += totleResV

        # print('Order: Rate=' + str(tragetRate) + ' Volumn=' + str(tragetVolumn) + ' USDT=' + str(totleResV))
      else:
        volumn = (float(volumn * (1-(fee/100))))
        totleResV = tragetRate * volumn
        totleRes += totleResV

        # print('Order: Rate=' + str(tragetRate) + ' Volumn=' + str(tragetVolumn) + ' USDT=' + str(totleResV))
        break

    return totleRes

    # return volumn*float(jsonConvertedRate['bids'][0][0])
  except Exception as ex : 
    print(str(ex) + ' at getSwapAmountCanSell('+symbol+')')

def getQuantityCanBuy(symbol,sourceSymbol):
  try: 
   
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return round(GetBalance(sourceSymbol)-(1/1000000),6)/float(jsonConvertedRate['asks'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+symbol+')')



def getQuantityCanSale(symbol,trgetSymbol):
  try: 
    if trgetSymbol != 'USDT':
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+trgetSymbol+'USDT')
    else:
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return round(GetBalance(trgetSymbol)-(1/1000000),6)*float(jsonConvertedRate['bids'][0][0])
    
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+symbol+')')


def getQuantityCanBuyByVolumn(symbol, volumn):
  try: 
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+symbol)
    jsonConvertedRate = json.loads(getText.text)
    return volumn/float(jsonConvertedRate['asks'][0][0])
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+symbol+')')

 

################################################################################################################################################################################################################################



def SellOrder(sourceSymbol, tragetSymbol):
  try:
    
    side = 'SELL'
    typeBinance = 'MARKET'
    if tragetSymbol == 'POW':
      tragetSymbol = 'POWR'

    if tragetSymbol == 'BCH':
      tragetSymbol = 'BCHABC'

    symbol = tragetSymbol + sourceSymbol
    jsonFloat = getFloatCoin()
    balanceTraget = GetBalance(tragetSymbol)
    balanceSource = GetBalance(sourceSymbol)

    quantity = getQtyWithFloatCoin(tragetSymbol , balanceTraget)
 
    if getCostForSale(sourceSymbol, tragetSymbol ,quantity )  > getMinimumSellSource(sourceSymbol,tragetSymbol):
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']

      bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)

      h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()

      parameter = {
          'symbol' : symbol,
          'side' : side,
          'type': typeBinance,
          'quantity' : quantity,
          'timestamp' :  timestamp,
          'signature' : h 
      }

      header = { 'X-MBX-APIKEY': API_KEY}
      r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
      jsonConvert = json.loads(r.text)
      print(r.text)

      balanceTragetEndProcess = GetBalance(tragetSymbol)
      balanceSourceEndProcess = GetBalance(sourceSymbol)
      Source = balanceSourceEndProcess - balanceSource
      Target = balanceTraget - balanceTragetEndProcess
      Rate = 0.0
      if Target > 0:
        Rate = Source / Target

      LineNoty.notiBuyOrSell('Sell' , 'Binance' ,  Target , tragetSymbol , Source , sourceSymbol  , balanceTragetEndProcess ,balanceSourceEndProcess, Rate , getPriceBidBx(1) , getRateBid("BTCUSDT"))
      LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(quantity))
      
      if 'orderId' in jsonConvert:
        if jsonConvert['orderId'] > 0:
          return True
      else :
        return False
        
  except Exception as ex : 
    print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')



def getCostForSale(sourceSymbol, tragetSymbol , qty):
  try: 
    if sourceSymbol != 'USDT':
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+sourceSymbol+'USDT')
    else:
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    
    jsonConvertedRate = json.loads(getText.text)
    if sourceSymbol != 'USDT':
      return qty
    else:
      return float(jsonConvertedRate['bids'][0][0])*qty
  except Exception as ex : 
    print(str(ex) + ' at getCostForSale('+sourceSymbol+','+tragetSymbol+','+str(qty)+')')

def getMinimumSellSource(sourceSymbol,tragetSymbol):
  try:
    if sourceSymbol == "USDT":
      return 10
    if sourceSymbol =='POWR':
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+sourceSymbol+'USDT')
    else:
      getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    jsonConvertedRate = json.loads(getText.text)
    return float(jsonConvertedRate['bids'][0][0])/10
  except Exception as ex : 
    print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')

def getMinimumBuySource(sourceSymbol,tragetSymbol):
  try:
    if tragetSymbol == "USDT":
      return 10
    
    if sourceSymbol == "USDT":
      return 10

    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    jsonConvertedRate = json.loads(getText.text)
    return float(jsonConvertedRate['asks'][0][0])/10
  except Exception as ex : 
    sourceSymbol = 'BTC'
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    jsonConvertedRate = json.loads(getText.text)
    return float(jsonConvertedRate['asks'][0][0])/10
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+sourceSymbol+')')


def getMinimumBuyTraget(sourceSymbol,tragetSymbol):
  try:
    if tragetSymbol == "USDT":
      return 10

    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    jsonConvertedRate = json.loads(getText.text)
    return 10/float(jsonConvertedRate['asks'][0][0])
  except Exception as ex : 
    sourceSymbol = 'BTC'
    getText = req.get('https://www.binance.com/api/v1/depth?symbol='+tragetSymbol+sourceSymbol)
    jsonConvertedRate = json.loads(getText.text)
    return 10/float(jsonConvertedRate['asks'][0][0])
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+sourceSymbol+')')

def transferOrder(symbol,tragetAccount, addressTag="" , exchange = ""): # in BX like this transferOrder(symbol,tragetAccount, addressTag=""):
    try:
      symbol = symbol.upper()
      if symbol == 'POW':
        symbol = 'POWR'

      if symbol == 'BCH':
        symbol = 'BCHABC'
      
      asset = symbol
      amount = GetBalance(symbol)
      amount = getQtyWithFloatCoin(symbol ,amount)
      
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']
      if addressTag != "":
        addressTag = '&addressTag='+str(addressTag)

      method = ""
      if exchange.upper() == 'KRAKEN' and symbol == 'USDT':
        method = "&network=OMNI"

      bodyReq = 'name='+str(asset)+'&timestamp='+str(timestamp)+addressTag+'&amount='+str(amount)+'&asset='+str(asset)+'&address='+tragetAccount + method
      
      h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
     
      bodyReq = bodyReq + '&signature='+h

      header = { 'Accept': 'application/json',
                'User-Agent': 'binance/python',
                'X-MBX-APIKEY': API_KEY}

      r = req.post('https://api.binance.com/wapi/v3/withdraw.html', bodyReq , headers =  header)
      jsonConvert = json.loads(r.text)
      print(r.text)
      time.sleep(3)
      endingAmount = GetBalance(symbol)
      if jsonConvert['success']:
        LineNoty.notiForTransfer(symbol,tragetAccount, amount , EXCHANGE_NAME , addressTag, exchange)
        LineNoty.notiTranfer("Withdraw","Binance","XXXXXX" , amount ,endingAmount,asset )

      return jsonConvert['success']
    except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')




def GetBalance(symbol):
  try:
    time.sleep(3)
    if symbol == 'POW':
      symbol = 'POWR'

    if symbol == 'BCH':
      symbol = 'BCHABC'
    
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)

    timestamp = currentTimeBuy['serverTime']
  

    bodyReq = 'timestamp='+str(timestamp)


    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    

    parameter = {
        'timestamp' :        timestamp,
        'signature' : h
    }

    header = { 'X-MBX-APIKEY': API_KEY}

    r = req.get('https://api.binance.com/api/v3/account', parameter , headers =  header)
    jsonConvert = json.loads(r.text) 
    #print(r.text) 
    for balan in jsonConvert['balances']:
      if balan['asset']== symbol :
        return float(balan['free'])
    
    return 0
  except Exception as ex : 
    print(str(ex) + ' at GetBalance('+symbol+')')



def BuyOrder(sourceSymbol, tragetSymbol,volumn,isRebalance): #<--- แก้หัวข้อตามนี้
  try:

    if tragetSymbol == 'POW':
      tragetSymbol = 'POWR'

    if tragetSymbol == 'BCH':
      tragetSymbol = 'BCHABC'

    symbol = tragetSymbol+sourceSymbol
    side = 'BUY'
    typeBinance = 'MARKET'
    jsonFloat = getFloatCoin()
    quantity = getQtyWithFloatCoin(tragetSymbol , getQuantityCanBuySP(symbol,sourceSymbol))

    balanceTraget = GetBalance(tragetSymbol)
    balanceSource = GetBalance(sourceSymbol)

   

    if isRebalance:
      quantity = getQtyWithFloatCoin(tragetSymbol , float(getQuantityCanBuyByVolumn(symbol,volumn)) )
      if quantity < float(getMinimumBuySource(sourceSymbol,tragetSymbol)):
        return False #จำนวนซื้อน้อยไป

  
    
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)
    timestamp = currentTimeBuy['serverTime']
    buySuccess = False

    bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)
    # bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&timestamp='+str(timestamp)

    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    

    parameter = {
        'symbol' : symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : quantity,
        'timestamp' :  timestamp,
        'signature' : h 
    }

    

    header = { 'X-MBX-APIKEY': API_KEY}
    # print(chr(27) + "[2J")
    # print('Balance : '+ str(GetBalance(tragetSymbol)))

    r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
    jsonConvert = json.loads(r.text)
    print(r.text)

    LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(quantity))
    balanceTragetEndProcess = GetBalance(tragetSymbol)
    balanceSourceEndProcess = GetBalance(sourceSymbol)
    Source = balanceSource - balanceSourceEndProcess
    Target = balanceTragetEndProcess - balanceTraget
    Rate = 0.0
    if Target > 0:
      Rate = Source / Target
    

    LineNoty.notiBuyOrSell('Buy' , 'Binance' , Source , sourceSymbol , Target , tragetSymbol , balanceSourceEndProcess ,balanceTragetEndProcess, Rate , getPriceBidBx(1) , getRateBid("BTCUSDT"))
    
    
    if 'msg' not in jsonConvert:
      buySuccess =  True
    else:
      print(jsonConvert['msg'])
      buySuccess = False
      return buySuccess

    return True
  except Exception as ex : 
    print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')

def SPOrder(sourceSymbol,volumn,BUYORSELL,tragetSymbol): #<--- เอาไวสำหรับ 4
  try:

    symbol = sourceSymbol
    side = BUYORSELL
    typeBinance = 'MARKET'
    quantity = 0
    
     
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)
    timestamp = currentTimeBuy['serverTime']
    buySuccess = False
     

    bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)

    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    

    parameter = {
        'symbol' : symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : quantity,
        'timestamp' :  timestamp,
        'signature' : h 
    }

    if BUYORSELL == "SELL":
      bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(quantity)+'&timestamp='+str(timestamp)
      parameter = {
        'symbol' : symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : quantity,
        'timestamp' :  timestamp,
        'signature' : h 
    }


    

    header = { 'X-MBX-APIKEY': API_KEY}
  
    r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
    jsonConvert = json.loads(r.text)
    print(r.text)

    if 'msg' not in jsonConvert:
      buySuccess =  True
    else:
      print(jsonConvert['msg'])
      buySuccess = False
      return buySuccess,quantity

    return buySuccess,jsonConvert['executedQty']
  except Exception as ex : 
    print(str(ex) + ' at SPOrder('+sourceSymbol+','+tragetSymbol+')')

def SPOrder2(sourceSymbol,volumn,BUYORSELL,tragetSymbol): #<--- เอาไวสำหรับ 4
  try:

    symbol = sourceSymbol
    side = BUYORSELL
    typeBinance = 'MARKET'     
    # timeBinance = req.get('https://api.binance.com/api/v1/time')
    # currentTimeBuy =  json.loads(timeBinance.text)
    # timestamp = currentTimeBuy['serverTime']
    buySuccess = False
    bodyReq=""
    parameter={}
    

    if BUYORSELL == "SELL":
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']
      volumn = getQtyWithFloatCoin(tragetSymbol , volumn)
      bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quantity='+str(volumn)+'&timestamp='+str(timestamp)
      h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
      parameter = {
        'symbol' : symbol,
        'side' : side,
        'type': typeBinance,
        'quantity' : volumn,
        'timestamp' :  timestamp,
        'signature' : h 
    }
    elif BUYORSELL == "BUY":
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']
      volumn = getQtyWithFloatCoin(tragetSymbol , volumn)
      bodyReq = 'symbol='+symbol+'&side='+side+'&type='+typeBinance+'&quoteOrderQty='+str(volumn)+'&timestamp='+str(timestamp)
      h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest() 

      parameter = {
          'symbol' : symbol,
          'side' : side,
          'type': typeBinance,
          'quoteOrderQty' : volumn,
          'timestamp' :  timestamp,
          'signature' : h 
      }

    

    header = { 'X-MBX-APIKEY': API_KEY}
  
    r = req.post('https://api.binance.com/api/v3/order', parameter , headers =  header)
    jsonConvert = json.loads(r.text)
    print(r.text)
    cummulativeQuoteQty = jsonConvert["cummulativeQuoteQty"]
    bala = 0
    if BUYORSELL == "BUY":
      bala = float(jsonConvert['executedQty']) -  sum(float(item['commission']) for item in jsonConvert['fills'])
    elif BUYORSELL =="SELL":
      bala = float(jsonConvert['cummulativeQuoteQty']) -  sum(float(item['commission']) for item in jsonConvert['fills'])
    #for sell cummulativeQuoteQty param diff with commission
    

    if 'msg' not in jsonConvert:
      buySuccess =  True
    else:
      print(jsonConvert['msg'])
      LineNoty.Noti(jsonConvert['msg'])
      buySuccess = False
      return buySuccess,0

    return buySuccess,bala,cummulativeQuoteQty
  except Exception as ex : 
    print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')


def getRateForBuy(_symbol):
    try:
        symbol = _symbol+'USDT'
        r = req.get('https://api.binance.com/api/v3/ticker/price?symbol='+symbol)
        jsonConvert = json.loads(r.text)
        rateBuy = float(jsonConvert['price']) * (110/100)
        return rateBuy
    except Exception as ex : 
        print(str(ex) + ' at getRateForBuy('+_symbol+')')


def GetFeePrice():
  try:
    fee = {'tradeFee':0.1,
    'transfer': {'CTR': 35.0, 'MATIC': 47.0, 'IOTX': 241.0, 'CDT': 105.0, 'DOCK': 102.0, 'STX': 10.0, 'DENT': 4958.0, 'AION': 0.1, 'NPXS': 6000.0, 'BCPT': 0.63, 'VIBE': 64.0, 'DGD': 0.045, 'ZRX': 3.87, 'SUB': 40.0, 'BCD': 0.01, 'POA': 0.01, 'AE': 0.1, 'IOST': 1.0, 'BCH': 0.001, 'POE': 447.0, 'OMG': 1.19, 'BAND': 3.11, 'HOT': 1207.0, 'BTC': 0.0005, 'NKN': 43.0, 'IOTA': 0.5, 'CVC': 36.0, 'BTG': 0.001, 'BCX': 0.5, 'BTM': 5.0, 'ARK': 0.2, 'TRIG': 50.0, 'RCN': 19.0, 'ARN': 0.11, 'KEY': 447.0, 'BTS': 1.0, 'TLDBTC': 1.0, 'BTT': 48.0, 'ONE': 2.78, 'ONG': 5.8, 'ANKR': 8.56, 'GNT': 25.0, 'ALGO': 0.01, 'SC': 0.1, 'ONT': 1.0, 'PPT': 1.82, 'RDN': 7.08, 'PIVX': 0.2, 'RUB': 1.0, 'ARDR': 2.0, 'AST': 42.0, 'CLOAK': 0.02, 'MANA': 32.0, 'NEBL': 0.01, 'VTHO': 21.0, 'MEETONE': 300.0, 'QSP': 91.0, 'SALT': 5.2, 'STORM': 671.0, 'ATD': 100.0, 'ICN': 3.4, 'ZEC': 0.005, 'REN': 25.0, 'REP': 0.085, 'APPC': 32.0, 'JEX': 50.0, 'ADA': 1.0, 'ELF': 15.0, 'REQ': 74.0, 'STORJ': 7.84, 'ICX': 0.02, 'ADD': 100.0, 'LOOM': 49.0, 'ZEN': 0.002, 'YOYO': 1.0, 'PAX': 0.87, 'DOGE': 50.0, 'DUSK': 0.33, 'HBAR': 1.0, 'RVN': 1.0, 'NANO': 0.01, 'WAVES': 0.002, 'CHZ': 1.77, 'ADX': 11.0, 'XRP': 0.25, 'WPR': 129.0, 'KAVA': 0.018, 'HCC': 0.0005, 'SYS': 1.0, 'COCOS': 1207.0, 'TUSD': 0.87, 'GAS': 0.0, 'WABI': 5.99, 'STRAT': 0.1, 'ENG': 1.92, 'THETA': 0.1, 'ENJ': 11.0, 'KZT': 1.0, 'WAN': 0.1, 'OAX': 16.0, 'GRS': 0.2, 'TFUEL': 3.06, 'PERL': 38.0, 'LEND': 64.0, 'DLT': 23.0, 'TROY': 2.08, 'LLT': 100.0, 'SBTC': 0.0005, 'XTZ': 0.5, 'MOD': 5.0, 'AGI': 47.0, 'EON': 10.0, 'EOP': 5.0, 'EOS': 0.1, 'GO': 0.01, 'NCASH': 1006.0, 'OST': 90.0, 'HC': 0.005, 'ZIL': 0.01, 'SKY': 0.02, 'XEM': 4.0, 'NAS': 0.1, 'NAV': 0.2, 'GTO': 1.52, 'CTXC': 0.1, 'WTC': 1.88, 'XVG': 0.1, 'TNB': 431.0, 'DNT': 154.0, 'BCHSV': 0.0001, 'STEEM': 0.01, 'TNT': 17.0, 'KMD': 0.002, 'IQ': 50.0, 'CMT': 1.0, 'MITH': 1.61, 'ERD': 7.09, 'CND': 88.0, 'UAH': 1.0, 'FTM': 1.36, 'POWR': 22.0, 'KNC': 4.37, 'GVT': 0.82, 'WINGS': 20.0, 'CHAT': 100.0, 'RLC': 1.62, 'PHB': 3.37, 'BGBP': 0.67, 'ATOM': 0.005, 'BLZ': 44.0, 'SNM': 69.0, 'SNT': 85.0, 'FUN': 236.0, 'SNGLS': 109.0, 'COS': 1.29, 'QKC': 241.0, 'FET': 17.0, 'ETC': 0.01, 'ETF': 1.0, 'BNB': 0.001, 'CELR': 198.0, 'ETH': 0.01, 'MCO': 0.21, 'NEO': 0.0, 'TOMO': 0.01, 'LRC': 37.0, 'MTH': 81.0, 'XZC': 0.02, 'GXS': 0.3, 'MTL': 3.16, 'VET': 100.0, 'BNT': 3.45, 'USDT': 0.87, 'QLC': 1.0, 'USDS': 0.88, 'MDA': 1.72, 'DASH': 0.002, 'EDO': 3.8, 'AMB': 50.0, 'FUEL': 262.0, 'TRY': 1.0, 'TRX': 1.0, 'EUR': 1.0, 'LSK': 0.1, 'NULS': 0.01, 'BEAM': 0.1, 'DCR': 0.01, 'NGN': 1.0, 'DATA': 60.0, 'LTC': 0.001, 'USDC': 0.87, 'WIN': 148.0, 'EVX': 3.31, 'NXS': 0.02, 'INS': 5.09, 'CBM': 200.0, 'XLM': 0.01, 'LINK': 0.39, 'MFT': 862.0, 'QTUM': 0.01, 'LUN':
1.01, 'BQX': 37.0, 'POLY': 33.0, 'VIB': 49.0, 'VIA': 0.01, 'BAT': 4.97, 'BRD': 3.58, 'BUSD': 0.87, 'XMR': 0.0001, 'ARPA': 91.0}
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')

def getFeeBinance():
   
  timeBinance = req.get('https://api.binance.com/api/v1/time')
  currentTimeBuy =  json.loads(timeBinance.text)
  timestamp = currentTimeBuy['serverTime']
  

  bodyReq = 'timestamp='+str(timestamp)
  h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
  bodyReq = bodyReq + '&signature='+h

  header = { 'Accept': 'application/json',
                'User-Agent': 'binance/python',
                'X-MBX-APIKEY': API_KEY}
  r = req.get('https://api.binance.com/wapi/v3/assetDetail.html', bodyReq , headers =  header)
  jsonConvert = json.loads(r.text)
  listcoinTranFee = {}
  for coin in jsonConvert['assetDetail']:
      value  = {coin : jsonConvert['assetDetail'][coin]['withdrawFee']}
      listcoinTranFee.update(value)

  return listcoinTranFee

def getFloatCoin():
  try:
    global floatCoin
   
    return floatCoin
  except Exception as ex : 
    print(str(ex) + ' at getFloatCoin()')

def CheckPendingOrder(symbol):#CheckPendingOrder()
  try:
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)
    asset = symbol
    timestamp = currentTimeBuy['serverTime']

    bodyReq = 'asset='+str(asset)+'&timestamp='+str(timestamp)
    
    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()

  
    bodyReq = bodyReq + '&signature='+h

    header = { 'Accept': 'application/json',
                'User-Agent': 'binance/python',
                'X-MBX-APIKEY': API_KEY}

    r = req.get('https://api.binance.com/wapi/v3/withdrawHistory.html', bodyReq , headers =  header)
    jsonConvert = json.loads(r.text)
    numberStatus = int(jsonConvert['withdrawList'][0]['status'])
    status = 'Processing '
    if(numberStatus == 3):
      status = 'Rejected'
    elif(numberStatus == 6):
      status = 'Completed'
    else:
      status = 'Processing '

    return status
  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')

def GetTradeStatus(sourceSymbol, tragetSymbol):
  try:
    r = req.get('https://www.binance.com/api/v1/exchangeInfo')
    jsonload = json.loads(r.text)
    for pair in jsonload['symbols']:
      if pair['symbol'] == tragetSymbol+sourceSymbol:
        return pair
        
    return False
  except Exception as ex : 
    print(str(ex) + ' at GetTradeStatus('+sourceSymbol+','+tragetSymbol+')')

def getQtyWithFloatCoin(coin , qty):
  try:
    jsonFloat = getFloatCoin()
    unitCoin = jsonFloat[coin]
    funcT = 0
    if unitCoin > 0: 
      funcT =  1 / pow(10 , unitCoin)

    
    testVal = round_down(qty,unitCoin)
    if unitCoin == 0:
      testVal = int(testVal)
    
    return testVal
  except Exception as ex : 
    print(str(ex) + ' at getQtyWithFloatCoin('+coin+','+str(qty)+')')

def round_down(n, decimals=0):
  try:
    multiplier = 10 ** decimals
    return math.floor(n * multiplier) / multiplier
  except Exception as ex : 
    print(str(ex) + ' at GetTradeStatus('+str(n)+','+str(decimals)+')')



def getExchangeInfo(): #<--- แก้หัวข้อตามนี้
  try:

    # print(chr(27) + "[2J")
    # print('Balance : '+ str(GetBalance(tragetSymbol)))
    global floatCoin

    r = req.get('https://api.binance.com/api/v1/exchangeInfo')
    jsonConvert = json.loads(r.text)
    
    for symbol in jsonConvert['symbols']:
      result = symbol['filters'][2]['minQty'].index('1')-1
      if result < 0 : result = 0
      
      if symbol["baseAsset"] in floatCoin:
        if floatCoin[symbol["baseAsset"]] > result:
            floatCoin[symbol["baseAsset"]] = result
      else:
        floatCoin[symbol["baseAsset"]] = result
    
    floatCoin['USDT'] = 0
    floatCoin['NEO'] = 0

    return True
  except Exception as ex : 
    print(str(ex) + ' at BuyOrder()')

def GetAccount( symbol , exchange="" ): # in BX like this transferOrder(symbol,tragetAccount, addressTag=""):
    try:

      symbol = symbol.upper()
      if symbol == 'POW':
        symbol = 'POWR'

      if symbol == 'BCH':
        symbol = 'BCHABC'

      
      exchange = exchange.upper()
      if exchange == "KRAKEN" and symbol == "USDT":
        return "18PCgM2bDFgmKZNQJSPkPZFoYLkpTmgPsc" , ""

      asset = symbol
      #amount = GetBalance('EOS')
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']
      coinNetwork = 'coin='+str(asset)
      if exchange == "KRAKEN" and symbol == "USDT":
        coinNetwork = coinNetwork + '&network=OMNI'

      bodyReq = coinNetwork+'&timestamp='+str(timestamp)
      
      h = hmac.new(bytes(ApiKeyExchange.Binance_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()

     
      bodyReq = bodyReq + '&signature='+h

      header = { 'Accept': 'application/json',
                'User-Agent': 'binance/python',
                'X-MBX-APIKEY': ApiKeyExchange.Binance_KEY}

      r = req.get('https://api.binance.com/sapi/v1/capital/deposit/address', bodyReq , headers =  header)
      jsonConvert = json.loads(r.text)
      address = ""
      tag = ""

      if 'address' in jsonConvert:
        address = jsonConvert['address']
        if 'tag' in jsonConvert:
          tag = jsonConvert['tag']


     

      return address , tag
    except Exception as ex : 
      print(str(ex) + ' at GetBinaceAccount('+symbol+')')        


def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')

def getCoinInfo(sourceSymbol , tragetSymbol = ""):
  try:
    sourceSymbol = sourceSymbol.upper()
    tragetSymbol = tragetSymbol.upper()
    currencyInfo = GetCurrencyInfo()

    if tragetSymbol != "":
      tradeStatus = GetTradeStatus(sourceSymbol , tragetSymbol)

      if 'isSpotTradingAllowed' in tradeStatus:
        if not tradeStatus['isSpotTradingAllowed']:
          return False
      else:
        return False
      
      if sourceSymbol in currencyInfo and tragetSymbol in currencyInfo:
        if not currencyInfo[sourceSymbol]['withdraw'] or not currencyInfo[sourceSymbol]['deposit']  or not currencyInfo[tragetSymbol]['withdraw'] or not currencyInfo[tragetSymbol]['deposit']:
          return False
      else:
        return True
    else:
      if sourceSymbol in currencyInfo:
        if not currencyInfo[sourceSymbol]['withdraw'] or not currencyInfo[sourceSymbol]['deposit'] :
            return False
      else:
        return True

    return True
  except Exception as ex :
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+' , '+tragetSymbol+')')
    return False 

def GetCurrencyInfo():
  try:
    timeBinance = req.get('https://api.binance.com/api/v1/time')
    currentTimeBuy =  json.loads(timeBinance.text)

    timestamp = currentTimeBuy['serverTime']
  

    bodyReq = 'timestamp='+str(timestamp)


    h = hmac.new(bytes(API_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()
    

    parameter = {
        'timestamp' :        timestamp,
        'signature' : h
    }

    header = { 'X-MBX-APIKEY': API_KEY}

    r = req.get('https://api.binance.com/sapi/v1/capital/config/getall', parameter , headers =  header)
    jsonConvert = json.loads(r.text) 
    #print(r.text)
    currencyInfo = {}

    for item in jsonConvert:
      coinName = item['coin']
      canWithdraw =  item['withdrawAllEnable']
      canDeposit = item['depositAllEnable']
      withdrawing = item['withdrawing']
      for subItem in item['networkList']:
        if not subItem['depositEnable']:
          canDeposit = False
        if not subItem['withdrawEnable']:
          canWithdraw = False

      if coinName == 'POW':
        coinName = 'POWR'

      if coinName == 'BCH':
        coinName = 'BCHABC'

      currencyInfo[coinName] = {
          "withdraw" : canWithdraw,
          "deposit" : canDeposit,
          "withdrawing" : withdrawing
      }
   
    
    return currencyInfo
  except Exception as ex : 
    print(str(ex) + ' at GetBalance()')



def getAllNetworkList( symbol , exchange="" ): # in BX like this transferOrder(symbol,tragetAccount, addressTag=""):
    try:

      symbol = symbol.upper()
      if symbol == 'POW':
        symbol = 'POWR'

      if symbol == 'BCH':
        symbol = 'BCHABC'

      
      exchange = exchange.upper()
      if exchange == "KRAKEN" and symbol == "USDT":
        return "18PCgM2bDFgmKZNQJSPkPZFoYLkpTmgPsc" , ""

      asset = symbol
      #amount = GetBalance('EOS')
      timeBinance = req.get('https://api.binance.com/api/v1/time')
      currentTimeBuy =  json.loads(timeBinance.text)
      timestamp = currentTimeBuy['serverTime']
      coinNetwork = 'coin='+str(asset)
      if exchange == "KRAKEN" and symbol == "USDT":
        coinNetwork = coinNetwork + '&network=OMNI'

      bodyReq = coinNetwork+'&timestamp='+str(timestamp)
      
      h = hmac.new(bytes(ApiKeyExchange.Binance_SECRET,"ascii"), msg=bytes(bodyReq,"ascii") ,digestmod = hashlib.sha256).hexdigest()

     
      bodyReq = bodyReq + '&signature='+h

      header = { 'Accept': 'application/json',
                'User-Agent': 'binance/python',
                'X-MBX-APIKEY': ApiKeyExchange.Binance_KEY}

      r = req.get('https://api.binance.com/sapi/v1/capital/deposit/address', bodyReq , headers =  header)
      jsonConvert = json.loads(r.text)
      address = ""
      tag = ""

      if 'address' in jsonConvert:
        address = jsonConvert['address']
        if 'tag' in jsonConvert:
          tag = jsonConvert['tag']


     

      return address , tag
    except Exception as ex : 
      print(str(ex) + ' at GetBinaceAccount('+symbol+')')        

getExchangeInfo() # Don't remove it !!!