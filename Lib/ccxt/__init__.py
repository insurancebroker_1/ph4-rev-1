# -*- coding: utf-8 -*-

"""CCXT: CryptoCurrency eXchange Trading Library"""

# MIT License
# Copyright (c) 2017 Igor Kroitor
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# ----------------------------------------------------------------------------

__version__ = '1.18.594'

# ----------------------------------------------------------------------------

from Lib.ccxt.base.exchange import Exchange                     # noqa: F401

from Lib.ccxt.base.decimal_to_precision import decimal_to_precision  # noqa: F401
from Lib.ccxt.base.decimal_to_precision import TRUNCATE              # noqa: F401
from Lib.ccxt.base.decimal_to_precision import ROUND                 # noqa: F401
from Lib.ccxt.base.decimal_to_precision import DECIMAL_PLACES        # noqa: F401
from Lib.ccxt.base.decimal_to_precision import SIGNIFICANT_DIGITS    # noqa: F401
from Lib.ccxt.base.decimal_to_precision import NO_PADDING            # noqa: F401
from Lib.ccxt.base.decimal_to_precision import PAD_WITH_ZERO         # noqa: F401

from Lib.ccxt.base import errors                                # noqa: F401
from Lib.ccxt.base.errors import BaseError                      # noqa: F401
from Lib.ccxt.base.errors import ExchangeError                  # noqa: F401
from Lib.ccxt.base.errors import NotSupported                   # noqa: F401
from Lib.ccxt.base.errors import AuthenticationError            # noqa: F401
from Lib.ccxt.base.errors import PermissionDenied               # noqa: F401
from Lib.ccxt.base.errors import AccountSuspended               # noqa: F401
from Lib.ccxt.base.errors import InvalidNonce                   # noqa: F401
from Lib.ccxt.base.errors import InsufficientFunds              # noqa: F401
from Lib.ccxt.base.errors import InvalidOrder                   # noqa: F401
from Lib.ccxt.base.errors import OrderNotFound                  # noqa: F401
from Lib.ccxt.base.errors import OrderNotCached                 # noqa: F401
from Lib.ccxt.base.errors import DuplicateOrderId               # noqa: F401
from Lib.ccxt.base.errors import CancelPending                  # noqa: F401
from Lib.ccxt.base.errors import NetworkError                   # noqa: F401
from Lib.ccxt.base.errors import DDoSProtection                 # noqa: F401
from Lib.ccxt.base.errors import RequestTimeout                 # noqa: F401
from Lib.ccxt.base.errors import ExchangeNotAvailable           # noqa: F401
from Lib.ccxt.base.errors import InvalidAddress                 # noqa: F401
from Lib.ccxt.base.errors import AddressPending                 # noqa: F401
from Lib.ccxt.base.errors import ArgumentsRequired              # noqa: F401
from Lib.ccxt.base.errors import BadRequest                     # noqa: F401
from Lib.ccxt.base.errors import BadResponse                    # noqa: F401
from Lib.ccxt.base.errors import NullResponse                   # noqa: F401
from Lib.ccxt.base.errors import OrderImmediatelyFillable       # noqa: F401
from Lib.ccxt.base.errors import OrderNotFillable               # noqa: F401


# from Lib.ccxt._1btcxe import _1btcxe                            # noqa: F401
# from Lib.ccxt.acx import acx                                    # noqa: F401
# from Lib.ccxt.allcoin import allcoin                            # noqa: F401
# from Lib.ccxt.anxpro import anxpro                              # noqa: F401
# from Lib.ccxt.anybits import anybits                            # noqa: F401
# from Lib.ccxt.bcex import bcex                                  # noqa: F401
# from Lib.ccxt.bequant import bequant                            # noqa: F401
# from Lib.ccxt.bibox import bibox                                # noqa: F401
# from Lib.ccxt.bigone import bigone                              # noqa: F401
# from Lib.ccxt.binance import binance                            # noqa: F401
# from Lib.ccxt.binanceje import binanceje                        # noqa: F401
# from Lib.ccxt.bit2c import bit2c                                # noqa: F401
# from Lib.ccxt.bitbank import bitbank                            # noqa: F401
# from Lib.ccxt.bitbay import bitbay                              # noqa: F401
# from Lib.ccxt.bitfinex import bitfinex                          # noqa: F401
# from Lib.ccxt.bitfinex2 import bitfinex2                        # noqa: F401
# from Lib.ccxt.bitflyer import bitflyer                          # noqa: F401
# from Lib.ccxt.bitforex import bitforex                          # noqa: F401
# from Lib.ccxt.bithumb import bithumb                            # noqa: F401
# from Lib.ccxt.bitibu import bitibu                              # noqa: F401
# from Lib.ccxt.bitkk import bitkk                                # noqa: F401
# from Lib.ccxt.bitlish import bitlish                            # noqa: F401
# from Lib.ccxt.bitmarket import bitmarket                        # noqa: F401
# from Lib.ccxt.bitmex import bitmex                              # noqa: F401
# from Lib.ccxt.bitsane import bitsane                            # noqa: F401
# from Lib.ccxt.bitso import bitso                                # noqa: F401
# from Lib.ccxt.bitstamp import bitstamp                          # noqa: F401
# from Lib.ccxt.bitstamp1 import bitstamp1                        # noqa: F401
# from Lib.ccxt.bittrex import bittrex                            # noqa: F401
# from Lib.ccxt.bitz import bitz                                  # noqa: F401
# from Lib.ccxt.bl3p import bl3p                                  # noqa: F401
# from Lib.ccxt.bleutrade import bleutrade                        # noqa: F401
# from Lib.ccxt.braziliex import braziliex                        # noqa: F401
# from Lib.ccxt.btcalpha import btcalpha                          # noqa: F401
# from Lib.ccxt.btcbox import btcbox                              # noqa: F401
# from Lib.ccxt.btcchina import btcchina                          # noqa: F401
# from Lib.ccxt.btcexchange import btcexchange                    # noqa: F401
# from Lib.ccxt.btcmarkets import btcmarkets                      # noqa: F401
# from Lib.ccxt.btctradeim import btctradeim                      # noqa: F401
# from Lib.ccxt.btctradeua import btctradeua                      # noqa: F401
# from Lib.ccxt.btcturk import btcturk                            # noqa: F401
# from Lib.ccxt.buda import buda                                  # noqa: F401
# from Lib.ccxt.bxinth import bxinth                              # noqa: F401
# from Lib.ccxt.ccex import ccex                                  # noqa: F401
# from Lib.ccxt.cex import cex                                    # noqa: F401
# from Lib.ccxt.chbtc import chbtc                                # noqa: F401
# from Lib.ccxt.chilebit import chilebit                          # noqa: F401
# from Lib.ccxt.cobinhood import cobinhood                        # noqa: F401
# from Lib.ccxt.coinbase import coinbase                          # noqa: F401
# from Lib.ccxt.coinbaseprime import coinbaseprime                # noqa: F401
# from Lib.ccxt.coinbasepro import coinbasepro                    # noqa: F401
# from Lib.ccxt.coincheck import coincheck                        # noqa: F401
# from Lib.ccxt.coinegg import coinegg                            # noqa: F401
from Lib.ccxt.coinex import coinex                              # noqa: F401
# from Lib.ccxt.coinexchange import coinexchange                  # noqa: F401
# from Lib.ccxt.coinfalcon import coinfalcon                      # noqa: F401
# from Lib.ccxt.coinfloor import coinfloor                        # noqa: F401
# from Lib.ccxt.coingi import coingi                              # noqa: F401
# from Lib.ccxt.coinmarketcap import coinmarketcap                # noqa: F401
# from Lib.ccxt.coinmate import coinmate                          # noqa: F401
# from Lib.ccxt.coinnest import coinnest                          # noqa: F401
# from Lib.ccxt.coinone import coinone                            # noqa: F401
# from Lib.ccxt.coinspot import coinspot                          # noqa: F401
# from Lib.ccxt.cointiger import cointiger                        # noqa: F401
# from Lib.ccxt.coolcoin import coolcoin                          # noqa: F401
# from Lib.ccxt.coss import coss                                  # noqa: F401
# from Lib.ccxt.crex24 import crex24                              # noqa: F401
# from Lib.ccxt.crypton import crypton                            # noqa: F401
# from Lib.ccxt.deribit import deribit                            # noqa: F401
# from Lib.ccxt.dsx import dsx                                    # noqa: F401
# from Lib.ccxt.dx import dx                                      # noqa: F401
# from Lib.ccxt.ethfinex import ethfinex                          # noqa: F401
# from Lib.ccxt.exmo import exmo                                  # noqa: F401
# from Lib.ccxt.exx import exx                                    # noqa: F401
# from Lib.ccxt.fcoin import fcoin                                # noqa: F401
# from Lib.ccxt.fcoinjp import fcoinjp                            # noqa: F401
# from Lib.ccxt.flowbtc import flowbtc                            # noqa: F401
# from Lib.ccxt.foxbit import foxbit                              # noqa: F401
# from Lib.ccxt.fybse import fybse                                # noqa: F401
# from Lib.ccxt.fybsg import fybsg                                # noqa: F401
# from Lib.ccxt.gateio import gateio                              # noqa: F401
# from Lib.ccxt.gdax import gdax                                  # noqa: F401
# from Lib.ccxt.gemini import gemini                              # noqa: F401
# from Lib.ccxt.getbtc import getbtc                              # noqa: F401
# from Lib.ccxt.hadax import hadax                                # noqa: F401
# from Lib.ccxt.hitbtc import hitbtc                              # noqa: F401
# from Lib.ccxt.hitbtc2 import hitbtc2                            # noqa: F401
# from Lib.ccxt.huobipro import huobipro                          # noqa: F401
# from Lib.ccxt.huobiru import huobiru                            # noqa: F401
# from Lib.ccxt.ice3x import ice3x                                # noqa: F401
# from Lib.ccxt.independentreserve import independentreserve      # noqa: F401
# from Lib.ccxt.indodax import indodax                            # noqa: F401
# from Lib.ccxt.itbit import itbit                                # noqa: F401
# from Lib.ccxt.jubi import jubi                                  # noqa: F401
# from Lib.ccxt.kkex import kkex                                  # noqa: F401
# from Lib.ccxt.kraken import kraken                              # noqa: F401
# from Lib.ccxt.kucoin import kucoin                              # noqa: F401
# from Lib.ccxt.kucoin2 import kucoin2                            # noqa: F401
# from Lib.ccxt.kuna import kuna                                  # noqa: F401
# from Lib.ccxt.lakebtc import lakebtc                            # noqa: F401
# from Lib.ccxt.lbank import lbank                                # noqa: F401
# from Lib.ccxt.liqui import liqui                                # noqa: F401
# from Lib.ccxt.liquid import liquid                              # noqa: F401
# from Lib.ccxt.livecoin import livecoin                          # noqa: F401
# from Lib.ccxt.luno import luno                                  # noqa: F401
# from Lib.ccxt.lykke import lykke                                # noqa: F401
# from Lib.ccxt.mandala import mandala                            # noqa: F401
# from Lib.ccxt.mercado import mercado                            # noqa: F401
# from Lib.ccxt.mixcoins import mixcoins                          # noqa: F401
# from Lib.ccxt.negociecoins import negociecoins                  # noqa: F401
# from Lib.ccxt.nova import nova                                  # noqa: F401
# from Lib.ccxt.okcoincny import okcoincny                        # noqa: F401
# from Lib.ccxt.okcoinusd import okcoinusd                        # noqa: F401
# from Lib.ccxt.okex import okex                                  # noqa: F401
# from Lib.ccxt.okex3 import okex3                                # noqa: F401
# from Lib.ccxt.paymium import paymium                            # noqa: F401
# from Lib.ccxt.poloniex import poloniex                          # noqa: F401
# from Lib.ccxt.quadrigacx import quadrigacx                      # noqa: F401
# from Lib.ccxt.rightbtc import rightbtc                          # noqa: F401
# from Lib.ccxt.southxchange import southxchange                  # noqa: F401
# from Lib.ccxt.stronghold import stronghold                      # noqa: F401
# from Lib.ccxt.surbitcoin import surbitcoin                      # noqa: F401
# from Lib.ccxt.theocean import theocean                          # noqa: F401
# from Lib.ccxt.therock import therock                            # noqa: F401
# from Lib.ccxt.tidebit import tidebit                            # noqa: F401
# from Lib.ccxt.tidex import tidex                                # noqa: F401
# from Lib.ccxt.uex import uex                                    # noqa: F401
# from Lib.ccxt.upbit import upbit                                # noqa: F401
# from Lib.ccxt.urdubit import urdubit                            # noqa: F401
# from Lib.ccxt.vaultoro import vaultoro                          # noqa: F401
# from Lib.ccxt.vbtc import vbtc                                  # noqa: F401
# from Lib.ccxt.virwox import virwox                              # noqa: F401
# from Lib.ccxt.xbtce import xbtce                                # noqa: F401
# from Lib.ccxt.yobit import yobit                                # noqa: F401
# from Lib.ccxt.zaif import zaif                                  # noqa: F401
# from Lib.ccxt.zb import zb                                      # noqa: F401

exchanges = [
    '_1btcxe',
    'acx',
    'allcoin',
    'anxpro',
    'anybits',
    'bcex',
    'bequant',
    'bibox',
    'bigone',
    'binance',
    'binanceje',
    'bit2c',
    'bitbank',
    'bitbay',
    'bitfinex',
    'bitfinex2',
    'bitflyer',
    'bitforex',
    'bithumb',
    'bitibu',
    'bitkk',
    'bitlish',
    'bitmarket',
    'bitmex',
    'bitsane',
    'bitso',
    'bitstamp',
    'bitstamp1',
    'bittrex',
    'bitz',
    'bl3p',
    'bleutrade',
    'braziliex',
    'btcalpha',
    'btcbox',
    'btcchina',
    'btcexchange',
    'btcmarkets',
    'btctradeim',
    'btctradeua',
    'btcturk',
    'buda',
    'bxinth',
    'ccex',
    'cex',
    'chbtc',
    'chilebit',
    'cobinhood',
    'coinbase',
    'coinbaseprime',
    'coinbasepro',
    'coincheck',
    'coinegg',
    'coinex',
    'coinexchange',
    'coinfalcon',
    'coinfloor',
    'coingi',
    'coinmarketcap',
    'coinmate',
    'coinnest',
    'coinone',
    'coinspot',
    'cointiger',
    'coolcoin',
    'coss',
    'crex24',
    'crypton',
    'deribit',
    'dsx',
    'dx',
    'ethfinex',
    'exmo',
    'exx',
    'fcoin',
    'fcoinjp',
    'flowbtc',
    'foxbit',
    'fybse',
    'fybsg',
    'gateio',
    'gdax',
    'gemini',
    'getbtc',
    'hadax',
    'hitbtc',
    'hitbtc2',
    'huobipro',
    'huobiru',
    'ice3x',
    'independentreserve',
    'indodax',
    'itbit',
    'jubi',
    'kkex',
    'kraken',
    'kucoin',
    'kucoin2',
    'kuna',
    'lakebtc',
    'lbank',
    'liqui',
    'liquid',
    'livecoin',
    'luno',
    'lykke',
    'mandala',
    'mercado',
    'mixcoins',
    'negociecoins',
    'nova',
    'okcoincny',
    'okcoinusd',
    'okex',
    'okex3',
    'paymium',
    'poloniex',
    'quadrigacx',
    'rightbtc',
    'southxchange',
    'stronghold',
    'surbitcoin',
    'theocean',
    'therock',
    'tidebit',
    'tidex',
    'uex',
    'upbit',
    'urdubit',
    'vaultoro',
    'vbtc',
    'virwox',
    'xbtce',
    'yobit',
    'zaif',
    'zb',
]

base = [
    'Exchange',
    'exchanges',
    'decimal_to_precision',
]

__all__ = base + errors.__all__ + exchanges
