#!/usr/bin/env python3
from time import time
import Binance
import hmac
import hashlib
import json
import requests
from retrying import retry
import requests as req
import ApiKeyExchange
import LineNoty

key = ApiKeyExchange.Bittrex_Key
secret = ApiKeyExchange.Bittrex_SECRET
key_Encode          = key.encode()
secret_Encode       = secret.encode()
API_V1_1 = 'v1.1'
API_V2_0 = 'v2.0'
BASE_URL = 'https://bittrex.com/api/{version}/{method}?{paramvalue}'
EXCHANGE_NAME = "Bittrex"

class Bittrex(object):
	def __init__ (self, api_ver):
		self._api_ver = api_ver
		self._ReqUrlDict = dict ()

	@retry(stop_max_attempt_number=3)
	def get_json (self, url, sign = None):
		raw_dict = requests.get (url).json() if sign is None else requests.get(url, headers = {'apisign': sign}).json()
		if raw_dict ['success'] is True:
			return True, raw_dict['result'] 
		else:
			return False, raw_dict['message']
	
	@staticmethod
	def get_nonce ():
		return str(int(time() * 1000))

	@staticmethod
	def hmac_sign (msg_str, secret_bytes):
		return hmac.new (secret_bytes, msg_str.encode('utf-8'), hashlib.sha512).hexdigest()

class PublicAPI(Bittrex):
	def __init__ (self, api_ver):
		Bittrex.__init__ (self, api_ver)
		if api_ver == API_V1_1:
			self._ReqUrlDict = {'GET_MARKETS'	: BASE_URL.format (version = self._api_ver, method = 'public/getmarkets', paramvalue = '') [:-1],
				'GET_CURRENCIES'  : BASE_URL.format (version = self._api_ver, method = 'public/getcurrencies', paramvalue = '') [:-1],
				'GET_TICKER'	: BASE_URL.format (version = self._api_ver, method = 'public/getticker', paramvalue = 'market={mar}'),
				'GET_24HSUMALL'	: BASE_URL.format (version = self._api_ver, method = 'public/getmarketsummaries', paramvalue = '') [:-1],
				'GET_24HSUM'		 : BASE_URL.format (version = self._api_ver, method = 'public/getmarketsummary', paramvalue = 'market={mar}'),
				'GET_ORDERBOOK'  : BASE_URL.format (version = self._api_ver, method = 'public/getorderbook', paramvalue = 'market={mar}&type={typ}'),
				'GET_HISTORY'		: BASE_URL.format (version = self._api_ver, method = 'public/getmarketsummary', paramvalue = 'market={mar}'),
			  }
		elif api_ver == API_V2_0:
			self._ReqUrlDict = {'GET_BTC_PRICE': BASE_URL.format (version = self._api_ver, method = 'pub/currencies/GetBTCPrice', paramvalue = '')[:-1],
								'GET_TICKS': BASE_URL.format (version = self._api_ver, method = 'pub/market/GetTicks', paramvalue = 'marketName={mar}&tickInterval={itv}'),
								'GET_LATESTTICK': BASE_URL.format (version = self._api_ver, method = 'pub/market/GetLatestTick', paramvalue = 'marketName={mar}&tickInterval={itv}'),
							   }

	# These functions are dedicated for API_V1_1
	def get_markets (self):
		reqUrl = self._ReqUrlDict['GET_MARKETS']
		return self.get_json (reqUrl)

	def get_currencies (self):
		reqUrl = self._ReqUrlDict['GET_CURRENCIES']
		return self.get_json (reqUrl)

	def get_ticker (self, market):
		reqUrl = self._ReqUrlDict['GET_TICKER'].format (mar = market)
		return self.get_json (reqUrl)

	def get_24h_sum (self, market = ''):
		reqUrl = self._ReqUrlDict['GET_24HSUMALL'] if market is '' else self._ReqUrlDict['GET_24HSUM'].format (mar = market)
		return self.get_json (reqUrl)

	def get_order_book (self, market):
		reqUrl = self._ReqUrlDict['GET_ORDERBOOK'].format (mar = market, typ = 'sell')
		return self.get_json (reqUrl)

	def get_history (self, market):
		reqUrl = self._ReqUrlDict['GET_HISTORY'].format (mar = market)
		return self.get_json (reqUrl)

	# These functions are dedicated for API_V2_0
	def get_btc_price (self):
		reqUrl = self._ReqUrlDict['GET_BTC_PRICE']
		return self.get_json (reqUrl)

	def get_ticks (self, market, interval, only_lastest = False):
		"""
		interval supports only ['oneMin', 'fiveMin', 'thirtyMin', 'hour', 'day']
		"""
		if only_lastest:
			reqUrl = self._ReqUrlDict['GET_LATESTTICK'].format (mar=market, itv = interval)
		else:
			reqUrl = self._ReqUrlDict['GET_TICKS'].format (mar=market, itv = interval)
		return self.get_json (reqUrl)

class MarketAPI(Bittrex):
	def __init__ (self, api_ver, api_key, api_secret):
		Bittrex.__init__ (self, api_ver)
		self._api_key = api_key
		self._secret = api_secret
		self._ReqUrlDict ['BUY_LIMIT']	  = BASE_URL.format (version = self._api_ver, method = 'market/buylimit', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&market={mar}&quantity={qty}&rate={rate}')
		self._ReqUrlDict ['SELL_LIMIT']	 = BASE_URL.format (version = self._api_ver, method = 'market/selllimit', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&market={mar}&quantity={qty}&rate={rate}')
		self._ReqUrlDict ['CANCEL']	= BASE_URL.format (version = self._api_ver, method = 'market/cancel', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&uuid={o_uuid}')
		self._ReqUrlDict ['GET_OPENORDERS']	  = BASE_URL.format (version = self._api_ver, method = 'market/getopenorders', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&market={mar}')
		self._ReqUrlDict ['GET_OPENALLORDERS'] = BASE_URL.format (version = self._api_ver, method = 'market/getopenorders', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}')

	def buy_limit (self, market, quantity, rate):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['BUY_LIMIT'].format (no = nonce, mar = market, qty = quantity, rate = rate)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def sell_limit (self, market, quantity, rate):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['SELL_LIMIT'].format (no = nonce, mar = market, qty = quantity, rate = rate)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def cancel (self, uuid):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['CANCEL'].format (no = nonce, o_uuid = uuid)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))
	
	def get_open_orders (self, market = ''):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['GET_OPENORDERS'].format (no = nonce, mar = market) if market is not '' else self._ReqUrlDict['GET_OPENALLORDERS'].format(no = nonce) 
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))



class AccountAPI(Bittrex):
	def __init__ (self, api_ver, api_key, api_secret):
		Bittrex.__init__ (self, api_ver)
		self._api_key = api_key
		self._secret = api_secret
		self._ReqUrlDict ['GET_BALANCES']	 = BASE_URL.format (version = self._api_ver, method = 'account/getbalances', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}')
		self._ReqUrlDict ['GET_BALANCE']	= BASE_URL.format (version = self._api_ver, method = 'account/getbalance', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}')
		self._ReqUrlDict ['GET_DEPOSITADDR']	 = BASE_URL.format (version = self._api_ver, method = 'account/getdepositaddress', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}')
		self._ReqUrlDict ['WITHDRAW_NOPID']	 = BASE_URL.format (version = self._api_ver, method = 'account/withdraw', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}&quantity={qty}&address={addr}')
		self._ReqUrlDict ['WITHDRAW']	 = BASE_URL.format (version = self._api_ver, method = 'account/withdraw', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}&quantity={qty}&address={addr}&paymentid={pid}')
		self._ReqUrlDict ['GET_ORDER']	= BASE_URL.format (version = self._api_ver, method = 'account/getorder', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&uuid={o_uuid}')
		self._ReqUrlDict ['GET_ALLORDERHIS']	  = BASE_URL.format (version = self._api_ver, method = 'account/getorderhistory', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}')
		self._ReqUrlDict ['GET_ORDERHIS']	 = BASE_URL.format (version = self._api_ver, method = 'account/getorderhistory', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&market={mar}')
		self._ReqUrlDict ['GET_WITHDRAWALHIS'] = BASE_URL.format (version = self._api_ver, method = 'account/getwithdrawalhistory', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}')
		self._ReqUrlDict ['GET_DEPOSITHIS']		= BASE_URL.format (version = self._api_ver, method = 'account/getdeposithistory', paramvalue = 'apikey=' + self._api_key.decode() + '&nonce={no}&currency={cur}')

	def get_balance (self, cur = ''):
		nonce = self.get_nonce()
		reqUrl = self._ReqUrlDict['GET_BALANCES'].format(no = nonce) if cur is '' else self._ReqUrlDict['GET_BALANCE'].format (no = nonce, cur = cur)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def get_deposit_addr (self, cur):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['GET_DEPOSITADDR'].format (no = nonce, cur = cur)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def withdraw (self, cur,  qty, addr, pid = None):
		nonce = self.get_nonce ()
		if pid is None:
			reqUrl = self._ReqUrlDict ['WITHDRAW_NOPID'].format (no = nonce, cur = cur, qty = qty, addr = addr)
		else:
			reqUrl = self._ReqUrlDict ['WITHDRAW'].format (no = nonce, cur = cur, qty = qty, addr = addr, pid = pid)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def get_order (self, uuid):
		nonce = self.get_nonce()
		reqUrl = self._ReqUrlDict ['GET_ORDER'].format (no = nonce, o_uuid = uuid)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def get_order_history (self, market = ''):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['GET_ALLORDERHIS'].format (no = nonce) if market is '' else self._ReqUrlDict ['GET_ORDERHIS'].format (no = nonce, mar = market)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def get_withdrawal_history (self, cur):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['GET_WITHDRAWALHIS'].format (no = nonce, cur = cur)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

	def get_deposit_history (self, cur):
		nonce = self.get_nonce ()
		reqUrl = self._ReqUrlDict ['GET_DEPOSITHIS'].format (no = nonce, cur = cur)
		return self.get_json (reqUrl, self.hmac_sign (reqUrl, self._secret))

public_API   = PublicAPI (API_V1_1)
market_API   = MarketAPI (API_V1_1, key_Encode, secret_Encode)
account_API  = AccountAPI(API_V1_1, key_Encode, secret_Encode)
#---------------------------------------------------------------------------------------------------------
def GetBalance(symbol):
        #Example
        #               balance = Getbalance('XRP')
        #               return float 
        try :
            status, jsonbalance = account_API.get_balance(symbol)
            balance = float(json.dumps(jsonbalance["Available"]))
            return balance
        except Exception as ex:
                print(str(ex)+"at GetBalance("+symbol+")")


def BuyOrder(sourceSymbol, tragetSymbol,volume,isRebalance = False):
        #[Example]      
        #               status = BuyOrder('USD','XRP',True,5)
        #               status = BuyOrder('USD','XRP',False)
        #               return boolean
        try :
                market = str(sourceSymbol+"-"+tragetSymbol)
                status_market , json_ask_order = public_API.get_order_book(market)
                i=0
                amountToBuy = 0

                mintrade_size_USDT = 0.0005*float(json.dumps(public_API.get_ticker('USDT-BTC')[1]['Last']))

                
                if isRebalance == True :
                        fee = volume*25/10000
                        volume_after_paid_fee = volume - fee
                        while True:
                                qty_from_order  = float(json.dumps(json_ask_order[i]['Quantity']))
                                rate = float(json.dumps(json_ask_order[i]['Rate']))
                                value_order = qty_from_order*rate
                                if value_order >= volume_after_paid_fee :
                                        qty = volume_after_paid_fee/rate
                                        amountToBuy += qty
                                        status , jsonbuy = market_API.buy_limit (market,qty, rate)
                                        print(jsonbuy)
                                        break
                                else :
                                        amountToBuy += qty_from_order
                                        status , jsonbuy = market_API.buy_limit (market,qty_from_order, rate)
                                        volume_after_paid_fee  = volume_after_paid_fee - value_order
                                        i=i+1
                                if volume_after_paid_fee < mintrade_size_USDT : break
                else :
                        source = volume
                        fee  = source*25/10000
                        source_after_paid_fee = source - fee
                        while True:
                                qty_from_order  = float(json.dumps(json_ask_order[i]['Quantity']))
                                rate = float(json.dumps(json_ask_order[i]['Rate']))
                                value_order = qty_from_order*rate
                                if value_order >= source_after_paid_fee :
                                        qty = source_after_paid_fee/rate
                                        amountToBuy += qty
                                        status , jsonbuy = market_API.buy_limit (market,qty, rate)
                                        break
                                else :
                                        amountToBuy += qty_from_order
                                        status , jsonbuy = market_API.buy_limit (market,qty_from_order, rate)
                                        source_after_paid_fee  = source_after_paid_fee - value_order
                                        i=i+1
                                if source_after_paid_fee > mintrade_size_USDT : break
                
                LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amountToBuy))
                return status
        except Exception as ex :
                print(str(ex)+"at BuyOrder("+sourceSymbol+","+tragetSymbol+","+str(volume)+","+isRebalance+")")




def SellOrder(sourceSymbol , tragetSymbol):
        #[Example]
        #               status =  SellOrder('XRP','USD')
        #               return boolean
        try :
                market = str(sourceSymbol+"-"+tragetSymbol)
                qty    = GetBalance(tragetSymbol)

                # price = public_API.get_ticker(market)
                # rate   = round(price[1]["Bid"]*(90/100),999999)

                rate   = round(float(public_API.get_ticker(market)[1]["Bid"])*(90/100),999999)
                status , jsonSellOrder =  market_API.sell_limit(market,qty,rate)
                print(jsonSellOrder)
                LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(qty))
                return status
        except Exception as ex:
                print(str(ex) + "at SellOrder(" +sourceSymbol+","+tragetSymbol+")")


def GetTradeStatus(tragetSymbol):
        #[Example]
        #               status =  GetTradeStatus('XRP')
        #               return boolean
        try :
               json_currencies = public_API.get_currencies()[1]
               amount_COIN = len(json_currencies)
               for a in range(0,amount_COIN-1):
                        if json_currencies[a]["Currency"] == tragetSymbol:
                                status = json_currencies[a]["IsActive"]
                                return status
        except Exception as ex:
                print(str(ex)+'at GetTradeStatus('+tragetSymbol+')')

def GetBaseAddress(tragetSymbol):
       try :
              tragetSymbol = tragetSymbol.upper()
              json_currencies = public_API.get_currencies()[1]
              amount_COIN = len(json_currencies)
              for a in range(0,amount_COIN-1):
                       if json_currencies[a]["Currency"] == tragetSymbol:
                               status = json_currencies[a]["BaseAddress"]
                               return status
              return ''
       except Exception as ex:
               print(str(ex)+'at GetTradeStatus('+tragetSymbol+')')
               

def transferOrder(symbol,tragetAccount, addressTag="",exchange=""):
        #[Example]
        #               status =  transferOrder('XRP','rwADVoWvXxAS3d61zpUta7ZuPWsiEQXxPY','108076')
        #               return boolean
        
        try :
                qty = GetBalance(symbol)
                if addressTag != "":
                       status , json_Return = account_API.withdraw ( symbol,  qty, tragetAccount, addressTag)
                       LineNoty.notiForTransfer(symbol,tragetAccount, str(qty) , EXCHANGE_NAME , addressTag, exchange)
                       return status
                else :
                       status , json_Return = account_API.withdraw(symbol ,  qty, tragetAccount, pid = None)
                       LineNoty.notiForTransfer(symbol,tragetAccount, str(qty) , EXCHANGE_NAME , addressTag, exchange)
                       return status
        except Exception as ex:
                print(str(ex)+ 'at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')

def GetTradeFee():
        #[Example]
        #               tradeFee = GetTradeFee()
        #               return float
        tradefee = 0.25
        return tradefee

# def GetFeePrice(sourceSymbol):
#         #[Example]
#         #               fee_tranfer = GetFeePrice('ETH')
#         #               return float
#         try:
#                 json_currencies = public_API.get_currencies()[1]                
#                 amount_COIN = len(json_currencies)
#                 for a in range(0,amount_COIN-1):
#                         if json_currencies[a]["Currency"] == sourceSymbol:
#                                 fee_transfer = float(json_currencies[a]['TxFee'])
#                 return fee_transfer
        
#         except Exception as ex : 
#                 print(str(ex) + ' at GetFeePrice()')

def CheckPendingOrder(symbol):
        #[Example]
        #               status =  GetTradeStatus('XRP')
        #               return  boolean
        #                       False = ไม่มี Pending
        #                       True  = Pending อยู่
        try :   
                A,jsonWithdrawal_history = account_API.get_withdrawal_history(symbol)
                Val_Pending = json.dumps(jsonWithdrawal_history[0]["PendingPayment"])
                if str(Val_Pending) == "false" :
                        status,str_pending = False,"NOT PENDING"
                        return status
                else :
                        if str(Val_Pending) == "true":
                                status,str_pending = True,"PENDING"
                                return status
                
                        else :
                                return Val_Pending 
        except Exception as ex :
                      print(str(ex) + ' at CheckPendingOrder('+symbol+')')    



def GetAccount(symbol ,exchange=""):

        try:
                address =''
                tag =''
                boolean ,addressJson  = account_API.get_deposit_addr(symbol)


                bittrexBase = GetBaseAddress(symbol)
                if not boolean and addressJson == 'ADDRESS_GENERATING':
                        time.sleep(1)
                        addressJson = account_API.get_deposit_addr(symbol)

                        address = addressJson['Address']
                        tag = ''
                
                if checkCoinHasTag(symbol):
                        address = bittrexBase
                        tag = addressJson['Address']
                else:
                        address = addressJson['Address']


                return address , tag


        except Exception as ex :
                print(str(ex) + ' at GetBittrexAccount('+symbol+')')


def getMinimumBuySource(sourceSymbol,tragetSymbol):
    try:

        sourceSymbol = sourceSymbol.upper()
        
        if sourceSymbol == "USDT":
           return 10
        if sourceSymbol == "BTC":
           return 0.0012104


        getText = req.get('https://api.bittrex.com/api/v1.1/public/getorderbook?market='+sourceSymbol+"-"+tragetSymbol+"&type=sell")
        jsonConvertedRate = json.loads(getText.text)
        return float(jsonConvertedRate['result'][0]['Rate'])/10
    except Exception as ex :
          print(str(ex) + ' at getMinimumBuySource('+sourceSymbol+','+tragetSymbol+')')



def GetFeePrice():
        try:

                
                jsonResult = { 'tradeFee':0.25,
                         'transfer':{'BTC': 0.0005, 'LTC': 0.01, 'DOGE': 2.0, 'VTC': 0.02, 'PPC': 0.02, 'FTC': 0.2, 'RDD': 2.0, 'NXT': 2.0, 'DASH': 0.05, 'POT': 0.002, 'BLK': 0.02, 'EMC2': 0.2, 'XMY': 0.2, 'GRS': 0.2, 'NLG': 0.2, 'MONA': 0.2, 'VRC': 0.0002, 'CURE': 0.0002, 'XMR': 0.0001, 'XDN': 0.02, 'NAV': 0.2, 'XST': 0.02, 'VIA': 0.2, 'PINK': 0.2, 'IOC': 0.2, 'SYS': 0.0002, 'DGB': 0.2, 'BTS': 5.0, 'BURST': 2.0, 'EXCL': 0.2, 'BLOCK': 0.02, 'XRP': 1.0, 'GAME': 0.2, 'NXS': 0.2, 'XCP': 0.2, 'XVG': 5.0, 'GEO': 0.1, 'FLDC': 150.0, 'FLO': 0.2, 'XEM': 4.0, 'MUE': 0.02, 'GAM': 0.45, 'SPHR': 0.1, 'OK': 0.2, 'AEON': 0.1, 'ETH': 0.01, 'EXP': 0.01, 'OMNI': 0.1, 'USDT': 1.0, 'AMP': 5.0, 'XLM': 0.01, 'RVR': 0.1, 'EMC': 0.02, 'FCT': 0.01, 'MAID': 2.0, 'SLS': 0.002, 'RADS': 0.2, 'DCR': 0.01, 'PIVX': 0.02, 'MEME': 0.02, 'STEEM': 0.01, 'LSK': 0.1, 'DGD': 0.038, 'WAVES': 0.001, 'LBC': 0.02, 'SBD': 0.01, 'ETC': 0.01, 'STRAT': 0.2, 'REP': 0.05, 'ARDR': 2.0, 'XZC': 0.1, 'NEO': 0.025, 'ZEC': 0.01, 'UBQ': 0.01, 'KMD': 0.002, 'SIB': 0.2, 'ION': 0.2, 'CRW': 0.02, 'SWT': 4.5, 'MLN': 0.1, 'ARK': 0.1, 'INCNT': 0.1, 'GBYTE': 0.002, 'GNT': 9.0, 'NXC':
94.0, 'EDG': 3.0, 'MORE': 4.0, 'WINGS': 8.0, 'RLC': 3.0, 'GNO': 0.05, 'GUP': 39.0, 'HMQ': 74.0, 'ANT': 1.3, 'ZEN': 0.002, 'SC': 0.1, 'BAT': 4.5, '1ST': 4.5, 'QRL': 0.1, 'PTOY': 41.0, 'BNT': 1.05, 'NMR': 0.2, 'SNT': 29.0, 'DCT': 0.1, 'XEL': 0.2, 'MCO': 0.25, 'ADT': 109.0, 'FUN': 49.0, 'PAY': 3.5, 'MTL': 2.5, 'STORJ': 4.5, 'ADX': 7.0, 'OMG': 0.45, 'CVC': 12.0, 'PART': 0.1, 'QTUM': 0.01, 'BCH': 0.001, 'DNT': 55.0, 'ADA': 1.0, 'MANA': 15.0, 'SALT': 3.0,
'TIX': 5.0, 'RCN': 56.0, 'VIB': 26.0, 'MER': 0.1, 'POWR': 8.0, 'ENG': 2.0, 'UKG': 27.0, 'IGNIS': 2.0, 'SRN': 18.0, 'WAXP': 0.1, 'ZRX': 2.0, 'VEE': 139.0, 'BCPT': 21.0, 'TRX': 0.003, 'TUSD': 1.0, 'LRC': 15.0, 'UP': 48.0, 'DMT':
3.0, 'POLY': 5.0, 'PRO': 5.0, 'BLT': 19.0, 'STORM': 195.0, 'AID': 29.0, 'NGC': 9.0, 'GTO': 26.0, 'OCN': 189.0, 'USD': 0.0, 'TUBE': 0.01, 'CBC': 82.0, 'CMCT': 1.0, 'NLC2': 0.1, 'BKX': 30.0, 'MFT': 216.0, 'LOOM': 14.0, 'RFR': 310.0, 'RVN': 1.0, 'BFT': 29.0, 'GO': 0.01, 'HYDRO': 365.0, 'UPP': 44.0, 'ENJ': 18.0, 'MET': 0.95, 'DTA': 30.0, 'EDR': 16.0, 'BOXX': 15.0, 'IHT': 54.0, 'XHV': 0.01, 'NPXS': 1368.0, 'PMA': 1026.0, 'PAL': 1.0, 'PAX': 1.0, 'ZIL': 0.001, 'MOC': 17.0, 'OST': 28.0, 'SPC': 110.0, 'MED': 10.0, 'BSV': 0.001, 'IOST': 1.0, 'XNK': 133.0, 'NCASH': 328.0, 'SOLVE': 1.25, 'USDS': 1.0, 'JNT': 16.0, 'LBA': 56.0, 'MOBI': 1.0, 'DENT': 530.0, 'DRGN': 7.0, 'VITE': 1.0, 'IOTX': 1.0, 'BTM': 0.1, 'ELF': 1.0, 'QNT': 0.2, 'BTU': 1.0, 'SPND': 1.0, 'BTT': 1.0, 'NKN': 1.0, 'GRIN': 0.1, 'CTXC': 1.0, 'HXRO': 1.0, 'SERV': 1.0, 'META': 0.006, 'FSN': 1.0, 'HST': 1.0, 'ANKR': 1.0, 'TRAC': 1.0, 'CRO': 1.0, 'ONT': 1.0, 'ONG': 0.1, 'AERGO': 1.0, 'TTC': 0.006, 'SLT': 0.1, 'PTON': 1.0, 'PI': 1.0, 'PLA': 1.0, 'ART': 1.0, 'VBK': 0.1, 'ORBS': 1.0, 'BORA': 1.0, 'CND': 1.0, 'TRIO': 1.0, 'FX': 1.0, 'ATOM': 0.01, 'OCEAN': 1.0, 'XYO': 1.0, 'WIB': 1.0, 'BWX': 1.0, 'SNX': 1.0, 'SUSD': 1.0, 'VDX': 1.0, 'COSM': 1.0, 'OGO': 1.0, 'ITM': 1.0, 'STPT': 1.0, 'LAMB': 1.0, 'FET': 1.0, 'MKR': 0.001, 'DAI': 1.0, 'CPT': 1.0, 'FNB': 1.0, 'ABT': 1.0, 'PROM': 1.0, 'FTM': 1.0, 'ABYSS': 1.0, 'EOS': 0.1, 'FXC': 1.0, 'DUSK': 1.0, 'URAC': 1.0, 'BLOC': 1.0, 'BRZ': 1.0, 'LUNA': 0.01, 'TEMCO': 10.0, 'SPIN': 10.0, 'HINT': 1.0, 'CHR': 1.0, 'TUDA': 1.0, 'UTK': 1.0, 'PXL': 1.0, 'BTXCRD': 0.01, 'AKRO': 1.0, 'TSHP': 1.0, 'HEDG': 1.0, 'MRPH': 1.0, 'HBAR': 0.1, 'VET': 0.01, 'PLG': 1.0, 'SIX': 0.1, 'WGP': 1.0, 'APM': 1.0, 'FLETA': 1.0, 'BLTV': 1.0, 'HDAC': 0.01, 'HYC': 1.0}}

                return jsonResult
        except Exception as ex :
                print(str(ex) + ' at GetFeePrice()')


def GetInfo(source,market):
        try:
                market = source+'-'+market
                getText = req.get("https://api.bittrex.com/v3/markets/"+market)
                jsontext = json.loads(getText.text)
        
                jsoninfo = {'mintrade' : jsontext['minTradeSize'],
                            'precision':jsontext['precision']
                            }
                
                return jsoninfo
        except Exception as ex :
                print(str(ex))

def bittrexConvertAPIProblem(aBoke):   
 
  bids = []
  asks = []
  try:

          sonList = json.loads(aBoke)

          for bid in sonList['bid']:  
                bids.append([bid['rate'],bid['quantity']])

          for ask in sonList['ask']:
                asks.append([ask['rate'],ask['quantity']])
                
          return json.dumps( {'bids':bids,'asks':asks})


  except Exception as ex:
          print(ex)

def getCoinInfo(sourceSymbol , tragetSymbol = ""):
   try:
     sourceSymbol = sourceSymbol.upper()
     tragetSymbol = tragetSymbol.upper()
     getText = req.get('https://api.bittrex.com/api/v1.1/public/getcurrencies')
     jsonConvertedRate = json.loads(getText.text)
     checkCount = 0
     numCheck = 2
     if tragetSymbol == "":
            numCheck = 1 


     if jsonConvertedRate['success']:
        for item in jsonConvertedRate['result']:
           if item['Currency'] == sourceSymbol or item['Currency'] == tragetSymbol:
                checkCount = checkCount + 1
                if not item['IsActive']:
                        return False
     else:
        return False

     
     
     if checkCount < numCheck:
        return False
     
     if tragetSymbol != "":
        symbol = sourceSymbol + "-" + tragetSymbol
        r = req.get('https://api.bittrex.com/api/v1.1/public/getorderbook?market='+symbol+'&type=both')
        jsonConverted = json.loads(r.text)

        if jsonConverted['success']:
                if len(jsonConverted['result']["buy"]) == 0 and len(jsonConverted['result']["sell"]) == 0 :
                        return False  
        else:
                return False 

     return True
   except Exception as ex:
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+', '+tragetSymbol+')')
    return False 

def checkCoinHasTag(symbol=''):
  try:
    symbol = symbol.upper()
    hasTag = False
    address , tag = Binance.GetAccount(symbol)
    if tag != '':
      hasTag = True

    return hasTag
  except Exception as ex:
    print(str(ex) + ' at checkCoinHasTag('+symbol+')')

def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')