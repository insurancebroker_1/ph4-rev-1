#!/usr/bin/python
# /examples/order_book.py

# Sample script showing how order book syncing works.

from __future__ import print_function
from time import sleep
import json
import asyncio
import _thread 
from bittrex_websocket import OrderBook


COIN_ARRAY=[]
COIN_PRICE={}
class MySocket(OrderBook):
        def on_ping(self, msg):
            a=1


def wss_get_price(symbol , symbolJson):
    # Create the socket instance
    ws = MySocket()
    global COIN_PRICE
    # Enable logging
    # ws.enable_log()
    # Define tickers
    tickers = [symbol]
    # Subscribe to order book updates
    ws.subscribe_to_orderbook(tickers)

    while True:
        book = ws.get_order_book(symbol)
        if  book:
            for itemAsk in book['S']:
                COIN_PRICE[symbolJson]["asks"][itemAsk["R"]] = itemAsk["Q"]

            for itemBid in book['Z']:
                COIN_PRICE[symbolJson]["bids"][itemBid["R"]] = itemBid["Q"]

           
    else:
        pass


def run_wss_get_price(sourceSymbol,tragetSymbol = "") :
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price(sourceSymbol,tragetSymbol))

def run_thread_wss_get_price(sourceSymbol,tragetSymbol = "") :
    # create_file_txt(sourceSymbol,tragetSymbol)
    global COIN_ARRAY
    COIN_PRICE[tragetSymbol] = {
                "asks":{},
                "bids":{}
            }
    _thread.start_new_thread( run_wss_get_price, (sourceSymbol,tragetSymbol) )




def setWsExchangeSelectSymbol():
    try:
        for  symbol in COIN_ARRAY:
            symbolJson = symbol.replace("-","")
            
            run_thread_wss_get_price(symbol , symbolJson)


    except Exception as ex : 
        print(str(ex) + ' at setWsExchangeSelectSymbol()')

def getCOIN_PRICE():
    global COIN_PRICE
    # for item in COIN_PRICE:
    #   COIN_PRICE[item]["asks"] =    json.loads(json.dumps(COIN_PRICE[item]["asks"], sort_keys=True))
    #   COIN_PRICE[item]["bids"] =    json.loads(json.dumps(COIN_PRICE[item]["bids"], sort_keys=False))

    return COIN_PRICE

