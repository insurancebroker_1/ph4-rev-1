import requests

API_ENDPOINT_ADDLOG = "http://149.28.154.112:5201/logbot"
API_ENDPOINT_ERROR_ADDLOG = "http://149.28.154.112:5201/logError"

def addLog(datetime,exchange,botid,activity,source,amountsource,traget,amounttraget,endingsource,endingtraget,averagerate,detail):
  dataValue = {
      "datetime" : datetime,
      "exchange" : exchange,
      "botid" : botid,
      "activity" : activity,
      "source" : source,
      "amountsource" : amountsource,
      "traget" : traget,
      "amounttraget" : amounttraget,
      "endingsource" : endingsource,
      "endingtraget" : endingtraget,
      "averagerate" : averagerate,
      "detail" : detail
  }
  r = requests.post(url = API_ENDPOINT_ADDLOG, data = dataValue)

def addErrorLog(datetimeerror,botid,detail):
  dataValue = {
      "datetimeerror" : datetimeerror,
      "botid" : botid,
      "detail" : detail
  }
  r = requests.post(url = API_ENDPOINT_ERROR_ADDLOG, data = dataValue)