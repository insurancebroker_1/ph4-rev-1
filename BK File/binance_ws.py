import asyncio
import requests as req
import websockets
import json
import time
import _thread 
import time
import os, ssl
import certifi

ssl_context = ssl.create_default_context(cafile=certifi.where())


COIN_ARRAY=[]
COIN_PRICE={}

# def create_file_txt(sourceSymbol,tragetSymbol):
#         name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
#         f = open(name_file, "w")
#         f.write('wait')
#         f.close()
#         time.sleep(1)
async def wss_get_price(sourceSymbol,tragetSymbol=""):
   
    symbol =  tragetSymbol+sourceSymbol
    
    async with websockets.connect('wss://stream.binance.com:9443/ws/'+symbol.lower()+'@depth20@100ms' , ssl=ssl_context) as websocket:
        # name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
        global COIN_PRICE
        try:
            while True :
                response = await websocket.recv()
                response_json = json.loads(response)
                COIN_PRICE[symbol] = response_json
        except Exception as ex : 
            wss_get_price(sourceSymbol,tragetSymbol)
            #raise Exception('reloop')
            # f = open(name_file, "w")
            # f.write(response_bids_price)
            # f.close()

async def wss_get_price_symbol(Symbol):
   
    symbol =  Symbol
    
    async with websockets.connect('wss://stream.binance.com:9443/ws/'+symbol.lower()+'@depth20@100ms' , ssl=ssl_context) as websocket:
        # name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
        global COIN_PRICE
        try:
            while True :
                response = await websocket.recv()
                response_json = json.loads(response)
                COIN_PRICE[symbol] = response_json
        except Exception as ex : 
            print(str(ex) + ' at wss_get_price('+Symbol+')')
            return False
            # f = open(name_file, "w")
            # f.write(response_bids_price)
            # f.close()
        
                       
def run_wss_get_price(sourceSymbol,tragetSymbol = "") :
    try:
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price(sourceSymbol,tragetSymbol))
    except Exception as ex :
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price(sourceSymbol,tragetSymbol))

def run_wss_get_price_symbol(Symbol) :
        asyncio.get_event_loop_policy().new_event_loop().run_until_complete(wss_get_price_symbol(Symbol))

def run_thread_wss_get_price(sourceSymbol,tragetSymbol = "") :
    sourceSymbol  =  sourceSymbol.upper()
    tragetSymbol  =  tragetSymbol.upper()
    global COIN_PRICE
    symbol =  tragetSymbol+sourceSymbol
    COIN_PRICE[symbol] = {}
    # create_file_txt(sourceSymbol,tragetSymbol)
    _thread.start_new_thread( run_wss_get_price, (sourceSymbol,tragetSymbol) )




def run_thread_wss_get_price_symbol(Symbol) :
    global COIN_PRICE
    Symbol =  Symbol.upper()
    COIN_PRICE[Symbol] = {}
    # create_file_txt(sourceSymbol,tragetSymbol)
    _thread.start_new_thread( run_wss_get_price_symbol, (Symbol) )
# def get_price_bid(sourceSymbol,tragetSymbol):
#     sourceSymbol  =  sourceSymbol.lower()
#     tragetSymbol  =  tragetSymbol.lower()
#     f  = open('get_price_'+sourceSymbol+tragetSymbol+'.txt','r')
#     value = f.read()
#     while True:
#         if value == 'wait':
#             f  = open('get_price_'+sourceSymbol+tragetSymbol+'.txt','r')
#             value = f.read()
#             time.sleep(1)
#             continue
#         else :
#             return float(value)

def setWsExchangeSymbol(): #<--- แก้หัวข้อตามนี้
  try:

    # print(chr(27) + "[2J")
    # print('Balance : '+ str(GetBalance(tragetSymbol)))
    r = req.get('https://api.binance.com/api/v1/exchangeInfo')
    jsonConvert = json.loads(r.text)
    print(len(jsonConvert['symbols']))
    
    for  index , symbol in enumerate(jsonConvert['symbols'] , start=1):
        run_thread_wss_get_price(symbol['quoteAsset'] , symbol['baseAsset'])
        # asyncio.get_event_loop_policy().new_event_loop().run_until_complete (wss_get_price(symbol['quoteAsset'] , symbol['baseAsset']))
        # if index < 45 :
            # run_thread_wss_get_price(symbol['quoteAsset'] , symbol['baseAsset'])

  except Exception as ex : 
    print(str(ex) + ' at setWsExchangeSymbol()')



def setWsExchangeSelectSymbol():
    try:

        for    symbol in COIN_ARRAY:
            run_thread_wss_get_price(symbol)

    except Exception as ex : 
        print(str(ex) + ' at setWsExchangeSelectSymbol()')

async def wss_trade_steam(symbol):
   
    async with websockets.connect('wss://stream.binance.com:9443/ws/'+symbol.lower()+'@depth20@100ms' , ssl=ssl_context) as websocket:
        # name_file = 'get_price_'+sourceSymbol+tragetSymbol+'.txt'
        global COIN_PRICE
        try:
            while True :
                response = await websocket.recv()
                response_json = json.loads(response)
                COIN_PRICE[symbol] = response_json
        except Exception as ex : 
            print(str(ex) + ' at wss_get_price('+sourceSymbol+','+tragetSymbol+')')
            return False
            # f = open(name_file, "w")
            # f.write(response_bids_price)
            # f.close()




