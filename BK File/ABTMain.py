import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio  
import BX 
import Binance
import Poloniex
import Okex
import BrokerList
import Bitfinex
import LineNoty
import ConfigBot
import ApiSetting
import Binance
import StatToDb
import _thread

IsOnBuying = False
IsOnSelling = False
coinAfterWithdraw = 0

MajorCoins = ['BTC','ETH'] # 
XCoins = ['KEY','VET']#,'XVG','XEM','GNT'] #  
listNeedConvertCoins = ['POWR','REP','EVX','XZC']  
listTHBNeedConvertCoins = ['ZEC','DOGE']
coinABTPercentage = {'XRP':0.5,'BTC':0.5,'ETH':0.5,'EOS':0.5,'BCH':0.5,'XLM':0.5,'ZRX':0.5,'LTC':0.5,'NEO':0.5,'TRX':0.5,'XTZ':0.5,'ATOM':0.5,'NANO':0.5,'BSV':0.5,'LINK':0.5}
IsMultipleCoin = True #<----------------------------- if false it will making fix only CoinType of onec.
CoinType ='ETH'
IsRealMode = True #<----------------------------- use cost from any accounts.

usdtRate = Bitfinex.getRateUsdUsdt()
minimumTHBToTrade = 3000
biws = BrokerList.getbiWS()

 

def getDataAndCalFromBrokeToBuy(XCoin,balace,tradeFee): 
  try:
    
    rawBalace = balace
    listOrder = []
    listOfCost = []            
    _balace = rawBalace  
     
    buyJsonList = biws[XCoin+"USDT"]
    if len(buyJsonList) == 0:
      return 0,0
    if len(buyJsonList['asks'])  == 0:
      return 0,0
    # data = req.get("https://www.binance.com/api/v1/depth?symbol="+XCoin+"USDT").text
     
    # buyJsonList = json.loads(data)   
  
    for ask in buyJsonList['asks']:      
      
      if _balace - ((float(ask[0])*float(ask[1]))) >= 0 :      
        _balace = _balace - (float(ask[0])*float(ask[1]))        
        listOrder.append(float(ask[1])-(float(ask[1])*tradeFee/100)) ## get index 0 or 1 ?
        listOfCost.append((float(ask[0])*float(ask[1])))
      if _balace  > 0  and _balace < (float(ask[0])*float(ask[1]))  :        
        listOrder.append((_balace/float(ask[0]))-((_balace/float(ask[0])*tradeFee)/100))
        listOfCost.append(_balace)
        _balace=0
      
    
    sumOrder = sum([float(i) for i in listOrder])
    sumCost = sum([float(i) for i in listOfCost])
    
    



    # print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    # print('Total of Orders you got from '+brokeName+' : before => ',BeforesumOrder, ' after => ',sumOrder ,coinType+' and your\'s cost is :', sumCost ,'USDt Transfer Fee : ',transferCost)
    # print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    return sumOrder,sumCost
  except Exception as ex:
    print(str(ex) +' at getDataAndCalFromBrokeToBuy()')
    time.sleep(5)
    main()

def getDataAndCalFromBrokeToSell(XCoin,MCoin,sumOrder,sumCost,Mainbalace,groupType,tradeFee):
  try:  
    # vvv4.1 
    if sumOrder==0 and sumCost == 0 :
      return -99999
    if groupType == '1' :
      global percentProfit
       
      balace = 0
      rateBid = 0
      listOrder = []     
      # data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin).text
      # sellJsonList = json.loads(data)
      sellJsonList = biws[MCoin]
      if len(sellJsonList) == 0:
        return -99999
      if len(sellJsonList['asks'])  == 0:
        return -99999
      
      buyFormVolumn = sumOrder

       

      
      for ask in sellJsonList['asks']:      
        if buyFormVolumn - ((float(ask[0])*float(ask[1]))) >= 0 :      
          buyFormVolumn = buyFormVolumn - (float(ask[0])*float(ask[1]))        
          listOrder.append(float(ask[1])-(float(ask[1])*tradeFee/100)) ## get index 0 or 1 ?
          #listOfCost.append((float(ask[0])*float(ask[1])))
        if buyFormVolumn  > 0  and buyFormVolumn < (float(ask[0])*float(ask[1]))  :        
          listOrder.append((buyFormVolumn/float(ask[0]))-((buyFormVolumn/float(ask[0])*tradeFee)/100))
          #listOfCost.append(buyFormVolumn)
          buyFormVolumn=0
        rate = str(float(sellJsonList['asks'][0][0]))     
      
      balace = sum([float(i) for i in listOrder])
     # sumCost = sum([float(i) for i in listOfCost])  
    
    # ^^^4.1 
    # vvv4.2
    elif groupType == '2' :
      global percentProfit      
      rateBid = 0       
      buyFormVolumn = 0
       
      buyFormVolumn = sumOrder
      buyFormVolumn = buyFormVolumn - ((buyFormVolumn * tradeFee )/100)
      balace = 0
      listOrder = []

      # data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin).text

      # sellJsonList = json.loads(data)
      sellJsonList = biws[MCoin]
      if len(sellJsonList) == 0:
        return -99999
      if len(sellJsonList['asks'])  == 0:
        return -99999

      for bids in sellJsonList['bids']:
        if rateBid == 0:
          rateBid = float(bids[0])           
          
        if buyFormVolumn < float(bids[1])  :
          float(bids[1]) - buyFormVolumn              
          balace = balace + (float(bids[0])*buyFormVolumn)         
          listOrder.append(buyFormVolumn)        
          buyFormVolumn = 0
          break                                     

        if buyFormVolumn > float(bids[1])  :                        
          buyFormVolumn = buyFormVolumn - float(bids[1])        
          balace = balace + (float(bids[0])*float(bids[1]))         
          listOrder.append(bids[1])   

    # ^^^4.2 


    # VVVV4.1 4.2 as same


    # back to USDT -------------------------------------------------------------------------------------
    buyFormVolumn = 0
    buyFormVolumn = balace
    buyFormVolumn = buyFormVolumn - ((buyFormVolumn * tradeFee )/100)
    balace = 0
    listOrder = []

    # data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin..replace(XCoin,'')+"USDT").text

    # sellJsonList = json.loads(data)
    sellJsonList = biws[MCoin.replace(XCoin,'')+"USDT"]
    if len(sellJsonList) == 0:
        return -99999
    if len(sellJsonList['asks'])  == 0:
      return -99999

    for bids in sellJsonList['bids']:
      if rateBid == 0:
        rateBid = float(bids[0])           
        
      if buyFormVolumn < float(bids[1])  :
        float(bids[1]) - buyFormVolumn              
        balace = balace + (float(bids[0])*buyFormVolumn)         
        listOrder.append(balace + (float(bids[0])*buyFormVolumn)  )        
        buyFormVolumn = 0
        break                                     

      if buyFormVolumn > float(bids[1])  :                        
        buyFormVolumn = buyFormVolumn - float(bids[1])        
        balace = balace + (float(bids[0])*float(bids[1]))         
        listOrder.append(float(bids[0])*float(bids[1]))     




    
    # print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'USDT','Convert to THB : ',balace*rateProcess,'Profit : ',(balace) - buyFormCost  ,'USDt' )
    percentProfit = (((balace)- sumCost)*100)/sumCost
    # if percentProfit > 0:
    #   print('USDT -> ',XCoin,' -> ',MCoin,' -> USDT = ',percentProfit,' %') 
    #   msg = 'PH4 ====>  USDT -> ',XCoin,' -> ',MCoin,' -> USDT = ',percentProfit,' %'
    #   LineNoty.Noti(msg)

    # print('---------------------------------------------------------------------------------------------------------------------------------------------')

    

    return percentProfit
  except Exception as ex :
    print(str(ex)  +' at getDataAndCalFromBrokeToSell()')
    time.sleep(5)
    main()

 
 
 
 

#Starting Process--------------------------------------------------------------------------------------------
def startThread():
  try:
    main("balance-1000", 1000)
  #  _thread.start_new_thread( main, ("balance-1000", 1000, ) )
  #  _thread.start_new_thread( main, ("balance-5000", 5000, ) )
  #  _thread.start_new_thread( main, ("balance-10000", 10000, ) )
  #  _thread.start_new_thread( main, ("balance-50000", 50000, ) )
  #  _thread.start_new_thread( main, ("balance-100000", 100000, ) )
  except:
    print ("Error: unable to start thread")
  while 1:
   pass
def main(threadName,balace):    
  try:    
    coinList = BrokerList.getCoin()
   
    

    if IsMultipleCoin :
      
      while(True):  
        
        balace = 1000 #Mock Up $
        tradeFee = 0.1
        if balace > 0  :
          for Type in coinList:
           
            for xCoin in coinList[Type]:
            
              # USDT -> XCOIN -> MajorCoin -> USDT

               
              _balace = balace  

              sumOrder,sumCost = getDataAndCalFromBrokeToBuy(xCoin,_balace,tradeFee)  


              for mCoin in coinList[Type][xCoin]:  #<=== buy brokers MajorCoins

                percentProfit= getDataAndCalFromBrokeToSell(xCoin,mCoin,sumOrder,sumCost,balace,Type,tradeFee)
                if percentProfit > 0.1:
                  
                  StatToDb.StatInsert(xCoin,mCoin,percentProfit,str(balace))

                  

 
       
  except Exception as ex:
    print(str(ex) +' at main()')        
