import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio  
from collections import Counter
# import BX 
import Binance
# import Poloniex
# import Okex
import BrokerList
# import Bitfinex
import LineNoty
import ConfigBot
import ApiSetting
# import Binance
import StatToDb
import _thread
import subprocess
import binance_ws

IsOnBuying = False
IsOnSelling = False
coinAfterWithdraw = 0

MajorCoins = ['BTC','ETH'] # 
XCoins = ['KEY','VET']#,'XVG','XEM','GNT'] #  
listNeedConvertCoins = ['POWR','REP','EVX','XZC']  
listTHBNeedConvertCoins = ['ZEC','DOGE']
coinABTPercentage = {'XRP':0.5,'BTC':0.5,'ETH':0.5,'EOS':0.5,'BCH':0.5,'XLM':0.5,'ZRX':0.5,'LTC':0.5,'NEO':0.5,'TRX':0.5,'XTZ':0.5,'ATOM':0.5,'NANO':0.5,'BSV':0.5,'LINK':0.5}
IsMultipleCoin = True #<----------------------------- if false it will making fix only CoinType of onec.
CoinType ='ETH'
IsRealMode = True #<----------------------------- use cost from any accounts.

# usdtRate = Bitfinex.getRateUsdUsdt()
minimumTHBToTrade = 3000
 
global buyprice  
global sellprice  
global sellprice2  

# binance_ws.COIN_ARRAY = ['ETHBTC','BTCUSDT','ETHUSDT']
# binance_ws.COIN_ARRAY = ['ETHBTC', 'LTCBTC', 'BNBBTC', 'NEOBTC', 'MCOBTC', 'QTUMBTC', 'OMGBTC', 'ZRXBTC', 'FUNBTC', 'IOTABTC', 'LINKBTC', 'MTLBTC', 'EOSBTC', 'ETCBTC', 'ZECBTC', 'DASHBTC',
#                          'TRXBTC', 'XRPBTC', 'ENJBTC', 'NULSBTC', 'XMRBTC', 'BATBTC', 'ADABTC', 'XLMBTC', 'WAVESBTC', 'GTOBTC', 'ICXBTC', 'RLCBTC', 'IOSTBTC', 'NANOBTC', 'ZILBTC', 'ONTBTC', 'STORMBTC', 'WANBTC',
#                          'CVCBTC', 'THETABTC', 'IOTXBTC', 'KEYBTC', 'MFTBTC', 'HOTBTC', 'VETBTC', 'DOCKBTC', 'HCBTC', 'RVNBTC', 'MITHBTC', 'RENBTC', 'ONGBTC', 'FETBTC', 'CELRBTC', 'MATICBTC', 'ATOMBTC', 'TFUELBTC',
#                          'ONEBTC', 'FTMBTC', 'ALGOBTC', 'ERDBTC', 'DOGEBTC', 'DUSKBTC', 'ANKRBTC', 'COSBTC', 'COCOSBTC', 'TOMOBTC', 'PERLBTC', 'CHZBTC', 'BANDBTC', 'BEAMBTC', 'XTZBTC', 'HBARBTC', 'NKNBTC', 'STXBTC',
#                          'KAVABTC', 'ARPABTC', 'CTXCBTC', 'BCHBTC', 'TROYBTC', 'VITEBTC', 'FTTBTC', 'OGNBTC', 'DREPBTC', 'TCTBTC', 'QTUMETH', 'EOSETH', 'BNBETH', 'MCOETH', 'OMGETH', 'ZRXETH', 'FUNETH', 'NEOETH',
#                          'IOTAETH', 'LINKETH', 'MTLETH', 'ETCETH', 'ZECETH', 'DASHETH', 'TRXETH', 'XRPETH', 'ENJETH', 'NULSETH', 'XMRETH', 'BATETH', 'ADAETH', 'XLMETH', 'LTCETH', 'WAVESETH', 'GTOETH', 'ICXETH',
#                          'RLCETH', 'IOSTETH', 'NANOETH', 'ZILETH', 'ONTETH', 'STORMETH', 'WANETH', 'CVCETH', 'THETAETH', 'IOTXETH', 'NPXSETH', 'KEYETH', 'MFTETH', 'DENTETH', 'HOTETH', 'VETETH', 'DOCKETH', 'HCETH',
#                          'NULSBNB', 'BATBNB', 'NEOBNB', 'IOTABNB', 'XLMBNB', 'LTCBNB', 'WAVESBNB', 'GTOBNB', 'ICXBNB', 'MCOBNB', 'RLCBNB', 'NANOBNB', 'ZILBNB', 'ONTBNB', 'STORMBNB', 'QTUMBNB', 'WANBNB', 'ADABNB',
#                          'EOSBNB', 'THETABNB', 'XRPBNB', 'ENJBNB', 'TRXBNB', 'ETCBNB', 'MFTBNB', 'VETBNB', 'RVNBNB', 'MITHBNB', 'RENBNB', 'BTTBNB', 'ONGBNB', 'HOTBNB', 'ZRXBNB', 'FETBNB', 'XMRBNB', 'ZECBNB', 'IOSTBNB',
#                          'CELRBNB', 'DASHBNB', 'OMGBNB', 'MATICBNB', 'ATOMBNB', 'TFUELBNB', 'ONEBNB', 'FTMBNB', 'ALGOBNB', 'ERDBNB', 'DOGEBNB', 'DUSKBNB', 'ANKRBNB', 'WINBNB', 'COSBNB', 'COCOSBNB', 'TOMOBNB', 'PERLBNB',
#                          'CHZBNB', 'BANDBNB', 'BEAMBNB', 'XTZBNB', 'HBARBNB', 'NKNBNB', 'STXBNB', 'KAVABNB', 'ARPABNB', 'CTXCBNB', 'BCHBNB', 'TROYBNB', 'VITEBNB', 'FTTBNB', 'OGNBNB', 'DREPBNB', 'TCTBNB', 'TRXXRP', 'BTTTRX',
#                          'WINTRX', 'BNBBUSD', 'BTCBUSD', 'XRPBUSD', 'ETHBUSD', 'LTCBUSD', 'LINKBUSD', 'ETCBUSD', 'TRXBUSD', 'EOSBUSD', 'XLMBUSD', 'ADABUSD', 'BCHBUSD', 'QTUMBUSD', 'VETBUSD', 'EURBUSD', 'BULLBUSD', 'BEARBUSD',
#                          'BNBPAX', 'BTCPAX', 'ETHPAX', 'XRPPAX', 'EOSPAX', 'XLMPAX', 'USDCPAX', 'LINKPAX', 'LTCPAX', 'TRXPAX', 'BTTPAX', 'ZECPAX', 'ADAPAX', 'NEOPAX', 'BATPAX', 'ALGOPAX', 'DUSKPAX', 'ONTPAX', 'BCHPAX',
#                          'BTCTUSD', 'ETHTUSD', 'BNBTUSD', 'XRPTUSD', 'EOSTUSD', 'XLMTUSD', 'ADATUSD', 'TRXTUSD', 'NEOTUSD', 'PAXTUSD', 'USDCTUSD', 'LINKTUSD', 'WAVESTUSD', 'LTCTUSD', 'BTTTUSD', 'ZECTUSD', 'ATOMTUSD', 'BATTUSD',
#                          'ALGOTUSD', 'BCHTUSD', 'BNBUSDC', 'BTCUSDC', 'ETHUSDC', 'XRPUSDC', 'EOSUSDC', 'LINKUSDC', 'WAVESUSDC', 'LTCUSDC', 'TRXUSDC', 'BTTUSDC', 'ZECUSDC', 'ADAUSDC', 'NEOUSDC', 'ATOMUSDC', 'BATUSDC', 'ONEUSDC',
#                          'DUSKUSDC', 'WINUSDC', 'TOMOUSDC', 'BCHUSDC', 'BTCEUR', 'ETHEUR', 'BNBEUR', 'XRPEUR', 'BNBUSDS', 'BTCUSDS', 'BUSDNGN', 'BNBNGN', 'BTCNGN',
#                          # type2
#                          'BTCBUSD', 'PAXBTC', 'TUSDBTC', 'USDCBTC', 'EURBTC', 'USDSBTC', 'NGNBTC', 'ETHBTC', 'ETHBUSD', 'ETHPAX', 'TUSDETH', 'ETHUSDC', 'ETHEUR', 'BTCBNB', 'ETHBNB', 'BNBBUSD', 'BNBPAX', 'BNBTUSD', 'BNBUSDC', 'BNBEUR', 'BNBUSDS', 'BNBNGN', 'NEOBTC', 'NEOETH', 'NEOBNB', 'NEOPAX', 'NEOTUSD', 'NEOUSDC', 'LTCBTC', 'LTCETH', 'LTCBNB', 'LTCBUSD', 'LTCPAX', 'LTCTUSD', 'LTCUSDC', 'QTUMBTC', 'QTUMETH', 'QTUMBNB', 'QTUMBUSD', 'ADABTC', 'ADAETH', 'ADABNB', 'ADABUSD', 'ADAPAX', 'ADATUSD', 'ADAUSDC', 'XRPBTC', 'XRPETH', 'XRPBNB', 'XRPBUSD', 'XRPPAX', 'XRPTUSD', 'XRPUSDC', 'XRPEUR', 'EOSBTC', 'EOSETH', 'EOSBNB', 'EOSBUSD', 'EOSPAX', 'EOSTUSD', 'EOSUSDC', 'IOTABTC', 'IOTAETH', 'IOTABNB', 'XLMBTC', 'XLMETH', 'XLMBNB', 'XLMBUSD', 'XLMPAX', 'XLMTUSD', 'ONTBTC', 'ONTETH', 'ONTBNB', 'ONTPAX', 'TRXBTC', 'TRXETH', 'TRXBNB', 'TRXXRP', 'TRXBUSD', 'TRXPAX', 'TRXTUSD', 'TRXUSDC', 'ETCBTC', 'ETCETH', 'ETCBNB', 'ETCBUSD', 'ICXBTC', 'ICXETH', 'ICXBNB', 'NULSBTC', 'NULSETH', 'NULSBNB', 'VETBTC', 'VETETH', 'VETBNB', 'VETBUSD', 'PAXTUSD', 'USDCPAX', 'USDCTUSD', 'LINKBTC', 'LINKETH', 'LINKBUSD', 'LINKPAX', 'LINKTUSD', 'LINKUSDC', 'WAVESBTC', 'WAVESETH', 'WAVESBNB', 'WAVESTUSD', 'WAVESUSDC', 'BTTBNB', 'BTTTRX', 'BTTPAX', 'BTTTUSD', 'BTTUSDC', 'ONGBTC', 'ONGBNB', 'HOTBTC', 'HOTETH', 'HOTBNB', 'ZILBTC', 'ZILETH', 'ZILBNB', 'ZRXBTC', 'ZRXETH', 'ZRXBNB', 'FETBTC', 'FETBNB', 'BATBTC', 'BATETH', 'BATBNB', 'BATPAX', 'BATTUSD', 'BATUSDC', 'XMRBTC', 'XMRETH', 'XMRBNB', 'ZECBTC', 'ZECETH', 'ZECBNB', 'ZECPAX', 'ZECTUSD', 'ZECUSDC', 'IOSTBTC', 'IOSTETH', 'IOSTBNB', 'CELRBTC', 'CELRBNB', 'DASHBTC', 'DASHETH', 'DASHBNB', 'NANOBTC', 'NANOETH', 'NANOBNB', 'OMGBTC', 'OMGETH', 'OMGBNB', 'THETABTC', 'THETAETH', 'THETABNB', 'ENJBTC', 'ENJETH', 'ENJBNB', 'MITHBTC', 'MITHBNB', 'MATICBTC', 'MATICBNB', 'ATOMBTC', 'ATOMBNB', 'ATOMTUSD', 'ATOMUSDC', 'TFUELBTC', 'TFUELBNB', 'ONEBTC', 'ONEBNB', 'ONEUSDC', 'FTMBTC', 'FTMBNB', 'ALGOBTC', 'ALGOBNB', 'ALGOPAX', 'ALGOTUSD', 'GTOBTC', 'GTOETH', 'GTOBNB', 'ERDBTC', 'ERDBNB', 'DOGEBTC', 'DOGEBNB', 'DUSKBTC', 'DUSKBNB', 'DUSKPAX', 'DUSKUSDC', 'ANKRBTC', 'ANKRBNB', 'WINBNB', 'WINTRX', 'WINUSDC', 'COSBTC', 'COSBNB', 'NPXSETH', 'COCOSBTC', 'COCOSBNB', 'MTLBTC', 'MTLETH', 'TOMOBTC', 'TOMOBNB', 'TOMOUSDC', 'PERLBTC', 'PERLBNB', 'DENTETH', 'MFTBTC', 'MFTETH', 'MFTBNB', 'KEYBTC', 'KEYETH', 'STORMBTC', 'STORMETH', 'STORMBNB', 'DOCKBTC', 'DOCKETH', 'WANBTC', 'WANETH', 'WANBNB', 'FUNBTC', 'FUNETH', 'CVCBTC', 'CVCETH', 'CHZBTC', 'CHZBNB', 'BANDBTC', 'BANDBNB', 'BUSDNGN', 'BEAMBTC', 'BEAMBNB', 'XTZBTC', 'XTZBNB', 'RENBTC', 'RENBNB', 'RVNBTC', 'RVNBNB', 'HCBTC', 'HCETH', 'HBARBTC', 'HBARBNB', 'NKNBTC', 'NKNBNB', 'STXBTC', 'STXBNB', 'KAVABTC', 'KAVABNB', 'ARPABTC', 'ARPABNB', 'IOTXBTC', 'IOTXETH', 'RLCBTC', 'RLCETH', 'RLCBNB', 'MCOBTC', 'MCOETH', 'MCOBNB', 'CTXCBTC', 'CTXCBNB', 'BCHBTC', 'BCHBNB', 'BCHBUSD', 'BCHPAX', 'BCHTUSD', 'BCHUSDC', 'TROYBTC', 'TROYBNB', 'VITEBTC', 'VITEBNB', 'FTTBTC', 'FTTBNB', 'EURBUSD', 'OGNBTC', 'OGNBNB', 'DREPBTC', 'DREPBNB', 'BULLBUSD', 'BEARBUSD', 'TCTBCT', 'TCTBNB']

# binance_ws.COIN_ARRAY.extend(['ETHUSDT', 'LTCUSDT', 'BNBUSDT', 'NEOUSDT', 'MCOUSDT', 'QTUMUSDT', 'OMGUSDT', 'ZRXUSDT', 'FUNUSDT', 'IOTAUSDT', 'LINKUSDT', 'MTLUSDT', 'EOSUSDT', 'ETCUSDT',
#                               'ZECUSDT', 'DASHUSDT', 'TRXUSDT', 'XRPUSDT', 'ENJUSDT', 'NULSUSDT', 'XMRUSDT', 'BATUSDT', 'ADAUSDT', 'XLMUSDT', 'WAVESUSDT', 'GTOUSDT', 'ICXUSDT', 'RLCUSDT',
#                               'IOSTUSDT', 'NANOUSDT', 'ZILUSDT', 'ONTUSDT', 'STORMUSDT', 'WANUSDT', 'CVCUSDT', 'THETAUSDT', 'IOTXUSDT', 'KEYUSDT', 'MFTUSDT', 'HOTUSDT', 'VETUSDT', 'DOCKUSDT', 'HCUSDT', 'RVNUSDT',
#                               'MITHUSDT', 'RENUSDT', 'ONGUSDT', 'FETUSDT', 'CELRUSDT', 'MATICUSDT', 'ATOMUSDT', 'TFUELUSDT', 'ONEUSDT', 'FTMUSDT', 'ALGOUSDT', 'ERDUSDT', 'DOGEUSDT', 'DUSKUSDT', 'ANKRUSDT', 'COSUSDT',
#                               'COCOSUSDT', 'TOMOUSDT', 'PERLUSDT', 'CHZUSDT', 'BANDUSDT', 'BEAMUSDT', 'XTZUSDT', 'HBARUSDT', 'NKNUSDT', 'STXUSDT', 'KAVAUSDT', 'ARPAUSDT', 'CTXCUSDT', 'BCHUSDT', 'TROYUSDT', 'VITEUSDT',
#                               'FTTUSDT', 'OGNUSDT', 'DREPUSDT', 'TCTUSDT', 'NPXSUSDT', 'DENTUSDT', 'BTTUSDT', 'WINUSDT', 'EURUSDT', 'BULLUSDT', 'BEARUSDT', 'USDCUSDT', 'PAXUSDT', 'BUSDUSDT', 'NGNUSDT', 'USDSUSDT',
#                               'TUSDUSDT', 'BTCUSDT', 'WAVEUSDT', 'THETUSDT', 'MATIUSDT', 'TFUEUSDT', 'COCOUSDT', 'STORUSDT'])

# # binance_ws.COIN_ARRAY = ['LTCBUSD', 'LTCBTC', 'BTCUSDT', 'ETHBTC', 'ETHUSDT']

# binance_ws.setWsExchangeSelectSymbol() 

def getDataAndCalFromBrokeToBuy(XCoin,balace,tradeFee): 
  try:
    global buyprice
    rawBalace = balace
    listOrder = []
    listOfCost = []            
    _balace = rawBalace  
     
    # buyJsonList = binance_ws.getCOIN_PRICE()[XCoin+"USDT"]
    # if len(buyJsonList) == 0:
    #   return 0,0
    # if len(buyJsonList['asks'])  == 0:
    #   return 0,0
    data = req.get("https://www.binance.com/api/v1/depth?symbol="+XCoin+"USDT").text
     
    buyJsonList = json.loads(data)   
    # buyprice =  buyJsonList['asks'] 
    for  ask in buyJsonList['asks']:     
      # print(key) 
      tragetRate = ask[0]
      tragetVolumn = ask[1]
      
      if _balace - ((float(tragetRate)*float(tragetVolumn))) >= 0 :      
        _balace = _balace - (float(tragetRate)*float(tragetVolumn))        
        listOrder.append(float(tragetVolumn)-(float(tragetVolumn)*tradeFee/100)) ## get index 0 or 1 ?
        listOfCost.append((float(tragetRate)*float(tragetVolumn)))
      if _balace  > 0  and _balace < (float(tragetRate)*float(tragetVolumn))  :        
        listOrder.append((_balace/float(tragetRate))-((_balace/float(tragetRate)*tradeFee)/100))
        listOfCost.append(_balace)
        _balace=0
      
    
    sumOrder = sum([float(i) for i in listOrder])
    sumCost = sum([float(i) for i in listOfCost])
    
    



    # print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    # print('Total of Orders you got from '+brokeName+' : before => ',BeforesumOrder, ' after => ',sumOrder ,coinType+' and your\'s cost is :', sumCost ,'USDt Transfer Fee : ',transferCost)
    # print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    return sumOrder,sumCost
  except Exception as ex:
    print(str(ex) +' at getDataAndCalFromBrokeToBuy()')
    time.sleep(5)
    

def getDataAndCalFromBrokeToSell(XCoin,MCoin,sumOrder,sumCost,Mainbalace,groupType,tradeFee):
  try:  
    # vvv4.1 
    global sellprice
    global sellprice2
    if sumOrder==0 and sumCost == 0 :
      return -99999
    if groupType == '1' :
      global percentProfit
       
      balace = 0
      rateBid = 0
      listOrder = []     
      data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin).text
      sellJsonList = json.loads(data)
       
      # sellJsonList = binance_ws.getCOIN_PRICE()[MCoin]
      # if len(sellJsonList) == 0:
      #   return -99999
      # if len(sellJsonList['asks'])  == 0:
      #   return -99999
      
      buyFormVolumn = sumOrder

       

      sellprice = sellJsonList['asks']
      # for key, ask in sellJsonList['asks'].items():
      for ask in sellJsonList['asks']:
        tragetRate = ask[0]
        tragetVolumn = ask[1]
        if buyFormVolumn - ((float(tragetRate)*float(tragetVolumn))) >= 0 :      
          buyFormVolumn = buyFormVolumn - (float(tragetRate)*float(tragetVolumn))        
          listOrder.append(float(tragetVolumn)-(float(tragetVolumn)*tradeFee/100)) ## get index 0 or 1 ?
          #listOfCost.append((float(ask[0])*float(ask[1])))
        if buyFormVolumn  > 0  and buyFormVolumn < (float(tragetRate)*float(tragetVolumn))  :        
          listOrder.append((buyFormVolumn/float(tragetRate))-((buyFormVolumn/float(tragetRate)*tradeFee)/100))
          #listOfCost.append(buyFormVolumn)
          buyFormVolumn=0
      
      balace = sum([float(i) for i in listOrder])
     # sumCost = sum([float(i) for i in listOfCost])  
    
    # ^^^4.1 
    # vvv4.2
    elif groupType == '2' :
      global percentProfit      
      rateBid = 0       
      buyFormVolumn = 0
       
      buyFormVolumn = sumOrder
      buyFormVolumn = buyFormVolumn - ((buyFormVolumn * tradeFee )/100)
      balace = 0
      listOrder = []

      data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin).text

      sellJsonList = json.loads(data)
      # sellJsonList = binance_ws.getCOIN_PRICE()[MCoin]
      # if len(sellJsonList) == 0:
      #   return -99999
      # if len(sellJsonList['bids'])  == 0:
      #   return -99999
      # sellprice = sellJsonList['bids']

      for bid in sellJsonList['bids']:
        tragetRate = bid[0]
        tragetVolumn = bid[1]

        if rateBid == 0:
          rateBid = float(tragetRate)           
          
        if buyFormVolumn < float(tragetVolumn)  :
          float(tragetVolumn) - buyFormVolumn              
          balace = balace + (float(tragetRate)*buyFormVolumn)         
          listOrder.append(buyFormVolumn)        
          buyFormVolumn = 0
          break                                     

        if buyFormVolumn > float(tragetVolumn)  :                        
          buyFormVolumn = buyFormVolumn - float(tragetVolumn)        
          balace = balace + (float(tragetRate)*float(tragetVolumn))         
          listOrder.append(tragetVolumn)   

    # ^^^4.2 


    # VVVV4.1 4.2 as same


    # back to USDT -------------------------------------------------------------------------------------
    buyFormVolumn = 0
    buyFormVolumn = balace
    buyFormVolumn = buyFormVolumn - ((buyFormVolumn * tradeFee )/100)
    balace = 0
    listOrder = []

    data = req.get("https://www.binance.com/api/v1/depth?symbol="+MCoin.replace(XCoin,'')+"USDT").text

    sellJsonList = json.loads(data)
    # sellJsonList = binance_ws.getCOIN_PRICE()[MCoin.replace(XCoin,'')+"USDT"]


    # if len(sellJsonList) == 0:
    #     return -99999
    # if len(sellJsonList['bids'])  == 0:
    #   return -99999
    sellprice2 = sellJsonList['bids']
    # print('API = >',test['bids'][:5])
    # print('WS = >',dict(Counter(sellJsonList['bids']).most_common(5)))
    # sellprice2 = sellJsonList['bids']
    for bid in sellJsonList['bids']:
      tragetRate = bid[0]
      tragetVolumn = bid[1]

      if rateBid == 0:
        rateBid = float(tragetRate)           
        
      if buyFormVolumn < float(tragetVolumn)  :
        float(tragetVolumn) - buyFormVolumn              
        balace = balace + (float(tragetRate)*buyFormVolumn)         
        listOrder.append(balace + (float(tragetRate)*buyFormVolumn)  )        
        buyFormVolumn = 0
        break                                     

      if buyFormVolumn > float(tragetVolumn)  :                        
        buyFormVolumn = buyFormVolumn - float(tragetVolumn)        
        balace = balace + (float(tragetRate)*float(tragetVolumn))         
        listOrder.append(float(tragetRate)*float(tragetVolumn))     




    
    # print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'USDT','Convert to THB : ',balace*rateProcess,'Profit : ',(balace) - buyFormCost  ,'USDt' )
    percentProfit = (((balace)- sumCost)*100)/sumCost
    print(percentProfit)
    # if percentProfit > - 0.3:
    #   print('Cal -> ',percentProfit)
      ## print('USDT -> ',XCoin,' -> ',MCoin,' -> USDT = ',percentProfit,' %') 
      # msg = 'PH4 REAL TRADE of ',Mainbalace,' ====>  USDT -> ',XCoin,' -> ',MCoin,' -> USDT = ',percentProfit,' %\r\n'
      
      # set1 = 'set1 = ',buyprice,'\r\n'
      # set2 = 'set2 = ',sellprice,'\r\n'
      # set3 = 'set3 = ',sellprice2
      # set3 = 'set3 = ',dict(Counter(sellprice2).most_common(5))
      # sumMsg = set1+set2+set3
      # print(set1)
      # time.sleep(0.5)
      # LineNoty.Noti(sumMsg)

      

    # print('---------------------------------------------------------------------------------------------------------------------------------------------')

    

    return percentProfit
  except Exception as ex :
    print(str(ex)  +' at getDataAndCalFromBrokeToSell()')
    time.sleep(5)
     

 
 
 
 

#Starting Process--------------------------------------------------------------------------------------------
def startThread():
  try:
   _thread.start_new_thread( main, ("balance-1000", 1000, ) )
   _thread.start_new_thread( main, ("balance-5000", 5000, ) )
   _thread.start_new_thread( main, ("balance-10000", 10000, ) )
   _thread.start_new_thread( main, ("balance-50000", 50000, ) )
   _thread.start_new_thread( main, ("balance-100000", 100000, ) )
  except:
    print ("Error: unable to start thread")
  while 1:
   pass
def main(threadName,balace):    
  try:    
    coinList = BrokerList.getCoin()
   
    

    if IsMultipleCoin :
      flag = True
      while(flag):  
        
        # balace = 1000 #Mock Up $
        tradeFee = 0.1
        if balace > 0  :
          for cCoin in coinList:
            start_time = time.time()
            _balace = balace  
            sumOrder,sumCost = getDataAndCalFromBrokeToBuy(cCoin['xCoin'],_balace,tradeFee) 
            
            percentProfit= getDataAndCalFromBrokeToSell(cCoin['xCoin'],cCoin['mCoin'],sumOrder,sumCost,balace,cCoin['Type'],tradeFee)
          
              
            if percentProfit > 0.5   :
              trading(cCoin['xCoin'],cCoin['mCoin'],cCoin['Type'],balace,percentProfit)
              flag = False
           
           

 
       
  except Exception as ex:
    print(str(ex) +' at main()')   
    main(threadName,balace)     

def trading(xCoin,mCoin,Type,balance,percentCal):
  tragetSym = mCoin.replace(xCoin,'')
  
  step1 = False
  step2 = False
  step3 = False
  # Binance.SPOrder('TOMOBTC',Binance.GetBalance('TOMO'),'SELL','TOMO')
  # Binance.SPOrder('BTCUSDT',Binance.GetBalance('BTC'),'SELL','BTC')
  msg = ""
  if Type =="1":     
    start_time = time.time()
    # while step1==False:
    step1,quantity,cum1 = Binance.SPOrder2(xCoin+"USDT",balance,'BUY',xCoin) #get BTC amount
    # print(quantity)
    # while step2==False:    
    step2,quantity2,cum2 = Binance.SPOrder2(mCoin,quantity,'BUY',xCoin) #get ETH
    # print(quantity2)
    # while step3==False:
    step3,quantity3,cum3 = Binance.SPOrder2(tragetSym+"USDT",quantity2,'SELL',tragetSym)
    # print(quantity3)
    print("--- %s seconds ---" % (time.time() - start_time))
    _percentProfit = ((float(cum3)- float(cum1))*100)/float(cum1)
    msg = 'Trade finish time speed ->' ,(time.time() - start_time),' Start Coin => ',cum1,' - ',cum3,' percent diff-> ','Cal per-> ',str(percentCal),' trade per-> ',str(_percentProfit),' Coin-> ',mCoin
  if Type =="2":
    start_time = time.time()
    # while step1==False:
    step1,quantity,cum1 = Binance.SPOrder2(xCoin+"USDT",balance,'BUY',xCoin)    
    # while step2==False:
    step2,quantity2,cum2 = Binance.SPOrder2(mCoin,quantity,'SELL',xCoin)
    # while step3==False:
    step3,quantity3,cum3 = Binance.SPOrder2(tragetSym+"USDT",quantity2,'SELL',tragetSym)
    print("--- %s seconds ---" % (time.time() - start_time))
    _percentProfit = ((float(cum3)- float(cum1))*100)/float(cum1)
    msg = 'Trade finish time speed ->' ,(time.time() - start_time),' Start Coin => ',cum1,' - ',cum3,' percent diff-> ','Cal per-> ',str(percentCal),' trade per-> ',str(_percentProfit),' Coin-> ',mCoin
  LineNoty.Noti(msg)
  print('Order Successed')


 