import BX 
import Binace
import Poloniex
import ABTMain
import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
import BrokerList
import LineNoty

IsOnBuying = False
IsOnSelling = False
minimumRebalance = 1000
percentProfitNeed = 0.25
BrokerList= BrokerList.GetBrokerList()  
 
listCoins = ['ETH','XRP','LTC','BCH','EOS','OMG']

def RebalanceCal():
  try:
    time.sleep(5)
    listAllBalance = []
    listUSDTSideBalance = []

    bxBalance = BX.GetBalance('THB')
    binaceBalance = Binace.GetBalance('USDT')*ABTMain.getExchangeRate('rebalace')
    #poloniexBalance = Poloniex.GetBalance('USDT')*ABTMain.getExchangeRate('rebalace')

    # ALL
    listAllBalance.append(bxBalance)
    listAllBalance.append(binaceBalance)
    #listAllBalance.append(poloniexBalance)

    # USDT Sides
    listUSDTSideBalance.append(binaceBalance)
    #listUSDTSideBalance.append(poloniexBalance)

    _sumALl = sum([float(i) for i in listAllBalance])
    _sumUSDT = sum([float(i) for i in listUSDTSideBalance])


    print(str(_sumALl),' ALL 100% THB | half must be : ',str(_sumALl/2))
    print('in the time | BX side : ',bxBalance,' | USDT side : ',_sumUSDT )
    return _sumALl/2,bxBalance,_sumUSDT # retrun half,bxSide,USDTSide
  except Exception as ex:
    print(str(ex) +' at RebalanceCal()') 
    time.sleep(30)
    RebalanceProcess()
     

def RebalanceProcess():
  try:
    while(True):
      time.sleep(5)

      halfTHB,bxTHB,USDTTHB = RebalanceCal()

      if ((bxTHB / (bxTHB + USDTTHB))*100) <= 40 and halfTHB > minimumRebalance:
          print(((bxTHB / (bxTHB + USDTTHB))*100),'% in THB side') # pull money from other side
          FindingOpp('bx','binace',halfTHB-bxTHB)

      if ((USDTTHB / (bxTHB + USDTTHB))*100) <= 40 and halfTHB > minimumRebalance:
          print(((USDTTHB / (bxTHB + USDTTHB))*100),'% in USDT side') # pull money from other side
          FindingOpp('binace','bx',halfTHB-USDTTHB)
  except Exception as ex:
    print(str(ex) +' at RebalanceProcess()')   
    time.sleep(30)
    RebalanceProcess()      

def FindingOpp(BrokNeeder,BrokPayer,volumeTHB):   

  try:

    listProfitPass = []
    for coin in listCoins:
        time.sleep(5)
        print('vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv')
         
        balace = volumeTHB              
                
        brokData = req.get(BrokerList[BrokPayer][coin])
        buyFormVolumn,buyFormCost,CalRate,brokeBuyName = getDataAndCalFromBrokeToBuy(brokData,BrokPayer,balace,coin) #<-- add fee            
        

        if BrokNeeder != BrokPayer:

          aBoke = req.get(BrokerList[BrokNeeder][coin])
          usdtAmount,thbAmount,thbProfit,percentProfit = getDataAndCalFromBrokeToSell(aBoke,BrokNeeder,buyFormVolumn,buyFormCost,brokeBuyName,coin,ABTMain.getExchangeRate('rebalace')) #<-- add transfer fee
        if percentProfit > percentProfitNeed and IsOnBuying ==False and IsOnSelling == False:
          profitPass = {'coinType':coin,'percentProfit':percentProfit,'balace':balace,'calRate':CalRate,'BrokPayer':BrokPayer,'BrokNeeder':BrokNeeder,'IsOnBuying':IsOnBuying,'IsOnSelling':IsOnSelling}
          listProfitPass.append(profitPass)
          #StatToDb.StatInsert(BrokPayer,BrokNeeder,percentProfit,coin) #<--- comment it if not on server evironment.
                
    if len(listProfitPass) > 0:
        CalGreatProfitMultiple(listProfitPass,volumeTHB)       
    #time.sleep(5*60)

    
      
    
        
        
  except Exception as ex:
    print(str(ex) +' at Finding Opp()')          

def CalGreatProfitMultiple(listProfit,volumeTHB):
  try:
    _highValue = {}

    for findHigh in listProfit:
      if _highValue == {}:
        _highValue = findHigh
      if findHigh['percentProfit'] > _highValue['percentProfit'] :
        _highValue = findHigh     

    msg = 'Rebalance Pocessing......'
    LineNoty.Noti(msg)      
    ABTMain.tradeLoop(volumeTHB,volumeTHB,_highValue['BrokPayer'],_highValue['BrokNeeder'],_highValue['IsOnBuying'],_highValue['IsOnSelling'],_highValue['coinType'],ABTMain.getExchangeRate('rebalace'),True)

  except Exception as ex:
    print(str(ex) + ' at CalGreatProfitMultiple()')

def getDataAndCalFromBrokeToBuy(data,brokeName,balace,coinType): 
  try:
    time.sleep(5)
    rawBalace = balace
    listOrder = []
    listOfCost = []  
    
    if brokeName =='bitfinex':
      jsonList = bitfinexConvertAPIProblem(data)
    elif brokeName == 'kraken':
      jsonList = json.loads(data.text)['result']
    else:
      jsonList = json.loads(data.text)

    _balace = rawBalace  
    if brokeName !='bx':
      _balace = rawBalace / ABTMain.getExchangeRate('rebalace')
  
     
    
      
    for ask in jsonList['asks']: #Buy just top 10 [:9]      
      
      if _balace - ((float(ask[0])*float(ask[1]))) >= 0 :      
        _balace = _balace - (float(ask[0])*float(ask[1]))        
        listOrder.append(float(ask[1])-(float(ask[1])*BrokerList[brokeName]['FEE']['tradeFee']/100))
        listOfCost.append((float(ask[0])*float(ask[1])))
      if _balace  > 0  and _balace < (float(ask[0])*float(ask[1]))  :        
        listOrder.append((_balace/float(ask[0]))-((_balace/float(ask[0])*BrokerList[brokeName]['FEE']['tradeFee'])/100))
        listOfCost.append(_balace)
        _balace=0
      rate = str(float(jsonList['asks'][0][0])+0.1)
        
    sumOrder = sum([float(i) for i in listOrder])
    sumOrder = sumOrder - BrokerList[brokeName]['FEE']['transfer'][coinType]
    sumCost = sum([float(i) for i in listOfCost])
    
    if brokeName != 'bx':
      print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
      print('Total of Orders you got from '+brokeName+' : ',sumOrder ,coinType+' and your\'s cost is :', sumCost ,'USDt')
      print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    else:
      print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
      print('Total of Orders you got from '+brokeName+' : ',sumOrder ,coinType+' and your\'s cost is :', sumCost ,'THB')
      print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
    return sumOrder,sumCost,rate,brokeName
  except Exception as ex:
    print(str(ex) +' at getDataAndCalFromBrokeToBuy()')
    FindingOpp()


def getDataAndCalFromBrokeToSell(data,brokeName,buyFormVolumn,buyFormCost,brokeBuyName,coinType,rateProcess):
  try:
      
    time.sleep(5)
    balace = 0
    listOrder = [] 
    if brokeName == 'kraken':
      jsonList = json.loads(data.text)['result']
    elif brokeName == 'bittrex':
      jsonList = bittrexConvertAPIProblem(data.text)
    elif brokeName == 'yobit':
      jsonList = yobitConvertAPIProblem()
    elif brokeName == 'bitfinex':
      jsonList = bitfinexConvertAPIProblem(data)
    else:
      jsonList = json.loads(data.text)


    for bids in jsonList['bids']:
      
      while(buyFormVolumn !=0):         
        
        if buyFormVolumn < float(bids[1])  :
          float(bids[1]) - buyFormVolumn              
          balace = balace + (float(bids[0])*buyFormVolumn)  
          fee = ((float(bids[0])*buyFormVolumn) * BrokerList[brokeName]['FEE']['tradeFee']) / 100
          balace = balace - fee
          listOrder.append(buyFormVolumn)        
          buyFormVolumn = 0
          break                                     
                                
        buyFormVolumn = buyFormVolumn - float(bids[1])
        #print(buyFormVolumn)
        balace = balace + (float(bids[0])*float(bids[1]))  
        fee = ((float(bids[0])*float(bids[1]))*BrokerList[brokeName]['FEE']['tradeFee'])/100
        balace = balace - fee
        listOrder.append(bids[1])     
        
        
    sumOrder = sum([float(i) for i in listOrder])  
    if brokeBuyName == 'bx':
      if brokeName == 'bx': #<------ sell to
        print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'THB','Convert to USDt : ',balace/rateProcess,'Profit : ',(balace) - buyFormCost  ,'THB' )
        percentProfit = (((balace)- buyFormCost)*100)/buyFormCost
        print('Profit %', percentProfit)  
      else:
        print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'USDT','Convert to THB : ',balace*rateProcess,'Profit : ',(balace*rateProcess) - buyFormCost  ,'THB' )
        percentProfit = (((balace*rateProcess)- buyFormCost)*100)/buyFormCost
        print('Profit %', percentProfit)  
    else:
      if brokeName == 'bx': #<------ sell to
        print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'THB','Convert to USDt : ',balace/rateProcess,'Profit : ',(balace/rateProcess) - buyFormCost  ,'USDt' )
        percentProfit = (((balace/rateProcess)- buyFormCost)*100)/buyFormCost
        print('Profit %', percentProfit)  
      else:
        print('If you sell it to' ,brokeName,'Total you sale '+coinType+' : ',sumOrder ,coinType+' and your\'s sell is :', balace ,'USDT','Convert to THB : ',balace*rateProcess,'Profit : ',(balace) - buyFormCost  ,'USDt' )
        percentProfit = (((balace)- buyFormCost)*100)/buyFormCost
        print('Profit %', percentProfit) 

    print('---------------------------------------------------------------------------------------------------------------------------------------------')

    

    return balace,balace*rateProcess,(balace*rateProcess) - buyFormCost,percentProfit
  except Exception as ex :
    print(str(ex) +' at getDataAndCalFromBrokeToSell()')
    FindingOpp()

RebalanceProcess()




