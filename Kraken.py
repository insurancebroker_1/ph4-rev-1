
import requests as req
import json
import hmac
import hashlib
import time
import base64
import urllib
import ApiKeyExchange
import Lib.krakenex.api as krakenexApi
import Bitfinex
import LineNoty

EXCHANGE_NAME = "Kraken"
accountAPI = krakenexApi.API(ApiKeyExchange.Kraken_KEY , ApiKeyExchange.Kraken_SECRET)
usdtRate = Bitfinex.getRateUsdUsdt()
def GetBalance(_symbol):
    try:
        # time.sleep(1)
        # nonce = time.time()
        # data1 = {
        #     'nonce'  : int(nonce)
        # } 
        # postdata = urllib.parse.urlencode(data1)
        # urlpath = "/0/private/Balance"

        # # Unicode-objects must be encoded before hashing
        # encoded = (str(data1['nonce']) + postdata).encode()

        # message = urlpath.encode() + hashlib.sha256(encoded).digest()

        # signature = hmac.new(base64.b64decode(ApiKeyExchange.Kraken_SECRET),
        #                         message, hashlib.sha512)
        
        # sigdigest = base64.b64encode(signature.digest())
        
        
        
        # header = { 'API-Key': ApiKeyExchange.Kraken_KEY , 'API-Sign': sigdigest.decode()}
        # r = req.post('https://api.kraken.com/0/private/Balance', data = data1,  headers =  header , timeout = None)
        symbol = _symbol.upper()
        if symbol == 'BTC':
            symbol = 'XBT'
        symbol = "X"+symbol

        if _symbol == 'USDT':
            symbol = 'USDT'
        if _symbol == 'USD':
            symbol = 'ZUSD'

        r =  accountAPI.query_private('Balance' , {})
        jsonConvert = r
        if 'result' in jsonConvert:
            if symbol in jsonConvert['result']:
                return float(jsonConvert['result'][symbol])

        return 0.0
    except Exception as ex : 
        print(str(ex) + ' at GetBalance('+symbol+')')  


def GetAccount(symbol="", exchange="" ):
 try:
    symbol = symbol.upper()
    # nonce = int(1000*time.time())
    # data1 = {
    #     'nonce'  : nonce,
    #     'asset' : symbol
    # } 
    # postdata = urllib.parse.urlencode(data1)
    # urlpath = "/0/private/DepositMethods"

    # # Unicode-objects must be encoded before hashing
    # encoded = (str(data1['nonce']) + postdata).encode()

    # message = urlpath.encode() + hashlib.sha256(encoded).digest()

    # signature = hmac.new(base64.b64decode(ApiKeyExchange.Kraken_SECRET),
    #                         message, hashlib.sha512)
    
    # sigdigest = base64.b64encode(signature.digest())
    
    
    
    # header = { 'API-Key': ApiKeyExchange.Kraken_KEY , 'API-Sign': sigdigest.decode()}
    # r = req.post('https://api.kraken.com/0/private/DepositMethods', data = data1,  headers =  header , timeout = None)
    # jsonConvert = json.loads(r.text)
    r =  accountAPI.query_private('DepositMethods' , {'asset' : symbol})
    jsonConvert = r

    time.sleep(1)
    # nonce = int(1000*time.time())
    # data1 = {
    #     'nonce'  : nonce,
    #     'asset' : symbol,
    #     'method' : jsonConvert['result'][0]['method']
    # } 
    # postdata = urllib.parse.urlencode(data1)
    # urlpath = "/0/private/DepositAddresses"

    # # Unicode-objects must be encoded before hashing
    # encoded = (str(data1['nonce']) + postdata).encode()

    # message = urlpath.encode() + hashlib.sha256(encoded).digest()

    # signature = hmac.new(base64.b64decode(ApiKeyExchange.Kraken_SECRET),
    #                         message, hashlib.sha512)
    
    # sigdigest = base64.b64encode(signature.digest())
    
    
    
    # header = { 'API-Key': ApiKeyExchange.Kraken_KEY , 'API-Sign': sigdigest.decode()}
    # r = req.post('https://api.kraken.com/0/private/DepositAddresses', data = data1,  headers =  header , timeout = None)
    # jsonConvert = json.loads(r.text)
    # method =  jsonConvert['result'][0]['method'] 
    method = ''
    if symbol == 'USDT':
        method = "Tether USD"
    else:
        method =  jsonConvert['result'][0]['method']

    r =  accountAPI.query_private('DepositAddresses' , {'asset' : symbol , 'method' : method })
    jsonConvert = r

    tag = ""
    address = ""
    if len(jsonConvert['result']) > 0:
      address = jsonConvert['result'][0]["address"]
      if 'tag' in jsonConvert['result'][0]:
        tag = jsonConvert['result'][0]["tag"]

    return address,tag

 except Exception as ex :
     print(str(ex) + ' at GetAccount('+symbol+')')  

def transferOrder(_symbol,tragetAccount, addressTag="" , exchange=""):
    try:
        amount = GetBalance(_symbol)
        time.sleep(1)
        symbol = _symbol.upper()
        if symbol == 'BTC':
            symbol = 'XBT'

        symbol = "X"+symbol

        if _symbol == 'USDT':
            symbol = 'USDT'

        if _symbol == 'USD':
            symbol = 'ZUSD'
        
        _symbol = _symbol.upper()
        feeJson = GetFeePrice()
        feeTran = feeJson['transfer'][_symbol]
        amount = amount - feeTran
        if amount <=0:
            print("Not enought money tranfer ( coin : "+_symbol +" , amount : "+str(amount)+" )")
            return False
        r = req.post(url='https://api.kraken.com/0/public/Assets')
        jsonConvert = json.loads(r.text)       
        
        numR = int(jsonConvert["result"][symbol]["display_decimals"])
         
        keyWd = tragetAccount
        amount = round(amount , numR)
        
        objJson = {
            'asset':  symbol.lower()
            ,'amount': amount
            , "key": tragetAccount
        }

        jsonConvert = accountAPI.query_private('Withdraw' , objJson )
        LineNoty.notiForTransfer(symbol,tragetAccount, str(amount) , EXCHANGE_NAME , addressTag, exchange)

        return jsonConvert
    except Exception as ex : 
        print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')  


def BuyOrder(_sourceSymbol, _tragetSymbol,volumn=0,isRebalance=False):
    try:
        sourceSymbol = _sourceSymbol.upper()
        tragetSymbol = _tragetSymbol.upper()
        currencyPair = tragetSymbol+sourceSymbol
        if sourceSymbol == 'BTC':
            sourceSymbol = 'XBT'
        if tragetSymbol == 'BTC':
            tragetSymbol = 'XBT'

        balance = volumn
        sourceSymbol = "X"+sourceSymbol
        tragetSymbol = "X"+tragetSymbol

        if _sourceSymbol == 'USDT':
            sourceSymbol = 'USDT'
        if _tragetSymbol == 'USDT':
            tragetSymbol = 'USDT'
        if _sourceSymbol == 'USD':
            sourceSymbol = 'ZUSD'
        if _tragetSymbol == 'USD':
            tragetSymbol = 'ZUSD'

        if not isRebalance:
            balance = GetBalance(_sourceSymbol)

        _symbol = tragetSymbol+sourceSymbol
        balance = getAmountForBuy(_symbol , balance)
        
        r = accountAPI.query_private('AddOrder' , {'pair':  _symbol,'type': 'buy' , 'ordertype':'market' , 'volume' : balance } )
        
        
        if 'txid' in r['result']:
            LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(balance))
            return True

        return False

        # return r
    except Exception as ex : 
        print(str(ex) + ' at buyOrder('+sourceSymbol+','+tragetSymbol+','+str(volumn)+','+str(isRebalance)+')')        
      

def SellOrder(_sourceSymbol, _tragetSymbol):
    try:
        sourceSymbol = _sourceSymbol.upper()
        tragetSymbol = _tragetSymbol.upper()
        currencyPair = tragetSymbol+sourceSymbol
        if sourceSymbol == 'BTC':
            sourceSymbol = 'XBT'
        if tragetSymbol == 'BTC':
            tragetSymbol = 'XBT'

       
        sourceSymbol = "X"+sourceSymbol
        tragetSymbol = "X"+tragetSymbol

        if _sourceSymbol == 'USDT':
            sourceSymbol = 'USDT'
        if _tragetSymbol == 'USDT':
            tragetSymbol = 'USDT'
        if _sourceSymbol == 'USD':
            sourceSymbol = 'ZUSD'
        if _tragetSymbol == 'USD':
            tragetSymbol = 'ZUSD'

        balance = GetBalance(_tragetSymbol)
        _symbol = tragetSymbol+sourceSymbol

        # if tragetSymbol == 'XRP':
        #     balance = int(balance)

        r = accountAPI.query_private('AddOrder' , {'pair':  _symbol,'type': 'sell' , 'ordertype':'market' , 'volume' : str(balance) } )

        if 'txid' in r['result']:
            LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(balance))
            return True

        return False
    except Exception as ex : 
        print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')     

def CheckPendingOrder(symbol):#CheckPendingOrder()
  try:
    r = accountAPI.query_private('WithdrawStatus' , {'asset': symbol})

    for item in r['result']:
        if item['status'] == 'Success':
            return True

    return False
  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')

def GetFeePrice():
  try:
    fee = {'tradeFee':0.26,
    'transfer':{
      'BTC':0.00050000,
      'ETH':0.005,
      'XRP':0.02,
      'EOS':0.05,
      'LTC':0.001,
      'BCH':0.0001,
      'USDT':5.0,
      'XLM':0.00002,
      'BSV':0.0001,
      'ADA':0.3,
      'XMR':0.0001,
      'DASH':0.005,
      'ETC':0.005,
      'ZEC':0.0001,
      'DOGE':2.0,
      'QTUM':0.01,
      'REP':0.01,
      'MLN':0.003,
      'GNO':0.005,
      'OMG':1.1,
      'LTC':0.001,
      'ATOM':0.1,
      'ZRX': 2.0,
      'NEO': 0.0,
      'TRX': 1.0,
      'XTZ': 0.2,
      'NANO': 0.05,
      'LINK' : 0.6
      
      }
    }
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')


def getAmountForBuy(_symbol , volumn):
    try:
        
        r = req.post(url='https://api.kraken.com/0/public/Depth?pair='+_symbol)
        jsonConvert = json.loads(r.text)
        balance = volumn

        amountBuy =  balance/float(jsonConvert['result'][_symbol]['asks'][3][0])

        return amountBuy
    except Exception as ex : 
        print(str(ex) + ' at getAmountForBuy('+_symbol+')')

def APICoinlistConvertToStandart(data):
    preData = json.loads(data)
    
    bid = []
    ask = []
    result = {}
    converlist = preData['result'][ list(preData['result'].keys())[0] ]
    for conv in converlist['asks'] :
      ask.append([float(conv[0]),float(conv[1])])
    for conv in converlist['bids'] :  
      bid.append([float(conv[0]),float(conv[1])])
    result = {'bids':bid,'asks':ask}

    return json.dumps(result)

def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')


 
def getCoinInfo(_sourceSymbol , _tragetSymbol = ""):
   try:
     if _tragetSymbol == "":
         return True

     sourceSymbol = _sourceSymbol.upper()
     tragetSymbol = _tragetSymbol.upper()
     currencyPair = tragetSymbol+sourceSymbol



     if sourceSymbol == 'BTC':
      sourceSymbol = 'XBT'
     if tragetSymbol == 'BTC':
      tragetSymbol = 'XBT'

     sourceSymbol = "X"+sourceSymbol
     tragetSymbol = "X"+tragetSymbol

     if _sourceSymbol == 'USDT':
            sourceSymbol = 'USDT'
     if _tragetSymbol == 'USDT':
            tragetSymbol = 'USDT'
     if _sourceSymbol == 'USD':
            sourceSymbol = 'ZUSD'
     if _tragetSymbol == 'USD':
            tragetSymbol = 'ZUSD'
     symbol = tragetSymbol + sourceSymbol

     r = req.post(url='https://api.kraken.com/0/public/Depth?pair='+symbol)
     jsonReq = json.loads(r.text)
    
     if jsonReq['error'].__len__()  == 0 :
        if len(jsonReq['result'][symbol]['asks']) == 0 and len(jsonReq['result'][symbol]['bids']) == 0 :
          return False      
     else:
        return False

     

     return True
   except Exception as ex:
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+', '+tragetSymbol+')')
    return False 

