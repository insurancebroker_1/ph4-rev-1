import requests as req
import ConfigBot
import logbotService
import Bitfinex
import json
from datetime import datetime

def Noti(msg):
    url = 'https://notify-api.line.me/api/notify'
    token = 'xA7Q7xtCQoRdgxB3jzI45XxzZ1pIp2aXC36G3q6eIkb'
    headers = {'content-type':'application/x-www-form-urlencoded','authorization':'Bearer '+token}
     
    r = req.post(url,headers=headers,data={'message':msg})
  

def normalMsg(msg):
    url = 'http://149.28.154.112:5251/CallNotify'
    r = req.post(url,data={'message':msg})

def notiBuyOrSell(Activity,Exchange,Source, sourceSymbol ,Target , tragetSymbol ,EndSource,EndTarget,Rate,rateBtcBx,rateBtcBinance):
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')

    msg_Bot_Id = '\nBot ID: ' + str(ConfigBot.Bot_ID)
    msg_Activity = '\nActivity: ' +  str(Activity)
    msg_Exchange = '\nExchange: ' +  str(Exchange)
    msg_Source = '\nSource: ' + str(Source) + ' ' + str(sourceSymbol)
    msg_Target = '\nTarget: ' + str(Target) + ' ' + str(tragetSymbol)
    msg_EndingSource = '\nEnding Source: ' + str(EndSource) + ' ' + str(sourceSymbol)
    msg_EndingTraget = '\nEnding Target: ' + str(EndTarget) + ' ' + str(tragetSymbol)
    msg_Rate = ""
    if Activity == 'Buy':
      msg_Rate = '\nRate: ' + str(Activity) + ' ' + str(tragetSymbol) + ' @'+ str(Rate) + ' ' + str(sourceSymbol)
    else:
      msg_Rate = '\nRate: ' + str(Activity) + ' ' + str(sourceSymbol) + ' @'+ str(Rate) + ' ' + str(tragetSymbol)
    msg_NewLine = "\n"
    msg_Btc_Binance = '\nnow btc binance ' + str(rateBtcBinance) + ' USDT'
    msg_Btc_Bx = '\nnow btc BX ' + str(rateBtcBx) + ' THB'
    msg_UsdUsdt = '\nnow usd to usdt ' + str(Bitfinex.getRateUsdUsdt())
    msg_UsdThb  = '\nnow usd to thb ' + str(getExchangeRate())
    msg_date_time = '\nTime ' + date_time
    
    
    msg = msg_Bot_Id + msg_Activity + msg_Exchange + msg_Source + msg_Target + msg_EndingSource + msg_EndingTraget + msg_Rate + msg_NewLine + msg_Btc_Binance + msg_Btc_Bx + msg_UsdUsdt + msg_UsdThb + msg_date_time

    logbotService.addLog(str(date_time),str(Exchange),str(ConfigBot.Bot_ID),str(Activity),str(sourceSymbol),str(Source),str(tragetSymbol),str(Target),str(EndSource),str(EndTarget),str(Rate),"")
    normalMsg(msg)

    return True

#---------------------------------------------- Error Message ----------------------------------------------#

def notiTranfer(Activity, ExchangeForm ,ExchangeTo,Source,EndingSource , sourceSymbol):
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')
    msg_Bot_Id = '\nBot ID: ' + str(ConfigBot.Bot_ID)
    msg_Activity = '\nActivity: ' +  str(Activity)
    msg_Exchange = '\nExchange: ' +  str(ExchangeForm) + ' to ' + str(ExchangeTo)
    msg_Source = '\nSource: ' + str(Source) + ' ' + str(sourceSymbol)
    msg_EndingSource = '\nEnding Source: ' + str(EndingSource) + ' ' + str(sourceSymbol)
    msg_date_time = '\nTime ' + date_time
    
    msg = msg_Bot_Id + msg_Activity + msg_Exchange + msg_Source + msg_EndingSource + msg_date_time
    normalMsg(msg)
    logbotService.addLog(str(date_time),str(ExchangeForm),str(ConfigBot.Bot_ID),str(Activity),str(sourceSymbol),str(Source),'-','-','0','-','-',ExchangeTo)

    return True

def notiRecomment(coinType , buyRate , unitBuyBroke , buybrok, sellRate , unitSellBroke , sellBoke , percentProfit , Thb ,rateBtcBinance,rateBtcBx):
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')

    msg_header = '\nThis time recommend to:[Calucation...]'
    msg_Bot_Id = '\nUse Bot ID: ' + str(ConfigBot.Bot_ID)
    msg_Buy = '\nBuy: '+ str(coinType) + ' @'+ str(buyRate) +'  '+ str(unitBuyBroke)+'   From '+str(buybrok)
    msg_Sell = '\nSell  '+ str(coinType) +'  @'+ str(sellRate) +' '+ str(unitSellBroke) +'  to '+  str(sellBoke)
    msg_Newline = '\n\n'
    msg_Profit = '\n\nProfit: '+str(round(percentProfit,2) )+'%\nSource: '+str(Thb)+'THB '
    msg_Btc_Binance = '\nnow btc binance ' + str(rateBtcBinance) + ' USDT'
    msg_Btc_Bx = '\nnow btc BX ' + str(rateBtcBx) + ' THB'
    msg_UsdUsdt = '\nnow usd to usdt ' + str(Bitfinex.getRateUsdUsdt())
    msg_UsdThb  = '\nnow usd to thb ' + str(getExchangeRate())
    msg_date_time = '\nTime ' + str(date_time)

    msg = msg_header + msg_Bot_Id + msg_Buy + msg_Sell + msg_Newline + msg_Profit + msg_Btc_Binance + msg_Btc_Bx + msg_UsdUsdt + msg_UsdThb + msg_date_time
    normalMsg(msg)
    return True

def errorMsg(msg):
    url = 'http://149.28.154.112:5251/CallNotifyError'
    r = req.post(url,data={'message':msg})


def notiForTransfer(symbol,tragetAccount, amount , exchangeForm , addressTag="", exchangeTo = ""):
  try:
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')

    msg_Bot_Id = '\nBot ID: ' + str(ConfigBot.Bot_ID)
    msg_Activity = '\nActivity: Withdraw'
    msg_Exchange = '\nExchange: ' +  str(exchangeForm) + ' to ' + str(exchangeTo)

    msg_Tag = ""
    if addressTag != "":
      msg_Tag = " Tag " + addressTag

    msg_Address = '\nAddress: ' +  str(tragetAccount) + msg_Tag
    msg_Amount = '\nAmount: ' + str(amount) + ' ' + str(symbol)
    msg_date_time = '\nTime ' + date_time
    
    msg = msg_Bot_Id + msg_Activity + msg_Exchange + msg_Address + msg_Amount  + msg_date_time
    normalMsg(msg)

    return True
  except Exception as ex : 
    print(str(ex) + ' at notiForTransfer('+symbol+' , '+tragetAccount+' ,'+amount+', '+exchangeForm+' ,'+addressTag+','+exchangeTo+')')
    return False


def notiForBuy(sourceSymbol, tragetSymbol , exchange , amount ):
  try:
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')
    msg_Bot_Id = '\nBot ID: ' + str(ConfigBot.Bot_ID)
    msg_Activity = '\nActivity: Buy'
    msg_Exchange = '\nExchange: ' +  str(exchange)
    msg_Symbol = '\nSymbol: ' + str(sourceSymbol) + '-' + str(tragetSymbol)
    msg_Amount = '\nAmount To Buy: ' + str(amount) + ' ' +  str(tragetSymbol)
    msg_date_time = '\nTime ' + date_time

    msg = msg_Bot_Id + msg_Activity + msg_Exchange + msg_Symbol + msg_Amount  + msg_date_time
    normalMsg(msg)

    return True
  except Exception as ex : 
    print(str(ex) + ' at notiForBuy('+sourceSymbol+' , '+tragetSymbol+' ,'+exchange+','+amount+')')
    return False


def notiForSell(sourceSymbol, tragetSymbol , exchange , amount ):
  try:
    date_time = datetime.now().strftime('%Y/%m/%d/ %H:%M:%S')
    msg_Bot_Id = '\nBot ID: ' + str(ConfigBot.Bot_ID)
    msg_Activity = '\nActivity: Sell'
    msg_Exchange = '\nExchange: ' +  str(exchange)
    msg_Symbol = '\nSymbol: ' + str(sourceSymbol) + '-' + str(tragetSymbol)
    msg_Amount = '\nAmount To Sell: ' + str(amount) + ' ' +  str(tragetSymbol)
    msg_date_time = '\nTime ' + date_time

    msg = msg_Bot_Id + msg_Activity + msg_Exchange + msg_Symbol + msg_Amount  + msg_date_time
    normalMsg(msg)

    return True
  except Exception as ex : 
    print(str(ex) + ' at notiForSell('+sourceSymbol+' , '+tragetSymbol+' ,'+exchange+','+amount+')')
    return False


def getExchangeRate(forBX=""):
  try:
    getlink = req.get('https://api.exchangeratesapi.io/latest?symbols=USD,THB') #USDTHB
    jsonssss = json.loads(getlink.text)

    return  jsonssss['rates']['THB'] / jsonssss['rates']['USD']
  except Exception as ex:
    print(str(ex) + ' at getExchangeRate()')



