import requests as req
import json
import time 
import sys
import re
import hmac
import hashlib
import json
from datetime import datetime
from decimal import Decimal
import decimal
import asyncio
import ApiKeyExchange
import base64
import LineNoty



BASE_URL = "https://api.bitfinex.com"
EXCHANGE_NAME = "Bitfinex"

def _nonce():
      
        return str(int(round(time.time() * 3000)))

def _headers(path, nonce, payload):
        
        # signature = "/api/" + path + nonce + body
        # print ("Signing: " + signature)
        payload['request'] = path
        payload['nonce'] = nonce
        strJson = json.dumps(payload)
        payloadBase64 = base64.urlsafe_b64encode(strJson.encode()).decode()
         
        # payloadBase64 = base64.b64encode(payloadBase64 )
        h = hmac.new(ApiKeyExchange.Bitfinex_SECRET.encode() , payloadBase64.encode(), hashlib.sha384)
        signature = h.hexdigest()

        return {
                "X-BFX-PAYLOAD":payloadBase64,
                "X-BFX-APIKEY": ApiKeyExchange.Bitfinex_KEY,
                "X-BFX-SIGNATURE": signature
        }



def GetBalance(symbol):  
  transferSuccess = True
  try:
    # -------------- get Balance
    #parameter use :  currency  ,  amount
    
    if not transferFundToExchange(symbol):
       transferSuccess = False
    
    time.sleep(5)
    symbol = symbol.lower() 
    nonce = _nonce()
    body = {}
    rawBody = json.dumps(body)
    path = "/v1/balances"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    symbol = symbol.lower()
    if symbol == "usdt":
      symbol = "ust"

    
    if len(jsonCoverted) > 0 :
      for wallet in jsonCoverted:
              if wallet['currency'] == symbol and wallet['type'] == 'exchange' :
                      return float(wallet['available'])
    return 0
  except Exception as ex :
    print(str(ex) + ' at GetBalance('+symbol+')')
    return 0
    


def GetBalanceTest(symbol):  
  transferSuccess = True
  try:
    # -------------- get Balance
    #parameter use :  currency  ,  amount
    
    if not transferFundToExchange(symbol):
       transferSuccess = False
    

    symbol = symbol.lower()
    time.sleep(1)    
    nonce = _nonce()
    body = {}
    rawBody = json.dumps(body)
    path = "/v1/balances"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    symbol = symbol.lower()
    if symbol == "usdt":
      symbol = "ust"

    
    if len(jsonCoverted) > 0 :
      for wallet in jsonCoverted:
              if wallet['currency'] == symbol and wallet['type'] == 'exchange' :
                      return float(wallet['available']) , True , transferSuccess

    return 0.0 ,False , transferSuccess
  except Exception as ex :
    return 0.0 ,False , transferSuccess
    print(str(ex) + ' at GetBalance('+symbol+')')


def transferFundToExchange(symbol):
  try:
    symbol = symbol.lower()
    if symbol == 'usdt':
      symbol = 'ust'
    
    amount , checkSuccess = GetBalanceFund(symbol)
    if amount > 0:
      time.sleep(1)  
      nonce = _nonce()
      symbol = symbol.upper()
      body = {
        "amount": str(amount) ,
        "currency": symbol ,
        "walletfrom": "deposit",
        "walletto": "exchange"
      }
      rawBody = json.dumps(body)
      path = "/v1/transfer"
      headers = _headers(path, nonce, body)

      r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

      jsonCoverted = json.loads(r.text)
      if len(jsonCoverted) > 0:
        if jsonCoverted[0]['status'] == "success":
          return True
    
    return False
  except Exception as ex :
    print(str(ex) + ' at transferFundToExchange('+symbol+')')
    return False

def transferExchangeToFund(symbol):
  try:
    symbol = symbol.lower()
    if symbol == 'usdt':
      symbol = 'ust'
    amount = GetBalanceExchange(symbol) 
    if amount > 0:
      time.sleep(1)
      nonce = _nonce()
      symbol = symbol.upper()
      body = {
        "amount": str(amount) ,
        "currency": symbol ,
        "walletfrom": "exchange",
        "walletto": "deposit"
      }
      rawBody = json.dumps(body)
      path = "/v1/transfer"
      headers = _headers(path, nonce, body)

      r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

      jsonCoverted = json.loads(r.text)
      if len(jsonCoverted) > 0:
        if jsonCoverted[0]['status'] == "success":
          return True
      
    return False
  except Exception as ex :
    print(str(ex) + ' at transferExchangeToFund('+symbol+')')
    return False


def GetBalanceFund(symbol):
  try:
    # -------------- get Balance
    #parameter use :  currency  ,  amount
    time.sleep(5)
    symbol = symbol.lower()
    if symbol == 'usdt':
      symbol = 'ust'
        
    nonce = _nonce()
    body = {}
    rawBody = json.dumps(body)
    path = "/v1/balances"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    if len(jsonCoverted) > 0 :
      for wallet in jsonCoverted:
              if wallet['currency'] == symbol and wallet['type'] == 'deposit':
                      return float(wallet['available']) , True

    return 0.0 , False
  except Exception as ex :
    return 0.0 , False
    print(str(ex) + ' at GetBalance('+symbol+')')


def GetBalanceExchange(symbol):
  try:
    # -------------- get Balance
    #parameter use :  currency  ,  amount
    time.sleep(5)
    symbol = symbol.lower()
    if symbol == 'usdt':
      symbol = 'ust'

    nonce = _nonce()
    body = {}
    rawBody = json.dumps(body)
    path = "/v1/balances"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    if len(jsonCoverted) > 0 :
      for wallet in jsonCoverted:
              if wallet['currency'] == symbol and wallet['type'] == 'exchange' :
                      return float(wallet['available'])

    return 0.0
  except Exception as ex :
    print(str(ex) + ' at GetBalance('+symbol+')')

def jsonCurrrency():
  try:
    return  {
                    'AGI': 'agi',
                    'AID': 'aid',
                    'AIO': 'aio',
                    'ANT': 'ant',
                    'AVT': 'aventus',  # #1811
                    'BAT': 'bat',
                    'BCH': 'bcash',  # undocumented
                    'BCI': 'bci',
                    'BFT': 'bft',
                    'BTC': 'bitcoin',
                    'BTG': 'bgold',
                    'CFI': 'cfi',
                    'DAI': 'dai',
                    'DADI': 'dad',
                    'DASH': 'dash',
                    'DATA': 'datacoin',
                    'DTH': 'dth',
                    'EDO': 'eidoo',  # #1811
                    'ELF': 'elf',
                    'EOS': 'eos',
                    'ETC': 'ethereumc',
                    'ETH': 'ethereum',
                    'ETP': 'metaverse',
                    'FUN': 'fun',
                    'GNT': 'golem',
                    'IOST': 'ios',
                    'IOTA': 'iota',
                    'LRC': 'lrc',
                    'LTC': 'litecoin',
                    'LYM': 'lym',
                    'MANA': 'mna',
                    'MIT': 'mit',
                    'MKR': 'mkr',
                    'MTN': 'mtn',
                    'NEO': 'neo',
                    'ODE': 'ode',
                    'OMG': 'omisego',
                    'OMNI': 'mastercoin',
                    'QASH': 'qash',
                    'QTUM': 'qtum',  # #1811
                    'RCN': 'rcn',
                    'RDN': 'rdn',
                    'REP': 'rep',
                    'REQ': 'req',
                    'RLC': 'rlc',
                    'SAN': 'santiment',
                    'SNGLS': 'sng',
                    'SNT': 'status',
                    'SPANK': 'spk',
                    'STORJ': 'stj',
                    'TNB': 'tnb',
                    'TRX': 'trx',
                    'USD': 'wire',
                    'UTK': 'utk',
                    'USDT': 'tetheruso',  # undocumented
                    'VEE': 'vee',
                    'WAX': 'wax',
                    'XLM': 'xlm',
                    'XMR': 'monero',
                    'XRP': 'ripple',
                    'XVG': 'xvg',
                    'YOYOW': 'yoyow',
                    'ZEC': 'zcash',
                    'ZRX': 'zrx',
                    'XTZ': 'tezos',
                    'ATO': 'ato',
                    'BSV': 'bsv'
                }
  except Exception as ex : 
        print(str(ex) + ' at jsonCurrrency()')


def GetAccount(symbol , exchange = ''):
  try:
    symbol = symbol.upper()
    exchange = exchange.upper()
    r = req.get('https://api.bitfinex.com/v2/conf/pub:map:currency:label')
    jsonMethod = json.loads(r.text)
    method = ""
    for item in jsonMethod[0]:
      if item[0] == symbol:
          method = item[1]
          break

    time.sleep(1)    
    nonce = _nonce()
    jsonCurrrencyInfo = jsonCurrrency()
    if symbol == 'USDT' or symbol == 'UST':
      method = "tetheruse"
      if exchange == 'KRAKEN':
        method = "tetheruso"
    else:
      method = jsonCurrrencyInfo[symbol]

    
    body = {
      "method": method.lower(),
      "wallet_name": "deposit",
      # "wallet_name": "exchange",
    }
    rawBody = json.dumps(body)
    path = "/v1/deposit/new"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)
    jsonObj = json.loads(r.text)
    address = ""
    tag = ""

    if 'address_pool' in jsonObj:
      address = jsonObj['address_pool']
      tag = jsonObj['address']
    else:
      address = jsonObj['address']
    

    return  address , tag
  except Exception as ex : 
        print(str(ex) + ' at GetBitfinexAccount('+symbol+')')




def BuyOrder(sourceSymbol, tragetSymbol,volumn=0,isRebalance=False):
  try:
    if sourceSymbol == 'USDT':
      sourscSymbol = 'UST'
    if tragetSymbol == 'USDT':
      tragetSymbol = 'UST'
    transferFundToExchange(sourceSymbol)
    sourceSymbol = sourceSymbol.upper()
    tragetSymbol = tragetSymbol.upper()
    if sourceSymbol == 'USDT':
      sourceSymbol = 'UST'
    if tragetSymbol == 'USDT':
      tragetSymbol = 'UST'

    symbol = tragetSymbol.upper() + sourceSymbol.upper()
    balance = volumn
    if not isRebalance:
      balance = GetBalanceExchange(sourceSymbol)
  

    amount , price = getQuantityCanBuy(symbol , balance)

    if amount < 0:
      amount = 0.0
    
    nonce = _nonce()
    body = {
      "symbol": symbol ,
      "amount": str(amount),
      "price": str(price),
      "exchange": 'bitfinex',
      "side": 'buy',
      "type": 'exchange market'
    }
    rawBody = json.dumps(body)
    path = "/v1/order/new"
    headers = _headers(path, nonce, body)

    

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    print(r.text)
    LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amount))

    if 'id' not in jsonCoverted:
      return False 

    
    #-------------
    return True 
  except Exception as ex : 
      print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')


def getQuantityCanBuy(symbol,volumn):
  try: 
    symbol = symbol.lower()
    getText = req.get('https://api.bitfinex.com/v1/book/'+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return  float( round(volumn-(1/1000000),6)/float(jsonConvertedRate['asks'][0]['price']) ) * 0.98 , str(jsonConvertedRate['asks'][0]['price'])
    
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+symbol+')')




def transferOrder(symbol,tragetAccount, addressTag="",exchange=""):
  try:
    symbol = symbol.upper()
    if symbol == 'USDT':
      symbol = 'UST'
   
    # time.sleep(5)
    tranToFundStat = transferExchangeToFund(symbol)
    symbol = symbol.upper()
    r = req.get('https://api.bitfinex.com/v2/conf/pub:map:currency:label')
    jsonMethod = json.loads(r.text)
    method = ""
    for item in jsonMethod[0]:
      if item[0] == symbol:
          method = item[1]
          break

    time.sleep(5)
    balance , checkSuccess = GetBalanceFund(symbol)
    
    

    exchange = exchange.upper()
    jsonCurrrencyInfo = jsonCurrrency()
    feeTranfer = GetFeePrice()
    if symbol in feeTranfer['transfer']:
      balance = balance - float(feeTranfer['transfer'][symbol])

    if symbol == "UST":
      balance = round(balance,2) - 0.01
      method = "tetheruse"
      if exchange.upper() == 'KRAKEN':
        method = "tetheruso"
    else:
      method = jsonCurrrencyInfo[symbol]
    
    

    body = {
      #  "withdraw_type": "Tether(USD) on Ethereum".lower(),
      "withdraw_type": method.lower(),
      "walletselected": "deposit",
      "amount": str(balance),
      "address": tragetAccount,
      "payment_id": ""
      
    }

    if not addressTag == "":
      body['payment_id'] =  addressTag
        


    rawBody = json.dumps(body)
    path = "/v1/withdraw"
    nonce = _nonce()
    headers = _headers(path, nonce, body)

    

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    if(len(jsonCoverted)>0):
      if "status" in jsonCoverted[0]:
        if jsonCoverted[0]['status'] == "error":
          return False

      LineNoty.notiForTransfer(symbol,tragetAccount, str(balance) , EXCHANGE_NAME , addressTag, exchange)
      return True

    return False
  except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')

def transferOrderTest(symbol,tragetAccount, addressTag="",exchange=""):
  try:
    if symbol == 'USDT':
      symbol = 'UST'
   
    # time.sleep(5)
    tranToFundStat = transferExchangeToFund(symbol)
    symbol = symbol.upper()
    r = req.get('https://api.bitfinex.com/v2/conf/pub:map:currency:label')
    jsonMethod = json.loads(r.text)
    method = ""
    for item in jsonMethod[0]:
      if item[0] == symbol:
          method = item[1]
          break

    time.sleep(5)
    balance , checkSuccess = GetBalanceFund(symbol)
    
    

    exchange = exchange.upper()
    jsonCurrrencyInfo = jsonCurrrency()
    feeTranfer = getFeeBitfinex()
    if symbol in feeTranfer:
      balance = balance - float(feeTranfer[symbol])

    if symbol == "UST":
      balance = round(balance,2) - 0.01
      method = "tetheruse"
      if exchange == 'KRAKEN':
        method = "tetheruso"
    else:
      method = jsonCurrrencyInfo[symbol]
    
    
    
    

    body = {
      #  "withdraw_type": "Tether(USD) on Ethereum".lower(),
      "withdraw_type": method.lower(),
      "walletselected": "deposit",
      "amount": str(balance),
      "address": tragetAccount,
      "payment_id": ""
      
    }

    if not addressTag == "":
      body['payment_id'] =  addressTag
        


    rawBody = json.dumps(body)
    path = "/v1/withdraw"
    nonce = _nonce()
    headers = _headers(path, nonce, body)

    

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    if(len(jsonCoverted)>0):
      if "status" in jsonCoverted[0]:
        if jsonCoverted[0]['status'] == "error":
          return False , "Error : [ " + r.text + " ]"

      return True , "Success : [ " + r.text + " ]"

    return False , "Error : [ " + r.text + " ]"
  except Exception as ex : 
      print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')
      return False , "Error Ex : [ " + str(ex)  + " ]"




def SellOrder(sourceSymbol , tragetSymbol): # น่าจะถูกละ
  try:
    transferFundToExchange(tragetSymbol)
    sourceSymbol = sourceSymbol.upper()
    tragetSymbol = tragetSymbol.upper()
    if sourceSymbol == 'USDT':
      sourscSymbol = 'UST'
    if tragetSymbol == 'USDT':
      tragetSymbol = 'UST'
    
    symbol = tragetSymbol.upper() + sourceSymbol.upper()
    balance = GetBalanceExchange(tragetSymbol)

    amount , price = getQuantityCanSale(symbol , balance)

    if amount < 0:
      amount = 0.0
    
    
    nonce = _nonce()
    body = {
      "symbol": symbol ,
      "amount": str(balance),
      "price": str(price),
      "exchange": 'bitfinex',
      "side": 'sell',
      "type": 'exchange market'
    }
    rawBody = json.dumps(body)
    path = "/v1/order/new"
    headers = _headers(path, nonce, body)

    

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)

    jsonCoverted = json.loads(r.text)
    print(r.text)
    LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME ,str(balance))

    if 'id' not in jsonCoverted:
      return False 

    
    #-------------
    return True 
  
    return True
  except Exception as ex : 
    print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')


def getQuantityCanSale(symbol,volumn):
  try: 
    
    symbol = symbol.lower()
    getText = req.get('https://api.bitfinex.com/v1/book/'+symbol)
    jsonConvertedRate = json.loads(getText.text)
    
    return round(volumn-(1/1000000),6)*float(jsonConvertedRate['bids'][0]['price']) , str(jsonConvertedRate['bids'][0]['price'])
    
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+symbol+')')



def getMinimumSellSource(sourceSymbol):
  try:
    getText = req.get("https://api.bitfinex.com/v1/symbols_details")
    jsonConvertedRate = json.loads(getText.text)
    for pair in jsonConvertedRate:
       if pair["pair"] == sourceSymbol:
               return float( pair["minimum_order_size"])


    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')  

def getMinimumBuySource(tragetSymbol):
  try:
    getText = req.get("https://api.bitfinex.com/v1/symbols_details")
    jsonConvertedRate = json.loads(getText.text)
    for pair in jsonConvertedRate:
       if pair["pair"] == tragetSymbol:
               return float( pair["minimum_margin"])


    return 0
  except Exception as ex : 
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+')')  




def getRateUsdUsdt():
    try:

        r = req.get("https://api.bitfinex.com/v1/book/ustusd")
        jsonText = json.loads(r.text)
        return  float(jsonText['asks'][0]['price'])
    except Exception as ex : 
        print(str(ex) + ' at getRateUsdUsdt()')
usdtRate = getRateUsdUsdt()
 
def GetFeePrice():
  try:
    fee = {'tradeFee': 0.2, 'transfer': {'BTC': '0.0004', 'LTC': '0.001', 'ETH': '0.00135', 'ETC': '0.01', 'ZEC': '0.003', 'XMR': '0.0001', 'DSH': '0.002', 'XRP': '0.1', 'IOT': '0.5', 'EOS': '0.0', 'SAN': '0.68521', 'OMG': '0.27083', 'NEO': '0.0', 'ETP': '0.01', 'QTM': '0.01', 'AVT': '1.4283', 'EDO': '0.74018', 'BTG': '0.001', 'DAT': '11.05', 'QSH': '3.3608', 'YYW': '9.3966', 'GNT': '5.1273', 'SNT': '18.514', 'BAT': '1.1178', 'MNA': '7.3438', 'FUN': '57.965',
'ZRX': '0.85476', 'TNB': '90.605', 'SPK': '31.164', 'TRX': '1.0', 'RCN': '1.8001', 'RLC': '0.32374', 'AID': '21.2', 'SNG': '9.0037', 'REP': '0.019123', 'ELF': '3.1812', 'NEC': '2.4281', 'IOS': '21.677', 'AIO': '0.71966', 'REQ': '12.273', 'RDN': '1.1672', 'LRC': '7.1939', 'WAX': '6.0298', 'DAI': '0.19709', 'CFI': '0.00135', 'AGI': '5.7119', 'BFT': '10.45', 'MTN': '25.897', 'ODE': '2.2501', 'ANT': '0.37146', 'DTH': '54.022', 'MIT': '7.1053', 'STJ': '1.1429', 'XLM': '0.0', 'XVG': '0.1', 'BCI': '0.1', 'MKR': '0.00040011', 'VEN': '0.00135', 'KNC': '0.98469', 'POA': '7.1128', 'EVT': '349.34', 'LYM': '30.689', 'UTK': '13.981', 'VEE': '122.73', 'DAD': '3.6067', 'ORS': '6.1364', 'AUC': '38.583', 'POY': '5.1964', 'FSN': '0.4516', 'CBT': '4.9147', 'ZCN': '4.7481', 'SEN': '243.25', 'NCA': '216.35', 'CND': '3.6814', 'CTX': '0.97869', 'PAI': '0.5', 'SEE': '151.52', 'ESS': '256.66', 'ATD': '0.0', 'ADD': '0.0', 'MTO': '3872.9', 'ATM': '586.96', 'HOT': '36.497', 'DTA': '658.54', 'IQX': '10.0', 'WPR': '28.626', 'ZIL': '19.383', 'BNT': '0.37815', 'ABS': '22.843', 'XRA': '27.076', 'MAN': '1.35', 'BBN': '468.75', 'NIO': '79.882', 'DGX': '0.0044061', 'VET': '100.0', 'UTN': '78.994', 'TKN': '0.82574', 'GOT': '0.64289', 'XTZ': '0.2', 'CNN': '4218.8', 'BOX': '30.0', 'MGO': '3.6861', 'RTE': '118.53', 'YGG': '392.45', 'MLN': '0.035886', 'WTC': '0.27', 'CSX': '1.7479', 'OMN': '1.0', 'INT': '7.6466', 'DRN': '4.7847', 'PNK': '27.49', 'DGB': '0.5', 'BSV': '0.001', 'BAB': '0.001', 'WLO': '0.0', 'VLD': '63.53', 'ENJ': '0.90037', 'ONL': '6.784', 'RBT': '0.001', 'GSD': '0.20478', 'UDC': '0.20289', 'TSD': '0.20341', 'PAX': '0.051098', 'RIF': '1.0', 'PAS': '103.85', 'VSY': '1.0', 'BTT': '50.0', 'CLO': '1.0', 'IMP': '22.805', 'SCR': '105.47', 'GNO': '0.015594', 'GEN': '2.1775', 'ATOM': '0.0', 'WBT': '0.00002673', 'XCH': '0.18982', 'EUS': '0.00024021', 'LBT': '0.0', 'LES': '0.0', 'LET': '0.20437', 'AST': '8.1257', 'FOA': '6.104', 'UFR': '8.1819', 'ZBT': '0.90135', 'OKB': '0.031891', 'USK': '0.00009963', 'GTX': '0.20437', 'KAN': '90.685', 'AMP': '0.18756', 'ALG': '0.0', 'SWM': '5.1924', 'TRI': '129.81', 'LOO': '3.375', 'DUSK': '4.3371', 'UOS': '5.5564', 'RRB': '0.18976', 'EOX': '0.0', 'DTX': '0.043987', 'FTT': '0.1106', 'CHZ': '18.166', 'LNX': '0.000001', 'USDT': '5', 'UST': '5'}}
    return fee
  except Exception as ex : 
    print(str(ex) + ' at GetFeePrice()')


def APICoinlistConvertToStandart(data):
    replaceData = data.replace("{\"price\":", "[").replace("\"amount\":", "").replace("\"timestamp\":", "").replace("},", "],").replace("}]", "]]")
    
    bid = []
    ask = []
    result = {}
    preData = json.loads(replaceData)
    for conv in preData['asks'] :
      ask.append([float(conv[0]),float(conv[1])])
    for conv in preData['bids'] :  
      bid.append([float(conv[0]),float(conv[1])])
    result = {'bids':bid,'asks':ask}

    return json.dumps(result)
   
def getCoinInfo(sourceSymbol , tragetSymbol = ""):
   try:
    sourceSymbol = sourceSymbol.upper()
    tragetSymbol = tragetSymbol.upper()
    if tragetSymbol == 'ATOM':
      tragetSymbol = 'ATO'
    

    if sourceSymbol != 'USD':
      accountInfoSource = getAccountInfo(sourceSymbol)
      if 'result' in accountInfoSource:
        if accountInfoSource['result'] != "success" :
          return False
      else:
        return False

    if tragetSymbol != 'USD' and tragetSymbol != "":
      accountInfoTraget = getAccountInfo(tragetSymbol)
      if 'result' in accountInfoTraget:
        if accountInfoTraget['result'] != "success":
          return False
      else:
        return False

    
    

    if tragetSymbol != "":
      if sourceSymbol == 'USDT':
        sourceSymbol = 'UST'
      if tragetSymbol == 'USDT':
        tragetSymbol = 'UST'
      symbol = tragetSymbol.upper() + sourceSymbol.upper()
      symbol = symbol.lower()
      getText = req.get('https://api.bitfinex.com/v1/book/'+symbol)
      jsonConvertedRate = json.loads(getText.text)

      if "bids" in jsonConvertedRate and "asks" in jsonConvertedRate:
        if len(jsonConvertedRate["bids"]) == 0 and len(jsonConvertedRate["asks"]) == 0 :
          return False
      else:
        return False

    return True
   except Exception as ex : 
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+' , '+tragetSymbol+')')
    return False 

def getAccountInfo(symbol , exchange = ""):
  try:
    

    symbol = symbol.upper()
    if symbol == 'ATOM':
       symbol = 'ATO'


    exchange = exchange.upper()
    r = req.get('https://api.bitfinex.com/v2/conf/pub:map:currency:label')
    jsonMethod = json.loads(r.text)
    method = ""
    for item in jsonMethod[0]:
      if item[0] == symbol:
          method = item[1]
          break

    time.sleep(1)    
    nonce = _nonce()
    jsonCurrrencyInfo = jsonCurrrency()
    if symbol == 'USDT' or symbol == 'UST':
      method = "tetheruse"
      if exchange == 'KRAKEN':
        method = "tetheruso"
    elif symbol in jsonCurrrencyInfo:
      method = jsonCurrrencyInfo[symbol]

    
    body = {
      "method": method.lower(),
      "wallet_name": "exchange",
    }
    rawBody = json.dumps(body)
    path = "/v1/deposit/new"
    headers = _headers(path, nonce, body)

    r = req.post(BASE_URL + path, headers=headers, data=rawBody, verify=True)
    jsonObj = json.loads(r.text)

    return jsonObj
  except Exception as ex : 
    print(str(ex) + ' at getAccountInfo()') 