import Okex
import Bitfinex
import HitBTC
import time
from datetime import datetime

XLM_OKEX_TO_BITFINEX = "Withdraw XLM  Okex to Bitfinex"
XLM_BITFINEX_TO_OKEX = "Withdraw XLM  Bitfinex to Okex"

USDT_OKEX_TO_BITFINEX = "Withdraw USDT  Okex to Bitfinex"
USDT_BITFINEX_TO_OKEX = "Withdraw USDT  Bitfinex to Okex"

WITHHDRAW_OKEX_TO_BITFINEX = "Withdraw Okex to Bitfinex"
WITHHDRAW_BITFINEX_TO_OKEX = "Withdraw Bitfinex to Okex"

SWITCH_Widthdraw = WITHHDRAW_OKEX_TO_BITFINEX

SWITCH_Widthdraw_XLM = XLM_OKEX_TO_BITFINEX
SWITCH_Widthdraw_USDT = USDT_BITFINEX_TO_OKEX


def main():
    try:
        global SWITCH_Widthdraw_XLM
        global SWITCH_Widthdraw_USDT

        global XLM_OKEX_TO_BITFINEX
        global XLM_BITFINEX_TO_OKEX

        global USDT_OKEX_TO_BITFINEX
        global USDT_BITFINEX_TO_OKEX

        global WITHHDRAW_OKEX_TO_BITFINEX
        global WITHHDRAW_BITFINEX_TO_OKEX

        global SWITCH_Widthdraw

        
        Address_XLM_OKEX , Tag_XLM_OKEX = Okex.GetAccount("XLM")
        Address_XLM_Bitfinex , Tag_XLM_Bitfinex = Bitfinex.GetAccount("XLM")

        Address_USDT_OKEX , Tag_USDT_OKEX = Okex.GetAccount("USDT")
        Address_USDT_Bitfinex , Tag_USDT_Bitfinex = Bitfinex.GetAccount("USDT")
        
        print(Okex.GetBalance('USDT'))
        print(Bitfinex.GetBalance('USDT'))

        print(Okex.GetBalance('XLM'))
        print(Bitfinex.GetBalance('XLM'))

        print(Address_XLM_OKEX + " , " + Tag_XLM_OKEX)
        print(Address_XLM_Bitfinex+ " , " +Tag_XLM_Bitfinex)

        print(Address_USDT_OKEX + " , " + Tag_USDT_OKEX)
        print(Address_USDT_Bitfinex + " , " + Tag_USDT_Bitfinex)

        while(True):
            writeLog(" ------------------------------    Start  Loop   ------------------------------ ")
            if SWITCH_Widthdraw == WITHHDRAW_OKEX_TO_BITFINEX:
                writeLog("Start Mode " + WITHHDRAW_OKEX_TO_BITFINEX)
                
                
                if SWITCH_Widthdraw_XLM == XLM_OKEX_TO_BITFINEX:
                    balance_XLM , check_XLM1 , check_XLM2 = Okex.GetBalanceTest('XLM')
                    if check_XLM1:
                        if balance_XLM > 0.1:
                            writeLog("Deposit XLM Okex Succes  Amount = " + str(balance_XLM))
                        else:
                            writeLog("Waitting Deposit XLM Okex 3 Minute")
                            time.sleep(180)
                            continue
                    else:
                        writeLog("Okex Getbalance(XLM) Error ")
                        time.sleep(5)
                        continue

                if SWITCH_Widthdraw_USDT == USDT_OKEX_TO_BITFINEX:
                    balance_USDT , check_USDT1 , check_USDT2 = Okex.GetBalanceTest('USDT')
                    if check_USDT1:
                        if balance_USDT > 0.1:
                            writeLog("Deposit USDT Okex Succes  Amount = " + str(balance_USDT))
                        else:
                            writeLog("Waitting Deposit USDT Okex 3 Minute")
                            time.sleep(180)
                            continue
                    else:
                        writeLog("Okex Getbalance(USDT) Error ")
                        time.sleep(5)
                        continue

                


                

                if SWITCH_Widthdraw_XLM == XLM_OKEX_TO_BITFINEX:
                    xlmResult , xlmText  = Okex.transferOrderTest('XLM',Address_XLM_Bitfinex ,Tag_XLM_Bitfinex,'')
                    writeLog("OKEX transferOrder(XLM) " + xlmText)
                    if xlmResult:
                        writeLog("SWITCH Mode To" + XLM_BITFINEX_TO_OKEX)
                        SWITCH_Widthdraw_XLM = XLM_BITFINEX_TO_OKEX

                time.sleep(5)

                if SWITCH_Widthdraw_USDT == USDT_OKEX_TO_BITFINEX:
                    usdtResult , usdtText  = Okex.transferOrderTest('USDT',Address_USDT_Bitfinex ,Tag_USDT_Bitfinex,'')
                    writeLog("OKEX transferOrder(USDT) " + usdtText)
                    if usdtResult:
                        writeLog("SWITCH Mode To" + USDT_BITFINEX_TO_OKEX)
                        SWITCH_Widthdraw_USDT = USDT_BITFINEX_TO_OKEX
                
                if SWITCH_Widthdraw_XLM == XLM_BITFINEX_TO_OKEX and SWITCH_Widthdraw_USDT == USDT_BITFINEX_TO_OKEX:
                    writeLog("SWITCH OKEX To Mode " + WITHHDRAW_BITFINEX_TO_OKEX)
                    SWITCH_Widthdraw = WITHHDRAW_BITFINEX_TO_OKEX



            elif SWITCH_Widthdraw == WITHHDRAW_BITFINEX_TO_OKEX:
                writeLog("Start Mode " + WITHHDRAW_BITFINEX_TO_OKEX)
               
                
                if SWITCH_Widthdraw_XLM == XLM_BITFINEX_TO_OKEX:
                    balance_XLM , check_XLM1 , check_XLM2 = Bitfinex.GetBalanceTest('XLM')
                    if check_XLM1:
                        if balance_XLM > 0.1:
                            writeLog("Deposit XLM Bitfinex Succes  Amount = " + str(balance_XLM))
                        else:
                            writeLog("Waitting Deposit XLM Bitfinex 3 Minute")
                            time.sleep(180)
                            continue
                    else:
                        writeLog("Bitfinex Getbalance(XLM) Error ")
                        time.sleep(5)
                        continue

                if SWITCH_Widthdraw_USDT == USDT_BITFINEX_TO_OKEX:
                    balance_USDT , check_USDT1 , check_USDT2 = Bitfinex.GetBalanceTest('USDT')
                    if check_USDT1:
                        if balance_USDT > 0.1:
                            writeLog("Deposit USDT Bitfinex Succes  Amount = " + str(balance_USDT))
                        else:
                            writeLog("Waitting Deposit USDT Bitfinex 3 Minute")
                            time.sleep(180)
                            continue
                    else:
                        writeLog("Bitfinex Getbalance(USDT) Error ")
                        time.sleep(5)
                        continue

                if SWITCH_Widthdraw_XLM == XLM_BITFINEX_TO_OKEX:
                    xlmResult , xlmText  = Bitfinex.transferOrderTest('XLM',Address_XLM_OKEX ,Tag_XLM_OKEX,'')
                    writeLog("BITFINEX transferOrder(XLM) " + xlmText)
                    if xlmResult:
                        writeLog("SWITCH Mode To" + XLM_OKEX_TO_BITFINEX)
                        SWITCH_Widthdraw_XLM = XLM_OKEX_TO_BITFINEX

                time.sleep(5)

                if SWITCH_Widthdraw_USDT == USDT_BITFINEX_TO_OKEX:
                    usdtResult , usdtText  = Bitfinex.transferOrderTest('USDT',Address_USDT_OKEX ,Tag_USDT_OKEX,'')
                    writeLog("BITFINEX transferOrder(USDT) " + usdtText)
                    if usdtResult:
                        writeLog("SWITCH Mode To" + USDT_OKEX_TO_BITFINEX)
                        SWITCH_Widthdraw_USDT = USDT_OKEX_TO_BITFINEX
                
                if SWITCH_Widthdraw_XLM == XLM_OKEX_TO_BITFINEX and SWITCH_Widthdraw_USDT == USDT_OKEX_TO_BITFINEX:
                    writeLog("SWITCH BITFINEX To Mode " + WITHHDRAW_OKEX_TO_BITFINEX)
                    SWITCH_Widthdraw = WITHHDRAW_OKEX_TO_BITFINEX    



            else:
                writeLog("Error not case [" + WITHHDRAW_OKEX_TO_BITFINEX + "] and [" + WITHHDRAW_BITFINEX_TO_OKEX + "]")

            writeLog(" ------------------------------ Sleep For 5 Second ------------------------------ ")
            time.sleep(5)
            
        return True
    except Exception as ex :
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        writeLog('main() Error :' + str(ex))
        print('['+current_time+'] main() Error :' + str(ex))



def writeLog(content):
    try:
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        f = open("log_test_withdraw.text", "a")
        f.write("["+current_time+"] "+ content + "\r\n")
        f.close()
        return True
    except Exception as ex :
        now = datetime.now()
        current_time = now.strftime("%d/%m/%y %H:%M:%S")
        print('['+current_time+'] writeLog() Error :'+ str(ex))


def testM():
    current_m = '0'
    while(True):
        now = datetime.now()
        if current_m == now.strftime("%M"):
            print("Wait miniute " )
            time.sleep(5)
            continue
        
        print("This miniute :"+  current_m )
        current_m = now.strftime("%M")
        
        time.sleep(5)


print(main())
# foo = 0.001
# fooT= str(foo)
# print(len(fooT.split('.')[1]))