import requests as req
import json
import base64
import hmac
import hashlib
import time
import Lib.okex.account_api as okexAccount
import Lib.okex.spot_api as okexSpot
import ApiKeyExchange
import LineNoty


EXCHANGE_NAME = "Okex"
# ApiKeyExchange.Okex_SECRET = 'D1799C5446C56094A3080378EC07EB3B'
# ApiKeyExchange.Okex_Key = 'd1e7f3cb-105c-4bf4-8d9e-52d1e0773bd1'
# ApiKeyExchange.Okex_PASSPHRASE = ApiKeyExchange.Okex_PASSPHRASE
# ApiKeyExchange.Okex_FUND = '7gUq69xJ'

# key1 = "29e322f1-4054-40a7-a7df-b3c47c43182a"
# skey1 = "2735B438B8F01B49BB2E8A26ADC6C39B"
# pkey1 = ApiKeyExchange.Okex_PASSPHRASE
# fkey1 = 'nampapaya1212'

accountAPI = okexAccount.AccountAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)
spotAPI = okexSpot.SpotAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)



def GetBalanceFund(symbol):
  try:
   time.sleep(5)
   currentTime = req.get('https://www.okex.com/api/general/v3/time')
   y = json.loads(currentTime.text)
   symbol = symbol.upper()
   message = y['iso']+ 'GET'+ '/api/account/v3/wallet'

   h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
   encoded = base64.b64encode(h)
   
   
   r = req.get('https://www.okex.com/api/account/v3/wallet', headers={'Content-Type':'application/json;  charset=UTF-8' ,
                                                                                'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                'OK-ACCESS-SIGN': encoded,
                                                                                'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
    
   jsonConvert = json.loads(r.text)
   for balan in jsonConvert:
        if balan['currency']== symbol :
            return float(balan['available']) , True

   return 0 , False
  except Exception as ex : 
    print(str(ex) + ' at GetBalance('+symbol+')')
    return 0 , False

def GetBalance(symbol):
  transferSuccess = True
  try:
   if not transferFundAccountToSpot(symbol):
     transferSuccess =  False
   time.sleep(3)
   return float(spotAPI.get_coin_account_info(symbol)['available'])
  except Exception as ex :
    return 0
    print(str(ex) + ' at GetBalance('+symbol+')') 


def GetBalanceTest(symbol):
  transferSuccess = True
  try:
   if not transferFundAccountToSpot(symbol):
     transferSuccess =  False
   time.sleep(5)
   return float(spotAPI.get_coin_account_info(symbol)['available']) , True , transferSuccess
  except Exception as ex :
    return 0 , False , transferSuccess
    print(str(ex) + ' at GetBalance('+symbol+')') 


def BuyOrder(sourceSymbol, tragetSymbol ,volumn,isRebalance):
   try:
      if not transferFundAccountToSpot(sourceSymbol):
        return False

      symbol = tragetSymbol+'-'+sourceSymbol
      #price = getRateForBuy(sourceSymbol, tragetSymbol)
      amount   = GetBalance(sourceSymbol)
      resultBool = False
      if isRebalance:
        amount = volumn

      spotAPI = okexSpot.SpotAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)
      r = spotAPI.take_order('market','buy',symbol,amount, str(amount),0)
      if 'result' in r:
        LineNoty.notiForBuy(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amount))
        resultBool = r['result']
      # jsonConvert = json.loads(r.text)
      # if 'result' in jsonConvert:
      #   return jsonConvert['result'] 

      return resultBool
   except Exception as ex : 
     print(str(ex) + ' at BuyOrder('+sourceSymbol+','+tragetSymbol+')')

def SellOrder(sourceSymbol, tragetSymbol):
  try:
      if not transferFundAccountToSpot(tragetSymbol):
        return False
      
      symbol = tragetSymbol+'-'+sourceSymbol
      #price = getRateForBuy(sourceSymbol, tragetSymbol)
      amount = GetBalance(tragetSymbol)
      
      resultBool = False

      spotAPI = okexSpot.SpotAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)
      r = spotAPI.take_order('market','sell',symbol,amount,'',0)
      if 'result' in r:
        LineNoty.notiForSell(sourceSymbol, tragetSymbol , EXCHANGE_NAME , str(amount))
        resultBool = r['result']
      # jsonConvert = json.loads(r.text)
      # if 'result' in jsonConvert:
      #   return jsonConvert['result']

      return resultBool
  except Exception as ex : 
    print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')
    return False



def transferOrder(symbol,tragetAccount, addressTag='' , exchange=""):
  amount = 0
  amountNew = 0
  _symbol = symbol
  _tragetAccount = tragetAccount
  _addressTag = addressTag
  _exchange = exchange

  try:
    if not transferSpotToFundAccount(symbol):
      return False

    tragetAccountNew = tragetAccount
    resultBool = False
    if(addressTag != ""):
      tragetAccountNew = str(tragetAccount)+':'+str(addressTag)

    amount , checkSuccess = GetBalanceFund(symbol)
    feeAmount = GetFeeWithdrawPrice(symbol , exchange)
    exchange = exchange.upper()
    symbol = symbol.upper()

    # if exchange == 'KRAKEN' and symbol == 'USDT':
    #   feeAmount = feeAmount / 2

    amountNew = amount - feeAmount
    if symbol in ['NEO']:
      amountNew = int(amountNew)
    if amountNew <= 0:
      print("AmountTotal below 0 transfer ( amount : "+str(amount)+" , fee : "+str(feeAmount)+" , amountTotal : "+str(amountNew)+")")
      return False

    r = accountAPI.coin_withdraw(symbol, amountNew, '4' , tragetAccountNew ,ApiKeyExchange.Okex_FUND ,feeAmount ) 
       
    if 'result' in r:
        LineNoty.notiForTransfer(symbol,tragetAccount, str(amountNew) , EXCHANGE_NAME , addressTag, exchange)
        resultBool = r['result']   
    #rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs: 1017748008
    return resultBool
  except Exception as ex :
    print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')' )
    if str(ex) == "API Request Error(code=34008): Insufficient funds" and amount > 0 and amountNew > 0:
      print('Sleep 5 second for retry transferOrder('+symbol+','+tragetAccount+','+addressTag+','+_exchange+')')
      time.sleep(5)
      transferOrder(_symbol , _tragetAccount , _addressTag , _exchange)


    

  


def transferOrderTest(symbol,tragetAccount, addressTag='' , exchange=""):
  amount = 0
  amountNew = 0

  try:
    if not transferSpotToFundAccount(symbol):
      return False

    tragetAccountNew = tragetAccount
    resultBool = False
    if(addressTag != ""):
      tragetAccountNew = str(tragetAccount)+':'+str(addressTag)

    amount , checkSuccess = GetBalanceFund(symbol)
    feeAmount = GetFeeWithdrawPriceMin(symbol , exchange)
    exchange = exchange.upper()
    symbol = symbol.upper()

    # if exchange == 'KRAKEN' and symbol == 'USDT':
    #   feeAmount = feeAmount / 2

    amountNew = amount - feeAmount
    if amountNew <= 0:
      textErr = "AmountTotal below 0 transfer ( amount : "+str(amount)+" , fee : "+str(feeAmount)+" , amountTotal : "+str(amountNew)+")"
      print(textErr)
      return False , textErr

    r = accountAPI.coin_withdraw(symbol, amountNew, '4' , tragetAccountNew ,ApiKeyExchange.Okex_FUND ,feeAmount ) 
    resultText = json.dumps(r)
    
    if 'result' in r:
        resultBool = r['result']

        
        return resultBool  ,  "Success : [ " + resultText  + " ] Amount = " + str(amountNew)
    #rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs: 1017748008
    return resultBool , "Error : [ " + resultText + " ] Amount = " + str(amountNew)
  except Exception as ex : 
    print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')
    return False , "Error Ex : [ " + str(ex)  + " ] Amount = " + str(amountNew) + " Amount no fee = " + str(amount)



def transferFundAccountToSpot(symbol , volumn=0.0):
  try:
    accountAPI = okexAccount.AccountAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)
    if volumn == 0.0:
       volumn , checkSuccess = GetBalanceFund(symbol)
    

    if volumn != 'null' and volumn > 0.0:
      r = accountAPI.coin_transfer(symbol, volumn, 6 , 1 )
      if 'code' in r:
        if r['code'] ==  34026:
          time.sleep(3)
          transferFundAccountToSpot(symbol , volumn)
      
    
    return True
  except Exception as ex :
    print(str(ex) + ' at transferFundAccount()')
    return False
    

def transferSpotToFundAccount(symbol , volumn=0.0):
  try:
    
    if volumn == 0.0:
       volumn = GetBalance(symbol)

    if volumn != 'null' and volumn > 0.0:
      r = accountAPI.coin_transfer(symbol, volumn, 1 , 6 ) 
      if 'code' in r:
        if r['code'] ==  34026:
          time.sleep(3)
          transferSpotToFundAccount(symbol , volumn)
    
    return True
  except Exception as ex :
    print(str(ex) + ' at transferFundAccount()')
    return False




def getQuantityCanBuy(sourceSymbol,tragetSymbol):
  try:
    getBalance = GetBalance(sourceSymbol)

    if getBalance == 'null':
        getBalance = 0.0

    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    getPrice = float(jsonConvert['asks'][0][0])

    return getBalance/getPrice
  except Exception as ex : 
    print(str(ex) + ' at getQuantityCanBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForBuy(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    rateBuy = float(jsonConvert['asks'][0][0]) * (110/100) 
    return rateBuy
  except Exception as ex : 
    print(str(ex) + ' at getRateForBuy('+sourceSymbol+','+tragetSymbol+')')

def getRateForSale(sourceSymbol, tragetSymbol ):
  try:
    time.sleep(1)
    jsonConvert = getPriceSymbol(sourceSymbol,tragetSymbol)
    rateSale = float(jsonConvert['bids'][0][0]) * (90/100) 
    return rateSale
  except Exception as ex : 
    print(str(ex) + ' at getRateForSale('+sourceSymbol+','+tragetSymbol+')')


# def GetFeeWithdrawPrice(symbol , exchange):
#    try:
#     time.sleep(1)
#     currentTime = req.get('https://www.okex.com/api/general/v3/time')
#     y = json.loads(currentTime.text)

#     message = y['iso'] + 'GET' + '/api/account/v3/withdrawal/fee?currency=' + symbol

#     h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#     encoded = base64.b64encode(h)
    
    
#     r = req.get('https://www.okex.com/api/account/v3/withdrawal/fee?currency=' + symbol, headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                   'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
#                                                                                   'OK-ACCESS-SIGN': encoded,
#                                                                                   'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                   'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
#     jsonConvert = json.loads(r.text)
#     feeReturn = float(jsonConvert[0]["max_fee"])
#     symbol = symbol.upper()
#     exchange = exchange.upper()
#     if exchange != 'KRAKEN' and symbol == 'USDT':
#       feeReturn = float(jsonConvert[1]["max_fee"])

#     return feeReturn
#    except Exception as ex : 
#      print(str(ex) + ' at GetFeePrice('+symbol+')')

def GetFeeWithdrawPriceMin(symbol , exchange):
   try:
    time.sleep(1)
    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)

    message = y['iso'] + 'GET' + '/api/account/v3/withdrawal/fee?currency=' + symbol

    h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/withdrawal/fee?currency=' + symbol, headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
    jsonConvert = json.loads(r.text)
    feeReturn = float(jsonConvert[0]["min_fee"])
    symbol = symbol.upper()
    exchange = exchange.upper()
    if exchange != 'KRAKEN' and symbol == 'USDT':
      feeReturn = float(jsonConvert[1]["min_fee"])

    return feeReturn
   except Exception as ex : 
     print(str(ex) + ' at GetFeePrice('+symbol+')')

def GetFeeWithdrawInfo():
   try:
    time.sleep(1)
    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)

    message = y['iso'] + 'GET' + '/api/account/v3/withdrawal/fee'

    h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/withdrawal/fee', headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
    jsonConvert = json.loads(r.text)
      

    return jsonConvert
   except Exception as ex : 
     print(str(ex) + ' at GetFeeWithdrawInfo()')


     

def GetFeePrice():
  try :
    # data = GetFeeWithdrawInfo()
    jsonResult = { 'tradeFee':0.135,
                     'transfer':{}
        }
    
    # for x in range(0,len(data)-1):
    #   if len(data[x]) >= 3 :
    #     coin = data[x]['currency']
    #     txfee = data[x]['max_fee']
    #     jsonResult['transfer'][coin] = float(txfee)
    jsonResult['transfer'] = {'BTC': 0.001, 'LTC': 0.002, 'ETH': 0.2, 'OKB': 1.0, 'ETC': 0.2, 'BCH': 0.002, 'USDT': 3, 'USDK': 1.0, '1ST': 30.0, 'AAC': 25.0, 'ABL': 50.0, 'ABT': 15.0, 'ACE': 11.0, 'ACT': 0.1, 'AE': 5.0, 'AIDOC': 85.0, 'ALGO': 0.004, 'ALV': 5.0, 'AMM': 30.0, 'ARDR': 10.0, 'ARK': 1.0, 'AST': 40.0, 'ATL': 7.5, 'AUTO': 1000.0, 'AVT': 4.0, 'BCD': 0.1, 'BCN': 5.0, 'BEC': 20.0, 'BKX': 15.0, 'BLOC': 30.0, 'BNT': 5.0, 'BRD': 7.5, 'BSV': 8e-05, 'BTG': 0.01, 'BTM': 14.0, 'BTT': 300.0, 'BXA': 30.0, 'CAG': 10.0, 'CAI': 5000.0, 'CAN': 15.0, 'CBT': 50.0, 'CHAT': 50.0, 'CIC': 750.0, 'CIT': 25.0, 'CMT': 60.0, 'CRO': 30.0, 'CTR': 35.0, 'CTXC': 5.0, 'CVC': 50.0, 'CVT': 200.0, 'DASH': 0.01, 'DAT': 10.0, 'DCR': 0.15, 'DENT': 500.0, 'DGB': 1.0, 'DGD': 0.1, 'DNA': 15.0, 'DNT': 100.0, 'DOGE': 20.0, 'DPY': 4.0, 'EC': 40.0, 'EDGE': 50.0, 'EDO': 10.0, 'EGT': 40.0, 'ELF': 15.0, 'EM': 30.0, 'ENG': 25.0, 'ENJ': 100.0, 'EON': 20.0, 'EOP': 10.0, 'EOS': 1.0, 'ETM': 30.0, 'EVX': 7.6, 'FAIR': 75.0, 'FSN': 1.0, 'FTM': 20.0, 'FUN': 200.0, 'GAS': 0.025, 'GNT': 20.0, 'GNX': 40.0, 'GRIN': 0.2, 'GSC': 100.0, 'GTC': 200.0, 'GTO': 50.0, 'GUSD': 1.0, 'HBAR': 2.0, 'HIT': 10000.0, 'HMC': 200.0, 'HOT': 50.0, 'HPB': 5.0, 'HYC': 20.0, 'ICN': 10.0, 'ICX': 6.0, 'INS': 12.5, 'INSUR': 5000.0, 'INT': 50.0, 'IOST': 500.0, 'IOTA': 0.5, 'IPC': 12.5, 'ITC': 0.0, 'KAN': 500.0, 'KCASH': 5.0, 'KEY': 250.0, 'KNC': 10.0, 'LA': 15.0, 'LAMB': 20.0, 'LBA': 50.0, 'LEND': 50.0, 'LEO': 1.0, 'LET': 500.0, 'LEV': 100.0, 'LIGHT': 5000.0, 'LINK': 1.0, 'LOL': 0.2, 'LRC': 20.0, 'LSK': 2.0, 'MAG': 170.0, 'MANA': 100.0, 'MCO': 1.0, 'MDA': 10.0, 'MDT': 35.0, 'MITH': 100.0, 'MKR': 0.002, 'MOF': 1.0, 'MOT': 7.5, 'MTH': 175.0, 'MTL': 2.5, 'MVP': 1000.0, 'NANO': 1.0, 'NAS': 1.0, 'NEO': 0.025, 'NGC': 7.5, 'NULS': 10.0, 'OAX': 30.0, 'OF': 3000.0, 'OMG': 0.73, 'ONG': 2.0, 'ONT': 5.0, 'ORBS': 100.0, 'ORS': 100.0, 'OST': 15.0, 'PAX': 1.0, 'PAY': 5.0, 'PLG': 200.0, 'PMA': 4000.0, 'POE': 150.0, 'PPT': 1.0, 'PRA': 15.0, 'PST': 50.0, 'QTUM': 0.01, 'QUN': 100.0, 'QVT': 20.0, 'R': 10.0, 'RCN': 100.0, 'RCT': 75.0, 'RDN': 1.5, 'READ': 100.0, 'REF': 1.0, 'REN': 250.0, 'REQ': 75.0, 'RFR': 1000.0, 'RNT': 65.0, 'ROAD': 20.0, 'SALT': 5.0, 'SAN': 5.0, 'SBTC': 0.2, 'SC': 0.5, 'SDA': 500.0, 'SHOW': 750.0, 'SMT':
40.0, 'SNC': 50.0, 'SNGLS': 100.0, 'SNM': 100.0, 'SNT': 150.0, 'SOC': 100.0, 'SPF': 25.0, 'SPND': 300.0, 'SSC': 40.0, 'STC': 100.0, 'STORJ': 10.0, 'SUB': 20.0, 'SWFTC': 1750.0, 'TCT': 250.0, 'THETA': 100.0, 'TIO': 12.5, 'TNB':
200.0, 'TOPC': 100.0, 'TRA': 2500.0, 'TRIO': 1000.0, 'TRUE': 4.0, 'TRX': 5.0, 'TUSD': 1.0, 'UBTC': 0.05, 'UCT': 50.0, 'UGC': 60.0, 'UKG': 12.5, 'USDC': 1.0, 'UTK': 15.0, 'VCASH': 0.1, 'VEE': 500.0, 'VIB': 30.0, 'VITE': 200.0, 'VIU': 200.0, 'VNT': 10.0, 'VSYS': 0.4, 'WAVES': 5.0, 'WFEE': 2500.0, 'WIN': 1000.0, 'WRC': 240.0, 'WTC': 2.0, 'WXT': 100.0, 'XAS': 20.0, 'XEM': 15.0, 'XLM': 0.1, 'XMR': 0.09, 'XPO': 100.0, 'XRP': 1.0, 'XTZ': 2.0, 'XUC': 20.0, 'YEE': 350.0, 'YOU': 100.0, 'YOYO': 5.0, 'ZCO': 250.0, 'ZEC': 0.005, 'ZEN': 0.35, 'ZIP': 5000.0, 'ZRX': 10.0, 'USDT-ERC20': 5.0, 'ATOM': 0.005,}   
      
    jsonResult['transfer']['USDT'] = 1
    return jsonResult
  except Exception as ex :
    print(str(ex) + ' at GetFeePrice()')
 

def getPriceSymbol(sourceSymbol, tragetSymbol):
    try:
      time.sleep(1)
      currentTime = req.get('https://www.okex.com/api/general/v3/time')
      y = json.loads(currentTime.text)
      symbol = tragetSymbol+'-'+sourceSymbol

      message = y['iso']+ 'GET'+ '/api/spot/v3/instruments/'+symbol+'/book'

      h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
      encoded = base64.b64encode(h)


      r = req.get('https://www.okex.com/api/spot/v3/instruments/'+symbol+'/book', headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                      'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                      'OK-ACCESS-SIGN': encoded,
                                                                                      'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                      'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
      jsonConvert = json.loads(r.text)
      return jsonConvert
    except Exception as ex : 
      print(str(ex) + ' at getPriceSymbol('+sourceSymbol+','+tragetSymbol+')')

# def getPriceSymbol(sourceSymbol, tragetSymbol):
#     try:
#       # time.sleep(1)
#       currentTime = req.get('https://www.okex.com/api/general/v3/time')
#       y = json.loads(currentTime.text)
#       symbol = tragetSymbol+'-'+sourceSymbol

#       message = y['iso']+ 'GET'+ '/api/spot/v3/instruments/'+symbol+'/book'

#       h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#       encoded = base64.b64encode(h)


#       r = req.get('https://www.okex.com/api/spot/v3/instruments/'+symbol+'/book', headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                       'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
#                                                                                       'OK-ACCESS-SIGN': encoded,
#                                                                                       'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                       'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
#       # jsonConvert = json.loads(r.text)
#       return r
#     except Exception as ex : 
#       print(str(ex) + ' at getPriceSymbol('+sourceSymbol+','+tragetSymbol+')')


# def transferOrderByLib(symbol,tragetAccount, addressTag=""):
#   try:
#     tragetAccountNew = tragetAccount
#     if(addressTag != ""):
#       tragetAccountNew = tragetAccountNew+"："+str(addressTag)
# #
#     accountAPI = okexAccount.AccountAPI(ApiKeyExchange.Okex_Key, ApiKeyExchange.Okex_SECRET, ApiKeyExchange.Okex_PASSPHRASE, True)
#     r = accountAPI.coin_withdraw(symbol, GetBalance(symbol), 4 , tragetAccountNew ,'nampapaya1212' ,GetFeeWithdrawPrice(symbol) ) 
#     print(r.text)       
    
#     return False
#   except Exception as ex : 
#     print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


def getMinimumBuySource( fakeParams='',tragetSymbol=''):
  try:
    currencies = accountAPI.get_currencies()
    for currency in currencies:
      if currency['currency'] == tragetSymbol:
         return float(currency['min_withdrawal'])
  except Exception as ex : 
    print(str(ex) + ' at getMinimumBuySource('+tragetSymbol+')')



def getMinimumSellSource(fakeSymboll='', sourceSymbol=''):
  try:
    currencies = accountAPI.get_currencies()
    for currency in currencies:
      if currency['currency'] == sourceSymbol:
        return float(currency['min_withdrawal'])
  except Exception as ex : 
    print(str(ex) + ' at getMinimumSellSource('+sourceSymbol+')')


def CheckPendingOrder(symbol):
  try:
    withdraw = accountAPI.get_coin_withdraw_record(symbol)
    if len(withdraw) > 0:
      if withdraw[0]['status'] == 2:
        return True
    else:
      return False

  except Exception as ex : 
    print(str(ex) + ' at CheckPendingOrder('+symbol+')')

def GetAccount(symbol="", exchange=""):
  try:
    symbol = symbol.lower()
    time.sleep(1)
    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)
    symbol = symbol.upper()
    message = y['iso']+ 'GET'+ '/api/account/v3/deposit/address?currency='+symbol

    h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/deposit/address?currency='+symbol, headers={'Content-Type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
      
    jsonConvert = json.loads(r.text)
    address = jsonConvert[0]["address"]

    if symbol == 'USDT':
      address = jsonConvert[1]["address"]

    tag = ""

    if 'tag' in jsonConvert[0]:
      tag = jsonConvert[0]["tag"]
    elif 'payment_id' in jsonConvert[0]:
      tag = jsonConvert[0]["payment_id"]
    elif 'memo' in jsonConvert[0]:
      tag = jsonConvert[0]["memo"]

    exchange = exchange.upper()
    if exchange == "KRAKEN" and symbol == 'USDT' :
          address = jsonConvert[0]["address"]


    return address,tag
  except Exception as ex : 
        print(str(ex) + ' at GetOkexAccount('+symbol+')')


def GetUSDTOmni(symbol):
  try:
    return "" , ""
  except Exception as ex :
    print(str(ex) + ' at GetUSDTOmni('+symbol+')')


def getCoinInfo(sourceSymbol , tragetSymbol = ""):
   try:
    sourceSymbol = sourceSymbol.upper()
    tragetSymbol = tragetSymbol.upper()
    

    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)
    message = y['iso']+ 'GET'+ '/api/account/v3/currencies'

    h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/currencies', headers={'Content-Type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
      
    jsonConvert = json.loads(r.text)
    checkCount = 0
    numCheck = 2
    if tragetSymbol == "":
      numCheck = 1

    # if sourceSymbol == "USDT":
    #   sourceSymbol = "USDT-ERC20"
    # or item['currency'].find(sourceSymbol) > -1 

    for item in jsonConvert:
      if item['currency'] == sourceSymbol or item['currency'] == tragetSymbol:
        checkCount = checkCount + 1
        if str(item['can_deposit']) != '1' or str(item['can_withdraw']) != '1':
          return False

    if checkCount < numCheck:
        return False


    if tragetSymbol != "":
      priceobj = getPriceSymbol(sourceSymbol,tragetSymbol)
      # priceSymbol = json.loads(priceobj.text)
      if 'asks' in priceobj and 'bids' in priceobj:
          if len(priceobj['asks']) == 0 and len(priceobj['bids']) == 0 :
            return False  
      else:
          return False 


    return True
   except Exception as ex:
    print(str(ex) + ' at getCoinInfo('+sourceSymbol+', '+tragetSymbol+')')
    return False 

def GetFeeWithdrawPrice(symbol , exchange):
   try:
    time.sleep(1)
    currentTime = req.get('https://www.okex.com/api/general/v3/time')
    y = json.loads(currentTime.text)

    message = y['iso'] + 'GET' + '/api/account/v3/withdrawal/fee?currency=' + symbol

    h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
    encoded = base64.b64encode(h)
    
    
    r = req.get('https://www.okex.com/api/account/v3/withdrawal/fee?currency=' + symbol, headers={'content-type':'application/json;  charset=UTF-8' ,
                                                                                  'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
                                                                                  'OK-ACCESS-SIGN': encoded,
                                                                                  'OK-ACCESS-TIMESTAMP': y['iso'],
                                                                                  'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
####
    if symbol == 'USDT':
      jsonConvert = json.loads(r.text)
      jsonConvert.remove(jsonConvert[0])
    else:
      jsonConvert = json.loads(r.text)
####

    
    feeReturn = float(jsonConvert[0]["max_fee"])
    
    symbol = symbol.upper()
    exchange = exchange.upper()
    if exchange == 'KRAKEN' and symbol == 'USDT':
      feeReturn = float(jsonConvert[1]["max_fee"])

    return feeReturn
   except Exception as ex : 
     print(str(ex) + ' at GetFeeWithdrawPrice('+symbol+')')
# print(transferOrderByLib("XRP" , "rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs" , "1015165964" ))





# def transferOrder(symbol,tragetAccount, addressTag=""):
#   try:
#     time.sleep(1)
#     currentTime = req.get('https://www.okex.com/api/general/v3/time')
#     y = json.loads(currentTime.text)
    
#     if(addressTag != ""):
#       tragetAccount = tragetAccount+"："+str(addressTag)

#     param = { 'currency': symbol ,
#               'amount': GetBalance(symbol) ,
#               'destination':4,
#               'to_address': tragetAccount,
#               'trade_pwd': 'wktsjz8q',
#               'fee':  GetFeeWithdrawPrice(symbol) 
#             }
#     paramBody = json.dumps(param)        
#     message = y["iso"] + "POST" + "/api/account/v3/withdrawal"+paramBody

#     h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#     encoded = base64.b64encode(h)
      
      
#     r = req.post('https://www.okex.com/api/spot/v3/orders' ,headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                     'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
#                                                                                     'OK-ACCESS-SIGN': encoded,
#                                                                                     'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                     'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE } )

#     jsonConvert = json.loads(r.text)
#     if 'result' in jsonConvert:
#       return jsonConvert['result']
    
#     return False
#   except Exception as ex : 
#         print(str(ex) + ' at transferOrder('+symbol+','+tragetAccount+','+addressTag+')')


# def SellOrder(sourceSymbol, tragetSymbol):
#   try:
#       time.sleep(1)
#       currentTime = req.get('https://www.okex.com/api/general/v3/time')
#       y = json.loads(currentTime.text)
#       symbol = tragetSymbol+'-'+sourceSymbol
#       price = getRateForSale(sourceSymbol, tragetSymbol)

#       message = y["iso"] + "POST" + "/api/spot/v3/orders{'type': 'limit', 'side': 'sell', 'instrument_id': "+symbol+", 'size': "+str(GetBalance(sourceSymbol))+",  'price': "+str(price)+",  'margin_trading': 1, 'order_type': '3'}"

#       h = hmac.new(bytes(ApiKeyExchange.Okex_SECRET,"ascii"), msg=bytes(message,"ascii") ,digestmod = hashlib.sha256).digest()
#       encoded = base64.b64encode(h)
      
      
#       r = req.get('https://www.okex.com/api/spot/v3/orders', headers={'content-type':'application/json;  charset=UTF-8' ,
#                                                                                     'OK-ACCESS-KEY': ApiKeyExchange.Okex_Key ,
#                                                                                     'OK-ACCESS-SIGN': encoded,
#                                                                                     'OK-ACCESS-TIMESTAMP': y['iso'],
#                                                                                     'OK-ACCESS-PASSPHRASE': ApiKeyExchange.Okex_PASSPHRASE })
#       jsonConvert = json.loads(r.text)
#       if 'result' in jsonConvert:
#         return jsonConvert['result']

#       return False
#   except Exception as ex : 
#         print(str(ex) + ' at SellOrder('+sourceSymbol+','+tragetSymbol+')')
# def tranferNew():
#   try:
    
#     print(ccxt.okex3.withdraw(ccxt.okex3,'XRP', '10' ,'rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1017748008' ))


#     return True
#   except Exception as ex : 
#     print(str(ex) + ' at CheckPendingOrder()') 



#print(transferOrder('XRP','rp7Fq2NQVRJxQJvUZ4o8ZzsTSocvgYoBbs','1017748008'))
#print(transferSpotToFundAccount('DASH'))
# print(transferOrder('DASH' , "XxgDCj9xSaPHWPaWLRJWvLTpg4puGnCAB5"))
# print(GetBalance('ardr'))
# print(getMinimumSellSource('btc','ARDR'))
